#include "SubGraph.h"

#include "SubGraph.ipp"
#include "SubGraph_special_members_and_operators.ipp"
#include "SubGraph_borrowing_logic.ipp"
#include "SubGraph_basic_algorithms.ipp"
#include "SubGraph_path_operations.ipp"
