template <typename Data>
Generic_Performance_Tester<Data>::Generic_Performance_Tester(Data& data, std::function<bool(Data&)> exec)
  : _data(std::move(data)), _exec(exec) {}

template <typename Data>
bool Generic_Performance_Tester<Data>::populate_data(){
  return this->_exec(this->_data);
}
