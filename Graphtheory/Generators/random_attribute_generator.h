#ifndef RANDOM_ATTRIBUTE_GENERATOR_H
#define RANDOM_ATTRIBUTE_GENERATOR_H

#include <cstddef>
#include <tuple>
#include <string>
#include <iostream>
#include <random>
#include <unordered_map>
#include <cassert>
#include <stdexcept>
#include <cmath>

#include "../Attribute.h"
#include "../../Common/constants.h"

class random_attribute_generator{
  static std::default_random_engine _engine;

  std::unordered_map<std::string, std::tuple<opt_dir, integrality, double, double> > _data;

public:
  random_attribute_generator() : _data({}) {}
  random_attribute_generator(std::unordered_map<std::string, std::tuple<opt_dir, integrality, double, double> > data) : _data(data) {
    for(auto [name, prop] : data){
      auto [direction, integr, lower, upper] = prop;
      assert( lower <= upper );
    }
  }

  random_attribute_generator(const random_attribute_generator& other){
    *this = other;
  }
  random_attribute_generator(random_attribute_generator&& other){
    *this = std::move(other);
  }

  std::unordered_map<std::string, Attribute> next();
  void operator>>(std::unordered_map<std::string, Attribute>& var);


  static std::string csv_columns(std::string prefix, std::string separator);
  std::string csv_data(std::string separator) const ;

  void operator=(const random_attribute_generator& other);
  void operator=(random_attribute_generator&& other);

  bool operator==(const random_attribute_generator& other) const ;
  bool operator!=(const random_attribute_generator& other) const {
    return !(*this == other);
  }

  random_attribute_generator(std::istream& is);

  friend std::ostream& operator<<(std::ostream& os, const random_attribute_generator& attribute_generator);
  friend std::istream& operator>>(std::istream& is, random_attribute_generator& attribute_generator);
};
std::ostream& operator<<(std::ostream& os, const random_attribute_generator& attribute_generator);
std::istream& operator>>(std::istream& is, random_attribute_generator& attribute_generator);


#endif
