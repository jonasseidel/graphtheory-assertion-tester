#ifndef MAINTENANCE_PROBLEM_TESTING_H
#define MAINTENANCE_PROBLEM_TESTING_H

// include struct, classes, macros and io functions necessary
#include "maintenance_problem_test_data_functions.h"


#include <functional>
#include <string>
#include <vector>
#include <filesystem>
#include <fstream>

bool generate_tests_data(std::filesystem::path path, int number_of_instances);
bool execute_tests(std::filesystem::path path, bool check_all_problem_data = false, bool add_new_execution = false);

void gather_data(std::filesystem::path path, std::vector<Data>& data_vector, int depth = 0);
std::map<std::string, std::vector<Data>> sort_data_by_parent_directory_name(const std::vector<Data>& data_vector);
std::map<std::pair<double,double>, std::vector<Data>> sort_data_by_raster_position(const std::vector<Data>& data_vector, const axis_data& x_axis_data, const axis_data& y_axis_data);
void sorted_data_to_csv(std::fstream& file, const std::map<std::string, std::vector<Data>>& sorted_data, const axis_data& x_axis_data, const axis_data& y_axis_data, const std::vector<std::string>& models);
void sorted_data_to_csv(std::fstream& file, const std::map<std::pair<double,double>, std::vector<Data>>& sorted_data, const axis_data& x_axis_data, const axis_data& y_axis_data, const std::vector<std::string>& models, bool avg_or_med);


// generates raster of tests
// gathers specified fields into a table which can be easily be plotted afterwards with pgfplots
bool generate_and_execute_2d_plot_test(std::filesystem::path path, size_t number_of_instances_per_coord, const axis_data& x_axis_data, const axis_data& y_axis_data, std::function<maintenance_problem_generator(double x, double y)> test_generator);

#endif
