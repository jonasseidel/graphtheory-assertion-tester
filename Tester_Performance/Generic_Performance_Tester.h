#ifndef GENERIC_PERFORMANCE_TESTER_H
#define GENERIC_PERFORMANCE_TESTER_H

#include <functional>
#include <vector>



template <typename Data>
class Generic_Performance_Tester{
  std::function<bool(Data&)> _exec;
public:
  Data _data;

  Generic_Performance_Tester(Data& data, std::function<bool(Data&)> exec);

  bool populate_data();
};

#include "Generic_Performance_Tester.ipp"

#endif
