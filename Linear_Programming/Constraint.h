#ifndef CONSTRAINT_H
#define CONSTRAINT_H

#include <cstddef>
#include <string>
#include <cstring>
#include <unordered_map>
#include <iostream>
#include <cmath>

#include "Variable.h"
#include "Coefficient.h"

#include <scip/cons_linear.h>

enum relation : bool {Inequality = false, Equality = true};

class Constraint{
  std::string _description;
  relation _relation;
  std::unordered_map<Variable*, Coefficient> _lhs;
  Coefficient _rhs;
public:
  Constraint(){}
  Constraint(const Constraint& other);
  Constraint(Constraint&& other);
  Constraint(std::string description, relation relation_type, std::unordered_map<Variable*, Coefficient> lhs, Coefficient rhs);
  Constraint(relation relation_type, std::unordered_map<Variable*, Coefficient> lhs, Coefficient rhs);

  Constraint(const Constraint& other, const std::unordered_map<const Variable*, Variable*>& variable_lookup);
  std::string description() const ;
  relation is_equality() const ;
  std::unordered_map<Variable*, Coefficient> lhs();
  Coefficient lhs_coefficient(Variable* var);
  Coefficient rhs();

  void operator=(const Constraint& other);
  void operator=(Constraint&& other);

  SCIP_CONS* computational_con(SCIP* scip, std::unordered_map<Variable*, SCIP_VAR*>& variable_lookup);

  friend std::ostream& operator<<(std::ostream& os, const Constraint& constraint);
};

std::ostream& operator<<(std::ostream& os, const Constraint& constraint);

#endif
