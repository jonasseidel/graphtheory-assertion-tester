#ifndef NODE_H
#define NODE_H

#include <cstddef>
#include <unordered_set>
#include <unordered_map>
#include <string>
#include <iostream>
#include <stdexcept>

#include "Attribute.h"
#include "Edge.h"

class Edge;

class Node{
  std::string _description;
  std::unordered_set<Edge*> _incident;
  std::unordered_map<std::string, Attribute> _attributes;
public:
  Node(){}
  Node(const Node& node) : _description(node.description()), _incident({}), _attributes(node.attributes()) {}
  Node(Node&& node) : _description(std::move(node._description)), _incident(std::move(node._incident)), _attributes(std::move(node._attributes)) {}

  Node(std::string description, std::unordered_map<std::string, Attribute> attributes = {}) : _description(std::move(description)), _attributes(std::move(attributes)) {}
  Node(std::istream& is);

  void operator=(const Node& node) {
    this->_description = node.description();
    this->_incident = {};
    this->_attributes = node.attributes();
  }
  void operator=(Node&& node){
    this->_description = std::move(node._description);
    this->_incident = std::move(node._incident);
    this->_attributes = std::move(node._attributes);
  }

  const std::string& description() const {
    return this->_description;
  }

  std::string& description(){
    return this->_description;
  }

  const std::unordered_set<Edge*>& incident() const {
    return this->_incident;
  }

  std::unordered_set<Edge*>& incident() {
    return this->_incident;
  }
  const Edge* add_incident(Edge* edge){
    this->_incident.insert(edge);
    return edge;
  }


  const std::unordered_map<std::string, Attribute>& attributes() const {
    return this->_attributes;
  }

  std::unordered_map<std::string, Attribute>& attributes() {
    return this->_attributes;
  }

  std::pair<bool, Attribute> attribute(const std::string& attr) const {
    auto search = this->_attributes.find(attr);
    if(search == this->_attributes.end()){
      return {false, {fix, 0}};
    }
    return {true, search->second};
  }

  Attribute& attribute_throwing(const std::string& attr){
    auto search = this->_attributes.find(attr);
    if(search == this->_attributes.end()){
      std::stringstream text;
      text << "\"" << attr << "\" is not defined for Node " << this->description();
      throw std::range_error(text.str());
    }
    return search->second;
  }

  Attribute& add_attribute(const std::string attr_name, Attribute attr){
    auto [iter, success] = this->_attributes.insert({attr_name, attr});

    if(!success) {
      std::stringstream text;
      text << "\"" << attr << "\" is could not be added to Node " << this->description();
      throw std::range_error(text.str());
    }
    return iter->second;
  }

  std::unordered_set<Edge*> incoming();

  std::unordered_set<Edge*> outgoing();

/*
  bool is_target() const;

  bool is_source() const;
*/
  ~Node();

  // _debug_helpers
  void dump();
};

std::ostream& operator<<(std::ostream& os, const Node& n);
std::istream& operator>>(std::istream& is, Node& n);

#endif
