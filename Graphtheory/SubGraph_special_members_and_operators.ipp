/*
  subgraphs operate on members of master
    (this is ensured by the use of edges() and nodes() in Graph and our overwrite of these virtual functions in SubGraph).
  Hence we refrain from initializing the local _nodes and _edges members.
*/

SubGraph::SubGraph(Graph& graph){
  if(graph.obj_type() != MasterGraph) throw std::runtime_error("donating obj is not a Graph consturct");
  if(graph.is_borrowed()) throw std::runtime_error("graph already has active subgraphs");

  this->_supergraph = (SubGraph*) &graph;
  this->_node_restriction = graph._nodes;
  this->_edge_restrction = graph._edges;
  this->_node_ledger = {};
  this->_edge_ledger = {};
  this->_nodes = {};
  this->_edges = {};

  graph.set_borrowed_by(*this);
}

SubGraph::SubGraph(SubGraph& subgraph, const std::unordered_set<Node*>& node_restriction) : _supergraph(&subgraph), _node_restriction({}), _edge_restrction({}), _node_ledger({}), _edge_ledger({}) {
  /**
    calculates the standard graph restriction given by all edges spanned by node_restriction.
  */
  if(subgraph.obj_type() != SlaveSubGraph) throw std::runtime_error("donating obj is not a SubGraph. Cannot assign.");
  for(Node* n : node_restriction){
    if(!subgraph.contains(n)) throw std::invalid_argument("only allows construction of restriction that already have all restriction components present; missing node");
  }

  for(Node* n : node_restriction){
    this->borrow(n);
  }

  for(Edge* e : subgraph.local_edges()){
    if( this->contains(e->from()) && this->contains(e->to()) ) this->borrow(e);
  }
}

SubGraph::SubGraph(SubGraph& subgraph, const std::unordered_set<Node*>& node_restriction, const std::unordered_set<Edge*>& edge_restriction) : _supergraph(&subgraph), _node_restriction({}), _edge_restrction({}), _node_ledger({}), _edge_ledger({}) {
  /**
    calculates the minimal valid graph including both node_restriction and edge_restriction
  */
  if(subgraph.obj_type() != SlaveSubGraph) throw std::runtime_error("donating obj is not a SubGraph. Cannot assign.");
  for(Node* n : node_restriction){
    if(!subgraph.contains(n)) throw std::invalid_argument("only allows construction of restriction that already have all restriction components present; missing node");
  }
  for(Edge* e : edge_restriction){
    if(!subgraph.contains(e)) throw std::invalid_argument("only allows construction of restriction that already have all restriction components present; missing edge");
  }


  for(Node* n : node_restriction){
    this->borrow(n);
  }
  for(Edge* e : edge_restriction){
    this->borrow(e);
  }
}



SubGraph::~SubGraph(){
  try{
    this->settle(this->local_nodes());
  }catch(std::exception& except){
    assert(false);
  }

  if(this->supergraph()->obj_type() == MasterGraph) {
    Graph* master = dynamic_cast<Graph*>(this->supergraph());
    assert(master != nullptr);
    master->set_unborrowed();
  }
}
