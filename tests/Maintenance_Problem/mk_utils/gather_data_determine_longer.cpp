#include <fstream>
#include <vector>
#include <filesystem>
#include <cfloat>

#include "../../../Specialization/LP_Problems/Maintenance_Problem/Testing/maintenance_problem_testing.h"

// make sure the requested models actually exist in the parsed data. Otherwise this in csv(...) at the latest.
int main(int argc, char** argv){
  assert(argc == 4);
  std::filesystem::path top_level_path(argv[1]);

  std::string model(argv[2]);
  double time_threshold = std::atof(argv[3]);

  std::vector<Data> data_vector;
  gather_data(top_level_path, data_vector);


  size_t selected_set_size = 0;
  size_t base_set_size = 0;

  for(Data data : data_vector){
    auto model_search = data.derived_performance.find(model);
    assert(model_search != data.derived_performance.end());
    auto model_perf_vec = model_search->second;
    assert(model_perf_vec.size() == 1);
    assert(model_perf_vec[0].values_complete());
    if(model_perf_vec[0].time_in_sec.second >= time_threshold){
      ++selected_set_size;
    }
    ++base_set_size;
  }

  std::cout << model << " takes at least " << time_threshold << " seconds in " << (double)selected_set_size/base_set_size*100 << "% of the cases. In total these are " << selected_set_size << " out of " << base_set_size << "instances." << std::endl;
}
