#ifndef SEMIASSIGNMENT_BRANCH_H

#include <cassert>
#include <algorithm>
#include <cmath>
#include <iostream>

#include <scip/scip.h>
#include <scip/scipdefplugins.h>
#include <scip/struct_branch.h>
#include <scip/struct_var.h>

#include "semiassignment_hdlr.h"

extern "C" {
  SCIP_EXPORT
  SCIP_RETCODE SCIPincludeBranchruleSemiassign(
    SCIP*                  scip,
    int                    ncons,
    SCIP_CONS**             lor_constraints
  );
}
#endif
