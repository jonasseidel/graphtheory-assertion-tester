#ifndef MONOMIAL_H
#define MONOMIAL_H

#include <set>
#include <unordered_set>
#include <unordered_map>
#include <sstream>

#include "Variable.h"

class Monomial{
  std::set<Variable*> _vars;
public:
  Monomial(const Monomial& other);
  Monomial(Monomial&& other);
  Monomial(std::set<Variable*> vars = {});
  Monomial(const Monomial& other, const std::unordered_map<const Variable*, Variable*>& variable_lookup);

  double value();


  class less{
  public:
    bool operator()(const Monomial a, const Monomial b) const;
  };
  friend less;

  friend Monomial operator*(const Monomial a, const Monomial b);
  friend std::ostream& operator<<(std::ostream& os, const Monomial m);
};


#endif
