#include "integrality.h"
std::ostream& operator<<(std::ostream& os, const integrality& integr){
  switch(integr){
    case Continuous:
      os << "Continuous";
      break;
    case Integral:
      os << "Integral";
      break;
    default:
      throw std::runtime_error("invalid integrality type");
  }
  return os;
}
std::istream& operator>>(std::istream& is, integrality& integr){
  std::string curr;
  is >> curr;
  if(curr == "Continuous"){
    integr = Continuous;
  }else if(curr == "Integral"){
    integr = Integral;
  }else{
    throw std::invalid_argument("invalid integrality");
  }
  return is;
}
