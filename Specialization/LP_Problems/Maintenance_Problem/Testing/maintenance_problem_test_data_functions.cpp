#include "maintenance_problem_test_data_functions.h"

#include <cassert>
#include <filesystem>
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cmath>

#include "../../../../Common/io_format.h"
#include "../../../../Common/constants.h"

// Derived_Problem_Data

std::string Derived_Problem_Data::csv_columns(std::string prefix, std::string separator) {
  std::stringstream csv_columns_stream;
  csv_columns_stream << prefix << "number_of_edges" << separator
                      << prefix << "number_of_nodes" << separator
                      << prefix << "avg_incid_per_node" << separator
                      << prefix << "number_of_critical_edges" << separator
                      << prefix << "share_of_critical" << separator
                      << prefix << "number_of_epochs" << separator
                      << prefix << "path_length_lower_bound";
  return csv_columns_stream.str();
}

std::string Derived_Problem_Data::csv_data(std::string separator) const {
  if(!this->values_complete()) throw std::invalid_argument("can only output csv if completely initialized (Derived_Problem_Data)");
  std::stringstream csv_columns_stream;
  csv_columns_stream << this->number_of_edges.second << separator
                      << this->number_of_nodes.second << separator
                      << this->avg_incid_per_node.second << separator
                      << this->number_of_critical_edges.second << separator
                      << this->share_of_critical.second << separator
                      << this->number_of_epochs.second << separator
                      << this->path_length_lower_bound.second;
  return csv_columns_stream.str();
}

bool Derived_Problem_Data::values_complete() const {
  if(!this->number_of_edges.first) return false;
  if(!this->number_of_nodes.first) return false;
  if(!this->avg_incid_per_node.first) return false;
  if(!this->number_of_critical_edges.first) return false;
  if(!this->share_of_critical.first) return false;
  if(!this->number_of_epochs.first) return false;
  if(!this->path_length_lower_bound.first) return false;
  return true;
}

std::ostream& operator<<(std::ostream& os, const Derived_Problem_Data& prob_data){
  os << "Derived_Problem_Data {\n";

  PRINT_IF_DEF(os, prob_data, number_of_edges);
  PRINT_IF_DEF(os, prob_data, number_of_nodes);
  PRINT_IF_DEF(os, prob_data, avg_incid_per_node);
  PRINT_IF_DEF(os, prob_data, number_of_critical_edges);
  PRINT_IF_DEF(os, prob_data, share_of_critical);
  PRINT_IF_DEF(os, prob_data, number_of_epochs);
  PRINT_IF_DEF(os, prob_data, path_length_lower_bound);

  os << "}";

  return os;
}

std::istream& operator>>(std::istream& is, Derived_Problem_Data& prob_data){
  prob_data.number_of_edges.first = false;
  prob_data.number_of_nodes.first = false;
  prob_data.avg_incid_per_node.first = false;
  prob_data.number_of_critical_edges.first = false;
  prob_data.share_of_critical.first = false;
  prob_data.number_of_epochs.first = false;
  prob_data.path_length_lower_bound.first = false;

  std::string curr;
  is >> curr;
  IO_THROW(curr, "Derived_Problem_Data");
  is >> curr;
  IO_THROW(curr, "{");
  while(is >> curr && curr != "}"){
    IF_READ_VAR_SET_VAR_VALID_DIRECT(is, prob_data, number_of_edges, curr);
    IF_READ_VAR_SET_VAR_VALID_DIRECT(is, prob_data, number_of_nodes, curr);
    IF_READ_VAR_SET_VAR_VALID_DIRECT(is, prob_data, avg_incid_per_node, curr);
    IF_READ_VAR_SET_VAR_VALID_DIRECT(is, prob_data, number_of_critical_edges, curr);
    IF_READ_VAR_SET_VAR_VALID_DIRECT(is, prob_data, share_of_critical, curr);
    IF_READ_VAR_SET_VAR_VALID_DIRECT(is, prob_data, number_of_epochs, curr);
    IF_READ_VAR_SET_VAR_VALID_DIRECT(is, prob_data, path_length_lower_bound, curr);
  }

  return is;
}

// Derived_Performance_Data
std::string Derived_Performance_Data::csv_columns(std::string prefix, std::string separator) {
  std::stringstream csv_columns_stream;
  csv_columns_stream << prefix << "time_in_sec" << separator
                      << prefix << "number_of_bnb_runs" << separator
                      << prefix << "number_of_reopt_runs" << separator
                      << prefix << "number_of_nodes_explored" << separator
                      << prefix << "max_depth" << separator
                      << prefix << "dual_bound" << separator
                      << prefix << "primal_bound" << separator
                      << prefix << "gap" << separator
                      << prefix << "number_of_primal_sols";
  return csv_columns_stream.str();
}

std::string Derived_Performance_Data::csv_data(std::string separator) const {
  if(!this->values_complete()) throw std::invalid_argument("can only output csv if completely initialized (Derived_Performance_Data)");
  std::stringstream csv_string;
  csv_string << this->time_in_sec.second                << separator
              << this->number_of_bnb_runs.second        << separator
              << this->number_of_reopt_runs.second      << separator
              << this->number_of_nodes_explored.second  << separator
              << this->max_depth.second                 << separator
              << this->dual_bound.second                << separator
              << this->primal_bound.second              << separator
              << this->gap.second                       << separator
              << this->number_of_primal_sols.second;
  return csv_string.str();
}

std::string Derived_Performance_Data::csv_data_avg(const std::vector<Derived_Performance_Data>& exec_vector, std::string separator){
  // only call convert to csv if fully initialized
  double time_in_sec = 0;
  AVG_PERF_OVER_VEC(exec_vector, time_in_sec);

  double number_of_bnb_runs = 0;
  AVG_PERF_OVER_VEC(exec_vector, number_of_bnb_runs);

  double number_of_reopt_runs = 0;
  AVG_PERF_OVER_VEC(exec_vector, number_of_reopt_runs);

  double number_of_nodes_explored = 0;
  AVG_PERF_OVER_VEC(exec_vector, number_of_nodes_explored);

  double max_depth = 0;
  AVG_PERF_OVER_VEC(exec_vector, max_depth);

  double dual_bound = 0;
  AVG_PERF_OVER_VEC(exec_vector, dual_bound);

  double gap = 0;
  AVG_PERF_OVER_VEC(exec_vector, gap);

  double primal_bound = 0;
  AVG_PERF_OVER_VEC(exec_vector, primal_bound);

  double number_of_primal_sols = 0;
  AVG_PERF_OVER_VEC(exec_vector, number_of_primal_sols);

  std::stringstream csv_string;
  csv_string << time_in_sec                << separator
              << number_of_bnb_runs        << separator
              << number_of_reopt_runs      << separator
              << number_of_nodes_explored  << separator
              << max_depth                 << separator
              << dual_bound                << separator
              << primal_bound              << separator
              << gap                       << separator
              << number_of_primal_sols;
  return csv_string.str();
}

std::string Derived_Performance_Data::csv_data_median(const std::vector<Derived_Performance_Data>& exec_vector, std::string separator){
  // only call convert to csv if fully initialized
  double time_in_sec = 0;
  MEDIAN_PERF_OVER_VEC(exec_vector, time_in_sec);

  double number_of_bnb_runs = 0;
  MEDIAN_PERF_OVER_VEC(exec_vector, number_of_bnb_runs);

  double number_of_reopt_runs = 0;
  MEDIAN_PERF_OVER_VEC(exec_vector, number_of_reopt_runs);

  double number_of_nodes_explored = 0;
  MEDIAN_PERF_OVER_VEC(exec_vector, number_of_nodes_explored);

  double max_depth = 0;
  MEDIAN_PERF_OVER_VEC(exec_vector, max_depth);

  double dual_bound = 0;
  MEDIAN_PERF_OVER_VEC(exec_vector, dual_bound);

  double gap = 0;
  MEDIAN_PERF_OVER_VEC(exec_vector, gap);

  double primal_bound = 0;
  MEDIAN_PERF_OVER_VEC(exec_vector, primal_bound);

  double number_of_primal_sols = 0;
  MEDIAN_PERF_OVER_VEC(exec_vector, number_of_primal_sols);

  std::stringstream csv_string;
  csv_string << time_in_sec                << separator
  << number_of_bnb_runs        << separator
  << number_of_reopt_runs      << separator
  << number_of_nodes_explored  << separator
  << max_depth                 << separator
  << dual_bound                << separator
  << primal_bound              << separator
  << gap                       << separator
  << number_of_primal_sols;
  return csv_string.str();
}

bool Derived_Performance_Data::values_complete() const {
  if(!this->time_in_sec.first) return false;
  if(!this->number_of_bnb_runs.first) return false;
  if(!this->number_of_reopt_runs.first) return false;
  if(!this->number_of_nodes_explored.first) return false;
  if(!this->max_depth.first) return false;
  if(!this->dual_bound.first) return false;
  if(!this->primal_bound.first) return false;
  if(!this->gap.first) return false;
  if(!this->number_of_primal_sols.first) return false;
  return true;
}


std::ostream& operator<<(std::ostream& os, const Derived_Performance_Data& exec_data){
  os << "Derived_Exec_Data {\n";

  PRINT_IF_DEF(os, exec_data, time_in_sec);
  PRINT_IF_DEF(os, exec_data, number_of_bnb_runs);
  PRINT_IF_DEF(os, exec_data, number_of_reopt_runs);
  PRINT_IF_DEF(os, exec_data, number_of_nodes_explored);
  PRINT_IF_DEF(os, exec_data, max_depth);
  PRINT_IF_DEF(os, exec_data, dual_bound);
  PRINT_IF_DEF(os, exec_data, primal_bound);
  PRINT_IF_DEF(os, exec_data, gap);
  PRINT_IF_DEF(os, exec_data, number_of_primal_sols);

  os << "}";

  return os;
}

std::istream& operator>>(std::istream& is, Derived_Performance_Data& exec_data){
  exec_data.time_in_sec.first = false;
  exec_data.number_of_bnb_runs.first = false;
  exec_data.number_of_reopt_runs.first = false;
  exec_data.number_of_nodes_explored.first = false;
  exec_data.max_depth.first = false;
  exec_data.dual_bound.first = false;
  exec_data.primal_bound.first = false;
  exec_data.gap.first = false;
  exec_data.number_of_primal_sols.first = false;

  std::string curr;
  is >> curr;
  IO_THROW(curr, "Derived_Exec_Data");
  is >> curr;
  IO_THROW(curr, "{");
  while(is >> curr && curr != "}"){
    IF_READ_VAR_SET_VAR_VALID_DIRECT(is, exec_data, time_in_sec, curr);
    IF_READ_VAR_SET_VAR_VALID_DIRECT(is, exec_data, number_of_bnb_runs, curr);
    IF_READ_VAR_SET_VAR_VALID_DIRECT(is, exec_data, number_of_reopt_runs, curr);
    IF_READ_VAR_SET_VAR_VALID_DIRECT(is, exec_data, number_of_nodes_explored, curr);
    IF_READ_VAR_SET_VAR_VALID_DIRECT(is, exec_data, max_depth, curr);
    IF_READ_VAR_SET_VAR_VALID_DIRECT(is, exec_data, dual_bound, curr);
    IF_READ_VAR_SET_VAR_VALID_DIRECT(is, exec_data, primal_bound, curr);
    IF_READ_VAR_SET_VAR_VALID_DIRECT(is, exec_data, gap, curr);
    IF_READ_VAR_SET_VAR_VALID_DIRECT(is, exec_data, number_of_primal_sols, curr);
  }

  return is;
}

std::istream& operator>>(std::istream& is, std::vector<Derived_Performance_Data>& exec_data){
  std::string curr;
  is >> curr;
  IO_THROW(curr, "[");
  auto pos = is.tellg();
  while(is >> curr && curr != "]"){
    is.seekg(pos);
    exec_data.push_back(Derived_Performance_Data{});
    Derived_Performance_Data& back = exec_data.back();
    is >> back;
    pos = is.tellg();
  }

  return is;
}

std::istream& operator>>(std::istream& is, std::map<std::string, std::vector<Derived_Performance_Data>>& formulation_exec_data){
  std::string curr;
  is >> curr;
  while(curr == "formulation:"){
    std::string formulation_name;
    std::getline(is, formulation_name);
    if(formulation_name.size() > 0 && formulation_name.c_str()[0] == ' '){
      formulation_name = formulation_name.substr(1, std::string::npos);
    }
    std::vector<Derived_Performance_Data> exec_vector;
    is >> exec_vector;
    formulation_exec_data.insert({formulation_name, std::move(exec_vector)});
    is >> curr;
  }

  return is;
}



// Data

std::string Data::csv_columns_incl_pos(const axis_data& x_axis_data, const axis_data& y_axis_data, const std::vector<std::string>& models, std::string prefix, std::string separator){
  std::stringstream csv_string;
  csv_string << prefix << x_axis_data.name                                                 << separator
              << prefix << y_axis_data.name                                                << separator
              << prefix << "marked_for_inspection"                                         << separator
              << prefix << "path"                                                          << separator
              << prefix << "name"                                                          << separator
              << prefix << maintenance_problem_generator::csv_columns(prefix, separator)   << separator;
  std::stringstream derived_data_prefix;
  derived_data_prefix << "example_" << prefix;
  csv_string  << prefix << Derived_Problem_Data::csv_columns(derived_data_prefix.str(), separator);

  for(std::string model : models){
    std::stringstream concat_prefix;
    concat_prefix << prefix << model << "_";
    csv_string << separator << Derived_Performance_Data::csv_columns(concat_prefix.str(), separator);
  }
  return csv_string.str();
}

std::string Data::csv_data_avg_or_median_at_given_pos(const std::pair<double, double> position, const std::vector<Data>& data_vector, const axis_data& x_axis_data, const axis_data& y_axis_data, const std::vector<std::string>& models, bool avg_or_med, std::string separator) {
  std::stringstream csv_string;
  const Data& data = data_vector.front();
  bool marked = false;

  for(const Data& curr_data : data_vector){
    double xval = position.first;
    double yval = position.second;
    std::stringstream raster_entry_folder_name;
    raster_entry_folder_name << std::filesystem::path(curr_data.path).parent_path().filename().string();
    {

      // parsing folder name
      std::string curr;
      std::getline(raster_entry_folder_name, curr, ':');
      if(curr == "raster_pos"){
        // parse for x_axis_val
        std::getline(raster_entry_folder_name, curr, '=');
        if(curr != x_axis_data.name){
          throw std::invalid_argument("data doesn't plot on the provided axes");
        }
        std::getline(raster_entry_folder_name, curr, '_');
        if( position.first != (xval = std::stod(curr)) ) throw std::invalid_argument("incoherent position in data set");

        // parse for y_axis_val
        std::getline(raster_entry_folder_name, curr, '=');
        if(curr != y_axis_data.name){
          throw std::invalid_argument("data doesn't plot on the provided axes");
        }
        std::getline(raster_entry_folder_name, curr);
        if( position.second != (yval = std::stod(curr)) ) throw std::invalid_argument("incoherent position in data set");
      }else{
        throw std::invalid_argument("data doesn't belong to raster plot");
      }
    }

    marked = (marked || curr_data.marked);
    if(&curr_data == &data_vector.back()){
      csv_string << xval                                      << separator
                  << yval                                     << separator
                  << curr_data.marked                         << separator
                  << raster_entry_folder_name.str()           << separator
                  << curr_data.name                           << separator
                  << curr_data.generator.csv_data(separator)  << separator
                  << curr_data.derived_problem.csv_data(separator);
    }else{
      if(raster_entry_folder_name.str() != std::filesystem::path(curr_data.path).parent_path().filename().string()) std::cerr << "WARN: incoherent folder of data to be averaged" << std::endl;
      if(curr_data.generator != data_vector.front().generator) throw std::invalid_argument("incoherent generator detected when supposed to average");
    }
  }

  // concatenate execution data
  for(std::string model : models){
    std::vector<Derived_Performance_Data> aggr_exec_vector;

    for(const Data& curr_data : data_vector){
      auto model_search = curr_data.derived_performance.find(model);
      if(model_search == curr_data.derived_performance.end()) throw std::invalid_argument("model not contained for all data points");
      const std::vector<Derived_Performance_Data>& curr_exec_vector = model_search->second;
      aggr_exec_vector.insert(aggr_exec_vector.begin(), curr_exec_vector.begin(), curr_exec_vector.end());
    }

    if(avg_or_med){
      csv_string << separator
                  << Derived_Performance_Data::csv_data_avg(aggr_exec_vector, separator);
    }else{
      csv_string << separator
                  << Derived_Performance_Data::csv_data_median(aggr_exec_vector, separator);
    }
  }

  return csv_string.str();
}


std::string Data::csv_singular_data_incl_pos(const axis_data& x_axis_data, const axis_data& y_axis_data, const std::vector<std::string>& models, std::string separator) const {
  std::stringstream csv_string;

  std::string xval = "";
  std::string yval = "";
  std::filesystem::path data_path(this->path);
  std::stringstream raster_entry_folder_name;
  raster_entry_folder_name << data_path.parent_path().filename().string();

  // parsing folder name
  std::string curr;
  std::getline(raster_entry_folder_name, curr, ':');
  if(curr == "raster_pos"){
    // parse for x_axis_val
    std::getline(raster_entry_folder_name, curr, '=');
    if(curr != x_axis_data.name){
      throw std::invalid_argument("data doesn't plot on the provided axes");
    }
    std::getline(raster_entry_folder_name, xval, '_');

    // parse for y_axis_val
    std::getline(raster_entry_folder_name, curr, '=');
    if(curr != y_axis_data.name){
      throw std::invalid_argument("data doesn't plot on the provided axes");
    }
    std::getline(raster_entry_folder_name, yval);
  }else{
    throw std::invalid_argument("data doesn't belong to raster plot");
  }

  csv_string << xval                                     << separator
              << yval                                     << separator
              << this->marked                               << separator
              << this->path                                << separator
              << this->name                                << separator
              << this->generator.csv_data(separator)       << separator
              << this->derived_problem.csv_data(separator);
  for(std::string model : models){
    auto exec_vector = this->derived_performance.find(model)->second;

    csv_string << separator
                << Derived_Performance_Data::csv_data_avg(exec_vector, separator);
  }

  return csv_string.str();
}

std::string Data::csv_data_avg_unknown_but_coherent_pos(const std::vector<Data>& data_vector, const axis_data& x_axis_data, const axis_data& y_axis_data, const std::vector<std::string>& models, std::string separator) {
  std::stringstream csv_string;
  const Data& data = data_vector.front();

  std::string xval = "";
  std::string yval = "";
  std::filesystem::path data_path(data.path);
  {
    std::stringstream raster_entry_folder_name;
    raster_entry_folder_name << data_path.parent_path().filename().string();

    // parsing folder name
    std::string curr;
    std::getline(raster_entry_folder_name, curr, ':');
    if(curr == "raster_pos"){
      // parse for x_axis_val
      std::getline(raster_entry_folder_name, curr, '=');
      if(curr != x_axis_data.name){
        throw std::invalid_argument("data doesn't plot on the provided axes");
      }
      std::getline(raster_entry_folder_name, xval, '_');

      // parse for y_axis_val
      std::getline(raster_entry_folder_name, curr, '=');
      if(curr != y_axis_data.name){
        throw std::invalid_argument("data doesn't plot on the provided axes");
      }
      std::getline(raster_entry_folder_name, yval);
    }else{
      throw std::invalid_argument("data doesn't belong to raster plot");
    }
  }

  csv_string << data.marked                          << separator
              << data_path.parent_path().string()    << separator
              << data.name                           << separator
              << xval                                << separator
              << yval                                << separator
              << data.generator.csv_data(separator)  << separator
              << data.derived_problem.csv_data(separator);
  // check that data is grouped sensibly

  for(const Data& curr_data : data_vector){
    std::string curr_xval = "";
    std::string curr_yval = "";
    std::filesystem::path curr_data_path(curr_data.path);
    std::stringstream curr_raster_entry_folder_name;
    curr_raster_entry_folder_name << curr_data_path.parent_path().filename().string();

    // parsing folder name
    std::string curr;
    std::getline(curr_raster_entry_folder_name, curr, ':');
    if(curr == "raster_pos"){
      // parse for x_axis_val
      std::getline(curr_raster_entry_folder_name, curr, '=');
      if(curr != x_axis_data.name){
        throw std::invalid_argument("data doesn't plot on the provided axes");
      }
      std::getline(curr_raster_entry_folder_name, curr_xval, '_');

      // parse for y_axis_val
      std::getline(curr_raster_entry_folder_name, curr, '=');
      if(curr != y_axis_data.name){
        throw std::invalid_argument("data doesn't plot on the provided axes");
      }
      std::getline(curr_raster_entry_folder_name, curr_yval);
    }else{
      throw std::invalid_argument("data doesn't belong to raster plot");
    }

    if(xval != curr_xval) throw std::invalid_argument("incoherent position on raster");
    if(yval != curr_yval) throw std::invalid_argument("incoherent position on raster");
  }

  // concatenate execution data
  for(std::string model : models){
    std::vector<Derived_Performance_Data> aggr_exec_vector;

    for(const Data& curr_data : data_vector){
      const std::vector<Derived_Performance_Data>& curr_exec_vector = curr_data.derived_performance.find(model)->second;
      aggr_exec_vector.insert(aggr_exec_vector.begin(), curr_exec_vector.begin(), curr_exec_vector.end());
    }

    csv_string << separator
                << Derived_Performance_Data::csv_data_avg(aggr_exec_vector, separator);
  }

  return csv_string.str();
}

bool Data::values_complete() const {
  for(auto& [formulation, exec_vector] : this->derived_performance){
    for(const Derived_Performance_Data& perf_data : exec_vector){
      if(!perf_data.values_complete()) return false;
    }
  }
  return this->derived_problem.values_complete();
}

std::istream& operator>>(std::istream& is, Data& data){
  data = Data{};
  std::string curr;
  is >> curr;
  IO_THROW(curr, "Performance_Tester_Data");
  is >> curr;
  IO_THROW(curr, "{");
  while( (is >> curr) && (curr != "}") ){
    IF_READ_VAR_SET_VAR_DIRECT(is, data, marked, curr);
    IF_READ_VAR_SET_VAR_GETLINE(is, data, path, curr);
    if(data.path.size() > 0 && data.path.c_str()[0] == ' '){
      data.path = data.path.substr(1, std::string::npos);
    }
    IF_READ_VAR_SET_VAR_GETLINE(is, data, name, curr);
    if(data.name.size() > 0 && data.name.c_str()[0] == ' '){
      data.name = data.name.substr(1, std::string::npos);
    }
    IF_READ_VAR_SET_VAR_DIRECT(is, data, generator, curr);
    IF_READ_VAR_SET_VAR_DIRECT(is, data, mp, curr);
    IF_READ_VAR_SET_VAR_DIRECT(is, data, derived_problem, curr);
    IF_READ_VAR_SET_VAR_DIRECT(is, data, derived_performance, curr);
  }

  return is;
}

std::ostream& operator<<(std::ostream& os, const Data& data){
  os << "Performance_Tester_Data {\n"
      << "marked: "                 << data.marked << "\n"
      << "path: "                   << data.path << "\n"
      << "name: "                   << data.name << "\n"
      << "generator: "              << data.generator << "\n"
      << "mp: "                     << data.mp << "\n"
      << "derived_problem: "        << data.derived_problem << "\n"
      << "derived_performance: \n";
  for(auto& [prob_name, exec_vector] : data.derived_performance){
    os << "formulation: " << prob_name << "\n"
        << "[\n";
    for(const Derived_Performance_Data& perf_data : exec_vector){
      os << perf_data << "\n";
    }
    os << "] \n";
  }
  os << "}";

  return os;
}
