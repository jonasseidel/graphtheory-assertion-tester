#ifndef MAINTENANCE_PROBLEM_BASIC_CUT_SEPA_H

#include <iostream>
#include <vector>

#include "scip/scip.h"
#include <scip/scipdefplugins.h>

#include "nmp_bulksepa_conshdlr.h"

extern "C"{
  SCIP_RETCODE SCIPincludeBenderscutBasicSepa(
    SCIP*                 scip,
    SCIP_BENDERS*         benders
  );
}

#endif
