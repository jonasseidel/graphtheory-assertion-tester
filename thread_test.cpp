#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <iostream>

void* test(void* param){
  sleep(1);
  std::cout << "test" << std::endl;
  return NULL;
}

int main(){
  pthread_t thread;
  pthread_create(&thread, NULL, test, NULL);
  std::cout << pthread_cancel(thread) << std::endl;
  pthread_setcanceltype(PTHREAD_CANCEL_ASYNCHRONOUS, NULL);
  std::cout << pthread_join(thread, NULL) << std::endl;
  std::cout << pthread_join(thread, NULL) << std::endl;
  std::cout << pthread_join(thread, NULL) << std::endl;
  sleep(2);
  std::cout << "end" << std::endl;
  sleep(1);
  std::cout << "end2" << std::endl;
  int count = 50;
  for(int i = 0; i < count; i++){
    std::cout << "." << std::endl;
  }
}
