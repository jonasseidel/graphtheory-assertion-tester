#include <iostream>
#include <fstream>
#include <cstdlib>
#include <cassert>
#include <vector>
#include <climits>

#include "./Specialization/LP_Problems/Maintenance_Problem/Maintenance_Problem.h"
#include "./Specialization/LP_Problems/Maintenance_Problem/maintenance_problem_generator.h"


void probe(maintenance_problem_generator& mpg){
  Maintenance_Problem mp;
  while(true){
    system("killall graph_display");
    std::cout << "generating problem" << std::endl;
    mp = mpg.next();
    std::cout << "generation finished" << std::endl;

    jmp:

    std::cout << "writing to disk" << std::endl;
    std::ofstream ofs_graph("./.data/Maintenance_Problem/mp.mp");
    ofs_graph << mp << std::endl;
    ofs_graph.close();
    std::ofstream ofs_program("./.data/Maintenance_Problem/mp.lp");
    ofs_program << static_cast<Linear_Program>(mp) << std::endl;
    ofs_program.close();
    std::ofstream ofs_netw("./.data/Maintenance_Problem/mp.netw");
    ofs_netw << mp.network() << std::endl;
    ofs_netw.close();
    std::cout << mp.network() << std::endl;
    std::cout << "finished write" << std::endl;
    system("cd ../display/graph_display/ && (./graph_display --file ../../discrete_optimization_library/.data/Maintenance_Problem/mp.netw &) && cd ../../discrete_optimization_library");

    std::cout << "generating SCIP model" << std::endl;
    std::vector<SCIP*> scips = mp.all_computational_models();
    for(SCIP* scip : scips){
      std::cout << "---------------->" << SCIPgetProbName(scip) << ":" << std::endl;
      SCIPsolve(scip);
    }

    for(SCIP* scip : scips){
      SCIPfree(&scip);
      //std::cin.ignore();
    }
    system("polymake --script .data/Maintenance_Problem/pm_script_lp2facets");
    std::cin.ignore();
  }
}


int main(int argc, char** argv){
  // cluster, connecting edges critical, simulate multiple if ex
  //maintenance_problem_generator mpg (
  //  random_graph_generator(
  //                // shelter_orphans   only_tip_fringes  only_tip_extreme_layer
  //    tipping_policy(false,             true,             false),
  //                  //   number_of_nodes  number_of_steps   node_attribute_generator
  //    uniform_node_steps(5,              1,                {{}}),
  //                  //          number_of_edges fuzzing_distance_from fuzzing_distance_to edge_attribute_generator              at_least_strongly_connected at_least_weakly_connected acyclic simple anti_reflexive
  //    uniform_edge_step_fuzzing(7,             0,                    0,                  {{{"Upper", {fix, Integral, 1, 100}}}}, false,                     false,                     true,   true, true),
  //    tipping_policy(true,             true,             false),
  //    uniform_node_steps(5,              1,                {{}}),
  //    uniform_edge_step_fuzzing(6,             0,                    0,                  {{{"Upper", {fix, Integral, 1, 100}}}}, false,                     false,                     true,   true, true),
  //    {{"Flow", Attribute(max, 0)},{"Upper", Attribute(fix, 1)}, {"Selected", Attribute(fix, 0)}, {"Edgepotential", Attribute(min, 0)}},
  //    {{"Nodepotential", Attribute(min, 0)}},
  //    {{{"Upper", {fix, Integral, INT_MAX, INT_MAX}}}},
  //    {},
  //    {{{"Upper", {fix, Integral, 1, 100}}}}
  //  ),
  //  //                                   critical_edge_candidates
  //  3, 1, network_connectors
  //);

  // no nested, broad, simulate multiple forced ex
  //maintenance_problem_generator mpg (
  //  random_graph_generator(
  //                // shelter_orphans   only_tip_fringes  only_tip_extreme_layer
  //    tipping_policy(false,             false,             true),
  //                  //   number_of_nodes  number_of_steps   node_attribute_generator
  //    uniform_node_steps(15,              5,                {{}}),
  //                  //          number_of_edges fuzzing_distance_from fuzzing_distance_to edge_attribute_generator              at_least_strongly_connected at_least_weakly_connected acyclic simple anti_reflexive
  //    uniform_edge_step_fuzzing(30,             0,                    1,                  {{{"Upper", {fix, Integral, 1, 100}}}}, false,                     false,                     true,   true, true),
  //    //             shelter_orphans   only_tip_fringes  only_tip_extreme_layer
  //    tipping_policy(true,             true,             false),
  //    //                 number_of_nodes  number_of_steps   node_attribute_generator
  //    uniform_node_steps(1,              1,                {{}}),
  //    //                        number_of_edges fuzzing_distance_from fuzzing_distance_to edge_attribute_generator              at_least_strongly_connected at_least_weakly_connected acyclic simple anti_reflexive
  //    uniform_edge_step_fuzzing(0,             0,                    0,                  {{{"Upper", {fix, Integral, 1, 100}}}}, false,                     false,                     true,   true, true),
  //    {{"Flow", Attribute(max, 0)},{"Upper", Attribute(fix, 1)}, {"Selected", Attribute(fix, 0)}, {"Edgepotential", Attribute(min, 0)}},
  //    {{"Nodepotential", Attribute(min, 0)}},
  //    {{{"Upper", {fix, Integral, INT_MAX, INT_MAX}}}},
  //    {},
  //    {{{"Upper", {fix, Integral, 1, 100}}}}
  //  ),
  //  //  number_of_epochs       share_of_critical     critical_edge_candidates
  //  3,                     1,                    network_connectors
  //);

  // basic low incident
  //maintenance_problem_generator mpg(
  //  random_graph_generator(
  //                // shelter_orphans   only_tip_fringes  only_tip_extreme_layer
  //    tipping_policy(false),
  //                  //   number_of_nodes  number_of_steps   node_attribute_generator
  //    uniform_node_steps(20,              1,                {{}}),
  //                  //          number_of_edges fuzzing_distance_from fuzzing_distance_to edge_attribute_generator              at_least_strongly_connected at_least_weakly_connected acyclic simple anti_reflexive
  //    uniform_edge_step_fuzzing(60,             0,                    1,                  {{{"Upper", {fix, Integral, 1, 100}}}}, false,                     false,                     true,   true, true),
  //    tipping_policy(true,             true,             false),
  //    //                 number_of_nodes number_of_steps   node_attribute_generator
  //    uniform_node_steps(1,              1,                {{}}),
  //    //                        number_of_edges fuzzing_distance_from fuzzing_distance_to edge_attribute_generator              at_least_strongly_connected at_least_weakly_connected acyclic simple anti_reflexive
  //    uniform_edge_step_fuzzing(0,             0,                    0,                  {{{"Upper", {fix, Integral, 1, 100}}}}, false,                     false,                     true,   true, true),
  //    {{"Flow", Attribute(max, 0)},{"Upper", Attribute(fix, 1)}, {"Selected", Attribute(fix, 0)}, {"Edgepotential", Attribute(min, 0)}},
  //    {{"Nodepotential", Attribute(min, 0)}},
  //    {{{"Upper", {fix, Integral, 1, 100}}}},
  //    {},
  //    {{{"Upper", {fix, Integral, 1, 100}}}}
  //  ),
  //    //                                   critical_edge_candidates
  //  3, 1, everywhere
  //);


  //// mpg from decentralization test (never used)
  //const double subnetwork_nnodes = 40;
  //const double nincidences = 3;
  //const size_t decentralization = 7;
  //const size_t nepochs = 3;

  //maintenance_problem_generator mpg(
  //  random_graph_generator(
  //                // shelter_orphans   only_tip_fringes  only_tip_extreme_layer
  //    tipping_policy(true,             true,             true),
  //                  //   number_of_nodes                number_of_steps   node_attribute_generator
  //    uniform_node_steps(decentralization,              1,                {{}}),
  //                  //          number_of_edges fuzzing_distance_from fuzzing_distance_to edge_attribute_generator              at_least_strongly_connected at_least_weakly_connected acyclic simple anti_reflexive
  //    uniform_edge_step_fuzzing(decentralization*(nincidences/2),         0,                    0,                  {{{"Upper", {fix, Integral, 1, 100}}}}, false,                     false,                     true,   true, true),
  //    tipping_policy(true,             true,             true),
  //    //                 number_of_nodes                number_of_steps   node_attribute_generator
  //    uniform_node_steps(round(subnetwork_nnodes/decentralization),              1,                {{}}),
  //    //                        number_of_edges fuzzing_distance_from fuzzing_distance_to edge_attribute_generator              at_least_strongly_connected at_least_weakly_connected acyclic simple anti_reflexive
  //    uniform_edge_step_fuzzing(round((subnetwork_nnodes/decentralization)*(nincidences/2)),             0,                    0,                  {{{"Upper", {fix, Integral, 1, 100}}}}, false,                     false,                     true,   true, true),
  //    {{"Flow", Attribute(max, 0)},{"Upper", Attribute(fix, 1)}, {"Selected", Attribute(fix, 0)}, {"Edgepotential", Attribute(min, 0)}},
  //    {{"Nodepotential", Attribute(min, 0)}},
  //    {{{"Upper", {fix, Integral, 1, 100}}}},
  //    {},
  //    {{{"Upper", {fix, Integral, 1, 100}}}}
  //  ),
  //  //                                   critical_edge_candidates
  //  nepochs, 1, network_connectors
  //);

  // mpg decentralization test aggregated_networks
  //const double nnodes = 40;
  //const double nincidences = 2;

  //const size_t decentralization = 5;
  //const size_t nepochs = 3;

  //maintenance_problem_generator mpg(
  //  random_graph_generator(
  //                // shelter_orphans   only_tip_fringes  only_tip_extreme_layer
  //    tipping_policy(true,             true,             true),
  //                  //   number_of_nodes                number_of_steps   node_attribute_generator
  //    uniform_node_steps(decentralization,              1,                {{}}),
  //                  //          number_of_edges fuzzing_distance_from fuzzing_distance_to edge_attribute_generator              at_least_strongly_connected at_least_weakly_connected acyclic simple anti_reflexive
  //    uniform_edge_step_fuzzing(decentralization*(nincidences/2),         0,                    0,                  {{{"Upper", {fix, Integral, 1, 100}}}}, false,                     false,                     true,   true, true),
  //    tipping_policy(true,             true,             true),
  //    //                 number_of_nodes                number_of_steps   node_attribute_generator
  //    uniform_node_steps(round(nnodes/decentralization),              1,                {{}}),
  //    //                        number_of_edges fuzzing_distance_from fuzzing_distance_to edge_attribute_generator              at_least_strongly_connected at_least_weakly_connected acyclic simple anti_reflexive
  //    uniform_edge_step_fuzzing(round((nnodes/decentralization)*(nincidences/2)),             0,                    0,                  {{{"Upper", {fix, Integral, 1, 100}}}}, false,                     false,                     true,   true, true),
  //    {{"Flow", Attribute(max, 0)},{"Upper", Attribute(fix, 1)}, {"Selected", Attribute(fix, 0)}, {"Edgepotential", Attribute(min, 0)}},
  //    {{"Nodepotential", Attribute(min, 0)}},
  //    {{{"Upper", {fix, Integral, 1, 100}}}},
  //    {},
  //    {{{"Upper", {fix, Integral, 1, 100}}}}
  //  ),
  //  //                                   critical_edge_candidates
  //  nepochs, 1, aggregated_networks
  //);


  // cut networks
  size_t nedges = 3;
  maintenance_problem_generator mpg(
    random_graph_generator(
                  // shelter_orphans   only_tip_fringes  only_tip_extreme_layer
      tipping_policy(false),
                    //   number_of_nodes                                        number_of_steps   node_attribute_generator
      uniform_node_steps(2,              2,                {{}}),
                    //          number_of_edges fuzzing_distance_from fuzzing_distance_to edge_attribute_generator              at_least_strongly_connected at_least_weakly_connected acyclic simple anti_reflexive
      uniform_edge_step_fuzzing(nedges,         1,                    1,                  {{{"Upper", {fix, Integral, 1, 1}}}}, false,                     false,                     true,   false, true),
      tipping_policy(true,             true,             false),
      //                 number_of_nodes number_of_steps   node_attribute_generator
      uniform_node_steps(1,              1,                {{}}),
      //                        number_of_edges fuzzing_distance_from fuzzing_distance_to edge_attribute_generator              at_least_strongly_connected at_least_weakly_connected acyclic simple anti_reflexive
      uniform_edge_step_fuzzing(0,             0,                    0,                  {{{"Upper", {fix, Integral, 1, 1}}}}, false,                     false,                     true,   true, true),
      {{"Flow", Attribute(max, 0)},{"Upper", Attribute(fix, 1)}, {"Selected", Attribute(fix, 0)}, {"Edgepotential", Attribute(min, 0)}},
      {{"Nodepotential", Attribute(min, 0)}},
      {{{"Upper", {fix, Integral, 1, 100}}}},
      {},
      {{{"Upper", {fix, Integral, 1, 100}}}}
    ),
    //                                   critical_edge_candidates
    2, 1, everywhere
  );

  if(argc > 1){
    std::ifstream mp_stream (argv[1]);
    Maintenance_Problem mp(mp_stream);

    std::cout << "writing to disk" << std::endl;
    std::ofstream ofs_graph("./.data/Maintenance_Problem/mp.mp");
    ofs_graph << mp << std::endl;
    ofs_graph.close();
    std::ofstream ofs_program("./.data/Maintenance_Problem/mp.lp");
    ofs_program << static_cast<Linear_Program>(mp) << std::endl;
    ofs_program.close();
    std::ofstream ofs_netw("./.data/Maintenance_Problem/mp.netw");
    ofs_netw << mp.network() << std::endl;
    ofs_netw.close();
    std::cout << mp.network() << std::endl;
    std::cout << "finished write" << std::endl;
    system("cd ../display/graph_display/ && (./graph_display --file ../../discrete_optimization_library/.data/Maintenance_Problem/mp.netw &) && cd ../../discrete_optimization_library");

    std::cout << "generating SCIP model" << std::endl;
    std::vector<SCIP*> scips = mp.all_computational_models();
    for(SCIP* scip : scips){
      std::cout << "---------------->" << SCIPgetProbName(scip) << ":" << std::endl;
      SCIPsolve(scip);
    }

    for(SCIP* scip : scips){
      SCIPfree(&scip);
      //std::cin.ignore();
    }
    //system("polymake --script .data/Maintenance_Problem/pm_script_lp2facets mp");
    std::cin.ignore();
  }

  probe(mpg);
}
