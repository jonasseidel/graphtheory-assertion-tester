CXX := g++-10
CXXFLAGS := -O0 -g -std=c++20 #-Wall -Wextra -Wpedantic
LIBSCIPOPT := /usr/lib/libscip.so.7.0.2.0
LIBORTOOLS := /usr/lib/libortools.so



GRAPH_DEP := $(patsubst %.cpp,%.o,$(shell find -type f -wholename "./Graphtheory/*"))
LP_DEP := $(patsubst %.cpp,%.o,$(shell find -type f -wholename "./Linear_Programming/*"))
LP_PROBLEMS_DEP := $(patsubst %.cpp,%.o,$(shell find -type f -wholename "./Specialization/LP_Problems/*" ! -wholename "*retired*"))
COMMON_DEP := $(patsubst %.cpp,%.o,$(shell find -type f -wholename "./Common_Types/*"))


GRAPH_OUT := $(patsubst %.cpp,%.o,$(shell find -wholename "./Graphtheory/*.cpp"))
LP_OUT := $(filter-out %lp_generator.o,$(patsubst %.cpp,%.o,$(shell find -wholename "./Linear_Programming/*.cpp")))
LP_PROBLEMS_OUT := $(patsubst %.cpp,%.o,$(shell find -wholename "./Specialization/LP_Problems/*.cpp" ! -wholename "*retired*"))
COMMON_OUT := $(patsubst %.cpp,%.o,$(shell find -wholename "./Common/*.cpp"))
TESTS_OUT := $(patsubst %.cpp,%.o,$(shell find -wholename "./tests/*.cpp" | grep -v "probe_test.cpp"))


targets := graph_test ass_test linear_program_test maintenance_problem_test benders_test scip_test preprocessor_test $(TESTS_OUT:.o=)
all: $(targets)
tests: $(TESTS_OUT:.o=)

OUTS :=

#Graph Test
graph_test: graph_test.o $(GRAPH_OUT) $(COMMON_OUT)
	$(CXX) $(CXXFLAGS) $^ -o $@

graph_test.o: graph_test.cpp $(GRAPH_DEP) $(COMMON_DEP)
	$(CXX) $(CXXFLAGS) -c $< -o $@



# Assertion Test
ass_test: ass_test.o $(GRAPH_OUT) $(COMMON_OUT)
	$(CXX) $(CXXFLAGS) $^ -o $@

ass_test.o: assertion_test.cpp $(GRAPH_DEP) $(COMMON_DEP)
	$(CXX) $(CXXFLAGS) -c $< -o $@



# Linear Program Test
linear_program_test: linear_program_test.o $(LP_OUT) $(LIBSCIPOPT) $(COMMON_OUT)
	$(CXX) $(CXXFLAGS) $^ -o $@

linear_program_test.o: linear_programming_test.cpp ./Linear_Programming/Linear_Program.o
	$(CXX) $(CXXFLAGS) -c $< -o $@


maintenance_problem_test: maintenance_problem_test.o $(LP_PROBLEMS_OUT) $(LP_OUT) $(GRAPH_OUT) Linear_Programming/lp_generator.o $(LIBSCIPOPT) $(LIBORTOOLS) $(COMMON_OUT)
	$(CXX) $(CXXFLAGS) $^ -o $@ -lpthread

maintenance_problem_test.o: maintenance_problem_test.cpp ./Specialization/LP_Problems/Maintenance_Problem/maintenance_problem_generator.o $(GRAPH_DEP) $(COMMON_DEP) $(LP_DEP) $(LP_PROBLEMS_DEP)
	$(CXX) $(CXXFLAGS) -c $< -o $@

benders_test: benders_test.o $(LIBSCIPOPT)
	$(CXX) $(CXXFLAGS) $^ -o $@

benders_test.o: benders_test.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

scip_test: scip_test.o $(LIBSCIPOPT)
	$(CXX) $(CXXFLAGS) $^ -o $@

scip_test.o: scip_test.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

preprocessor_test: preprocessor_test.o
	$(CXX) $(CXXFLAGS) $^ -o $@

preprocessor_test.o: preprocessor_test.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

thread_test: thread_test.o
	$(CXX) $(CXXFLAGS) $^ -o $@ -pthread

thread_test.o: thread_test.cpp
	$(CXX) $(CXXFLAGS) -c $< -o $@

.SECONDEXPANSION: TESTING
$(TESTS_OUT:.o=): $$(addsuffix .o, $$@) $(LP_PROBLEMS_OUT) $(LP_OUT) $(GRAPH_OUT) Linear_Programming/lp_generator.o $(LIBSCIPOPT) $(LIBORTOOLS) $(COMMON_OUT)
	$(CXX) $(CXXFLAGS) $^ -o $@ -lpthread

.SECONDEXPANSION: TESTING_O
$(TESTS_OUT) probe_test.o: $$(patsubst %.o,%.cpp,$$@)
	$(CXX) $(CXXFLAGS) -c $< -o $@

#gather_data_to_csv: gather_data_to_csv.o $(LP_PROBLEMS_OUT) $(LP_OUT) $(GRAPH_OUT) Linear_Programming/lp_generator.o $(LIBSCIPOPT) $(LIBORTOOLS) $(COMMON_OUT)
#	$(CXX) $(CXXFLAGS) $^ -o $@
#
#gather_data_to_csv.o: gather_data_to_csv.cpp
#	$(CXX) $(CXXFLAGS) -c $< -o $@


# Linear Programming Folder Object Files
.SECONDEXPANSION:LP
Graph := ./Graphtheory/
LP := ./Linear_Programming/
Common := ./Common_Types/
Linear_Program_dep := Polyeder.o
Polyeder_dep := Constraint.o
Constraint_dep := Variable.o Coefficient.o
Coefficient_dep := Variable.o Monomial.o
Monomial_dep := Variable.o
maintenance_problem_generator_dep := Maintenance_Problem.o
Maintenance_Problem_crossdep :=  Linear_Programming/Linear_Program.o $(GRAPH_DEP)
$(GRAPH_OUT) $(LP_OUT) $(LP_PROBLEMS_OUT) $(COMMON_OUT): $$(patsubst %.o,%.cpp,$$@) $$(patsubst %.o,%.h,$$@) $$(addprefix $$(@D)/,$$($$(patsubst %.o,%_dep,$$(@F)))) $$($$(patsubst %.o,%_crossdep,$$(@F))) #$$(addprefix $$(@D)/,$$(filter-out %include,$$(shell grep "include \"" $$(patsubst %.o,%.h,$$@) | tr -d '\"')))
	$(CXX) $(CXXFLAGS) -c $(patsubst %.o, %.cpp, $@) -o $@



.PHONY: clean clean_tests

clean:
	rm -f $(targets) a.out $(shell find -name "*.o") $(shell find -name "*.gch")

clean_tests:
	rm -f $(shell find -wholename "./tests/*.o") $(TESTS_OUT:.o=)
