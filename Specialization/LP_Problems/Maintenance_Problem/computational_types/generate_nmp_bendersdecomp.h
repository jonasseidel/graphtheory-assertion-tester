#ifndef GENERATE_NMP_BENDERSDECOMP_H

#include "../Maintenance_Problem.h"


SCIP* generate_nmp_bendersdecomp(Maintenance_Problem& nmp, SCIP* basic_scip, SCIP_SOL* opt);


#endif
