#include "node_steps_plugin.h"
#include "../../../Common/io_format.h"

// enumerate all node_steps_plugin derivatives
#include "uniform_node_steps.h"


void node_step_plugin::operator=(const node_step_plugin& other){
  this->_initialized = false;
  this->_number_of_nodes = other._number_of_nodes;
  this->_number_of_steps = other._number_of_steps;
  this->_node_attribute_generator = other._node_attribute_generator;
  this->_get_node_step_plugin_type = other._get_node_step_plugin_type;
  this->_add_node_steps = other._add_node_steps;
  this->_init = other._init;
  this->init();
}
void node_step_plugin::operator=(node_step_plugin&& other){
  this->_initialized = false;
  this->_number_of_nodes = std::move(other._number_of_nodes);
  this->_number_of_steps = std::move(other._number_of_steps);
  this->_node_attribute_generator = std::move(other._node_attribute_generator);
  this->_get_node_step_plugin_type = std::move(other._get_node_step_plugin_type);
  this->_add_node_steps = std::move(other._add_node_steps);
  this->_init = std::move(other._init);
  this->init();
}


std::string node_step_plugin::csv_columns(std::string prefix, std::string separator){
  std::stringstream csv_columns_stream;
  csv_columns_stream << prefix << "number_of_nodes" << separator
                      << prefix << "number_of_steps" << separator
                      << random_attribute_generator::csv_columns(prefix, separator);
  return csv_columns_stream.str();
}
std::string node_step_plugin::csv_data(std::string separator) const {
  std::stringstream csv_data;
  csv_data << this->_number_of_nodes << separator
            << this->_number_of_steps << separator
            << this->_node_attribute_generator.csv_data(separator);
  return csv_data.str();
}

bool node_step_plugin::operator==(const node_step_plugin& other) const {
  if(this->get_node_step_plugin_type() != other.get_node_step_plugin_type()) return false;
  if(this->_number_of_nodes != other._number_of_nodes ) return false;
  if(this->_number_of_steps != other._number_of_steps ) return false;
  if(this->_node_attribute_generator != other._node_attribute_generator ) return false;
  return true;
}

node_step_plugin::node_step_plugin(std::istream& is){
  std::string curr;
  is >> curr;
  IO_THROW(curr, "node_step_plugin");
  is >> curr;
  IO_THROW(curr, "{");
  std::string node_step_plugin_type;
  is >> node_step_plugin_type;
  is >> this->_number_of_nodes;
  is >> this->_number_of_steps;
  is >> this->_node_attribute_generator;
  is >> curr;
  IO_THROW(curr, "}");
  if(!is) throw std::invalid_argument("stream did not contain complete node_step_plugin object");

  if(node_step_plugin_type == "uniform_node_steps"){
    *this = uniform_node_steps(this->_number_of_nodes, this->_number_of_steps, this->_node_attribute_generator);
  }else{
    throw std::range_error("you need to manually extend the node_steps_plugin read operator to support your plugins");
  }
}

std::ostream& operator<<(std::ostream& os, const node_step_plugin& node_step_gen){
  os << "node_step_plugin" << " {" << "\n"
      << node_step_gen.get_node_step_plugin_type() << "\n"
      << node_step_gen._number_of_nodes << " "
      << node_step_gen._number_of_steps << "\n"
      << node_step_gen._node_attribute_generator
      << " }";
  return os;
}

std::istream& operator>>(std::istream& is, node_step_plugin& node_step_gen){
  node_step_gen = node_step_plugin(is);
  return is;
}
