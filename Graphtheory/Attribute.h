#ifndef ATTRIBUTE_H
#define ATTRIBUTE_H

#include <unordered_map>
#include <string>
#include <iostream>
#include <stdexcept>

#include "../Common/integrality.h"
#include "../Common/opt_dir.h"

class Attribute{
  opt_dir _dir;
  integrality _integrality;
  double _value;
public:
  Attribute(const Attribute& attr);
  Attribute(Attribute&& attr);
  Attribute(opt_dir optimization_direction, double attribute, integrality integrality = Continuous);

  Attribute(std::istream& is);

  void operator=(const Attribute& attr);
  void operator=(Attribute&& attr);

  bool operator!=(const Attribute& other) const ;
  bool operator==(const Attribute& other) const ;

  const opt_dir& optimization_direction() const;
  opt_dir& optimization_direction();
  const bool& is_dependent() const;
  bool& is_dependent();
  const integrality& is_integral() const;
  integrality& is_integral();
  const double& value() const;
  double& value();

  static std::string csv_columns(std::string prefix, std::string separator);
  static std::string csv_data(const std::unordered_map<std::string, Attribute>& attributes, std::string separator);


  friend std::ostream& operator<<(std::ostream& os, const Attribute& attr);
  friend std::istream& operator>>(std::istream& os, Attribute& attr);

  // _debug_helpers
  void dump();
};

std::ostream& operator<<(std::ostream& os, const Attribute& attr);
std::ostream& operator<<(std::ostream& os, const std::unordered_map<std::string, Attribute>& attributes);
std::istream& operator>>(std::istream& os, Attribute& attr);
std::istream& operator>>(std::istream& os, std::unordered_map<std::string, Attribute>& attr);

#endif
