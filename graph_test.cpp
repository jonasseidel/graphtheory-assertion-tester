#include <fstream>
#include <sstream>

#include "Graphtheory/Graphtheory.h"


int main(){

  random_graph_generator graph_generator(
                // shelter_orphans   only_tip_fringes  only_tip_extreme_layer
    tipping_policy(true,             true,             false),
                  //   number_of_nodes  number_of_steps   node_attribute_generator
    uniform_node_steps(3,              1,                {{}}),
                  //          number_of_edges fuzzing_distance_from fuzzing_distance_to edge_attribute_generator              at_least_strongly_connected at_least_weakly_connected acyclic simple anti_reflexive
    uniform_edge_step_fuzzing(6,             0,                    0,                  {{{"Upper", {fix, Integral, 1, 10}}}}, false,                     false,                    false,   false, false),
    tipping_policy(true,             true,             false),
    uniform_node_steps(4,              1,                {{}}),
    uniform_edge_step_fuzzing(3,             0,                    0,                  {{{"Upper", {fix, Integral, 1, 10}}}}, false,                     false,                    true,   false, false),
    {{"Flow", Attribute(max, 0)}, {"Lower", Attribute(fix, 0)}, {"Upper", Attribute(fix, 1)}}, // default_edge_attributes
    {},
    {{{"Upper", {fix, Integral, 1, 10}}}},
    {},
    {{{"Upper", {fix, Integral, 1, 10}}}}
  );

  auto [st, core_network, aggregated_subnetwork_cores, aggregated_subnetworks, network_connectors, g] = graph_generator.next();

  assert(!g.is_borrowed());
  std::cout << g << std::endl;

  std::cout << "Source: \n" << *st.first << std::endl;
  std::cout << "Target: \n" << *st.second << std::endl;

  std::cout << "MaxFlow: " << g.ford_fulkerson(st.first, st.second, "Flow", "Lower", "Upper") << std::endl;

  std::cout << g << std::endl;

  std::stringstream path;
  path << "./.data/.cache/" << &g << ".netw";
  std::ofstream ofs_graph(path.str());
  ofs_graph << g << std::endl;
  ofs_graph.close();

  std::stringstream command;
  command << "cd ../display/graph_display/ && (./graph_display --file ../../discrete_optimization_library/" << path.str() << " &) && cd ../../discrete_optimization_library";
  system(command.str().c_str());

  std::ifstream ifs_graph(path.str());
  Graph g2 (ifs_graph);

  std::cout << g2 << std::endl;

  return 0;
}
