template <typename T>
random_set_element_generator<T>::random_set_element_generator(std::unordered_set<T>* s, generation_type gen_type) : _gen_type(gen_type){
  switch(this->_gen_type){
    case random_unused:
      this->_current_run_set = this->_static_set = *s;
      break;
    case random_all_static:
      this->_static_set = *s;
      break;
    case random_all_dynamic:
      this->_dynamic_set = s;
      break;
    default:
      throw std::invalid_argument("unknown generation_type in contructor of random_set_element_generator");
  }
}

template <typename T>
random_set_element_generator<T>::random_set_element_generator(std::unordered_set<T> s, generation_type gen_type) : _gen_type(gen_type){
  switch(this->_gen_type){
    case random_unused:
      this->_current_run_set = this->_static_set = std::move(s);
      break;
    case random_all_static:
      this->_static_set = std::move(s);
      break;
    case random_all_dynamic:
      throw std::invalid_argument("reference needed to generate dynamically");
    default:
      throw std::invalid_argument("unknown generation_type in contructor of random_set_element_generator");
  }
}

template <typename T>
T random_set_element_generator<T>::next(){
  typename std::unordered_set<T>::iterator set_iterator;
  size_t current_set_size;
  switch(this->_gen_type){
    case random_unused:
      set_iterator = this->_current_run_set.begin();
      current_set_size = this->_current_run_set.size();
      break;
    case random_all_static:
      set_iterator = this->_static_set.begin();
      current_set_size = this->_static_set.size();
      break;
    case random_all_dynamic:
      set_iterator = this->_dynamic_set->begin();
      current_set_size = this->_dynamic_set->size();
      break;
    default:
      throw std::invalid_argument("unknown generation_type in contructor of random_set_element_generator");
  }

  if(current_set_size == 0) throw std::range_error("can't generate from emtpy set");
  std::uniform_int_distribution<int> _dist(0, current_set_size - 1);

  size_t element_index = _dist(random_set_element_generator<T>::_engine);
  std::advance(set_iterator, element_index);
  T return_before_iter_invalid = *set_iterator;

  if(this->_gen_type == random_unused){
    this->_current_run_set.erase(set_iterator);
  }

  return return_before_iter_invalid;
}

template <typename T>
void random_set_element_generator<T>::operator>>(T& var){
  var = this->next();
}

template <typename T>
void random_set_element_generator<T>::reset(){
  this->_current_run_set = this->_set;
}

template <typename T>
bool random_set_element_generator<T>::has_next(){
  size_t current_set_size;
  switch(this->_gen_type){
    case random_unused:
      current_set_size = this->_current_run_set.size();
      break;
    case random_all_static:
      current_set_size = this->_static_set.size();
      break;
    case random_all_dynamic:
      current_set_size = this->_dynamic_set->size();
      break;
    default:
      throw std::invalid_argument("unknown generation_type in contructor of random_set_element_generator");
  }
  return (current_set_size > 0);
}
