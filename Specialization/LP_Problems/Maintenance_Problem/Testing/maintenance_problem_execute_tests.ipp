#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <ctime>
#include <filesystem>
#include <chrono>
#include <signal.h>
#include "../stock_computational_benders_types/nmp_benders_generation.h"

bool redo(Data& data){
  bool decide = false;

  for( auto [formulation, exec_vector] : data.derived_performance ){
    for( const Derived_Performance_Data& derived_performance : exec_vector ){
      if( derived_performance.gap.second > CMP_EPS ){
        decide = true;
      }
    }
  }

  std::string curr;

  if(decide){
    decide:
    std::cout << "Do you want to include partially unsolved data? [y/redo/mark(save but mark)/skip/print(you will be prompted again)/show(you will be prompted again)]" << std::endl;
    while( std::getline(std::cin, curr) && curr != "y" && curr != "redo" && curr != "mark" && curr != "skip" && curr != "print" && curr != "show"){
      std::cout << "Do you want to include partially unsolved data? [y/redo/mark(save but mark)/skip/print(you will be prompted again)/show(you will be prompted again)]" << std::endl;
    }
    if(curr == "y"){
      return false;
    }else if(curr == "redo"){
      std::cerr << "recalculating" << std::endl;
      return true;
    }else if(curr == "mark"){
      data.marked = true;
      return false;
    }else if(curr == "print"){
      std::cout << data.mp << std::endl;
      goto decide;
    }else if(curr == "show"){
      std::ofstream ofs_netw("tmp.netw");
      ofs_netw << data.mp.network() << std::endl;
      ofs_netw.close();
      system("./graph_display --file tmp.netw &");
      goto decide;
    }
    std::cout << "invalid choice!" << std::endl;
    goto decide;
    throw std::runtime_error("unreachable");
  }

  return !data.values_complete();
}

struct exec_data{
  Data* data_ptr;
  bool check_all_problem_data;
  bool add_new_execution;
};

void* execute(void* info_ptr){
  exec_data* info = (exec_data*) info_ptr;
  Data& data = *info->data_ptr;
  bool check_all_problem_data = info->check_all_problem_data;
  bool add_new_execution = info->add_new_execution;

  usleep(200);

  bool all_succeeded = true;

  // determining derived problem data
  Derived_Problem_Data& derived_prob_data = data.derived_problem;
  //  number_of_edges
  SET_VARIABLE(derived_prob_data, number_of_edges, data.mp.network().size().edges);
  //  number_of_nodes
  SET_VARIABLE(derived_prob_data, number_of_nodes, data.mp.network().size().nodes);
  // avg incid per node
  double avg_incid_per_node_val = 2*(double)data.mp.network().size().edges/data.mp.network().size().nodes;
  SET_VARIABLE(derived_prob_data, avg_incid_per_node, avg_incid_per_node_val);
  // number_of_critical_edges
  size_t number_of_critical_edges_val = 0;
  data.mp.network().for_edges([&number_of_critical_edges_val](const Edge* e){
    if(e->attribute_throwing("Selected").value()) number_of_critical_edges_val++;
  });
  SET_VARIABLE(derived_prob_data, number_of_critical_edges, number_of_critical_edges_val);
  // share_of_critical
  double share_of_critical_val = (double)number_of_critical_edges_val/data.mp.network().size().edges;
  SET_VARIABLE(derived_prob_data, share_of_critical, share_of_critical_val);
  // number_of_epochs
  SET_VARIABLE(derived_prob_data, number_of_epochs, data.mp.number_of_epochs());
  // path_length_lower_bound
  auto shortest_path = data.mp.network().directed_admissible_st_path(data.mp.source(), data.mp.target(), [](const Node* from, const Edge* via){return (via->to(from) == via->to()); });
  size_t path_length_lower_bound_val;
  if(shortest_path.exists()){
    path_length_lower_bound_val = shortest_path.number_of_edges();
  }else{
    std::cerr << "problem execution: no connecting path could be found (variable shortest_path set to \"SIZE_MAX\")" << std::endl;
    path_length_lower_bound_val = SIZE_MAX;
  }
  SET_VARIABLE(derived_prob_data, path_length_lower_bound, path_length_lower_bound_val);

  std::vector<SCIP*> computational_models = data.mp.all_computational_models();

  std::cout << "size: " << computational_models.size() << std::endl;

  for(SCIP* current_scip : computational_models){
    auto scip_iterator = data.derived_performance.find(SCIPgetProbName(current_scip));
    if(scip_iterator == data.derived_performance.end()) {
      auto [new_scip_iterator, success] = data.derived_performance.insert({SCIPgetProbName(current_scip), std::vector<Derived_Performance_Data>()});
      assert(success);
      scip_iterator = new_scip_iterator;
    }
    auto& [name, exec_vector] = *scip_iterator;

    if(!add_new_execution && exec_vector.size() != 0
    && exec_vector.back().time_in_sec.first
    && exec_vector.back().number_of_bnb_runs.first
    && exec_vector.back().number_of_reopt_runs.first
    && exec_vector.back().number_of_nodes_explored.first
    && exec_vector.back().max_depth.first
    && exec_vector.back().dual_bound.first
    && exec_vector.back().primal_bound.first
    && exec_vector.back().gap.first
    && exec_vector.back().number_of_primal_sols.first)
    {
      std::cerr << "problem execution: skipping execution of " << SCIPgetProbName(current_scip) << " due to it having been previously executed" << std::endl;

      SCIPfree(&current_scip);

      continue;
    }

    std::cerr << "---------------->" << SCIPgetProbName(current_scip) << ":" << std::endl;

    SCIP_CALL_ABORT( SCIPsetRealParam(current_scip, "limits/time", 1000) );

    SCIPsolve(current_scip);

    if(add_new_execution || exec_vector.size() == 0){
      exec_vector.push_back({});
    }
    Derived_Performance_Data& performance_container = exec_vector.back();

    SET_VARIABLE(performance_container, time_in_sec, SCIPgetSolvingTime(current_scip));

    SET_VARIABLE(performance_container, number_of_bnb_runs, SCIPgetNRuns(current_scip));

    SET_VARIABLE(performance_container, number_of_reopt_runs, SCIPgetNReoptRuns(current_scip));

    SET_VARIABLE(performance_container, number_of_nodes_explored, SCIPgetNNodes(current_scip));

    SET_VARIABLE(performance_container, max_depth, SCIPgetMaxDepth(current_scip));

    SET_VARIABLE(performance_container, dual_bound, SCIPgetDualbound(current_scip));

    SET_VARIABLE(performance_container, primal_bound, SCIPgetPrimalbound(current_scip));

    SET_VARIABLE(performance_container, gap, SCIPgetGap(current_scip));

    SET_VARIABLE(performance_container, number_of_primal_sols, SCIPgetNSolsFound(current_scip));

    SCIPfree(&current_scip);

  }

  std::cerr << "called" << std::endl;

  return NULL;
}

bool execute_tests(std::filesystem::path path, bool check_all_problem_data, bool add_new_execution){
  std::cerr << "-----------------> execution started in " << path.string() << std::endl;

  // execution function


  // reading in testset
  std::vector<Data> data_vect;
  bool success = true;
  // iterating over all contained folders
  for (const auto & entry : std::filesystem::directory_iterator(path)){
    if(entry.is_directory()){
      std::filesystem::path data_path = entry.path()/"instance.test_data";
      std::cerr << "problem execution: reading data for instance " << "\033[1;31m" << data_path.parent_path().filename() << "\033[0m" << std::endl;

      Data curr_data = Data{};
      std::fstream data_file;
      do{
        data_file.open(data_path, std::fstream::in);
        data_file >> curr_data;
        data_file.close();

        std::cerr << "problem execution: executing" << std::endl;

        exec_data process_data = exec_data {
          &curr_data,
          check_all_problem_data,
          add_new_execution
        };

        pthread_t pth;
        int thread_id = pthread_create(&pth, NULL, execute, &process_data);

        std::filesystem::directory_entry logfile("all_exec.log");
        std::chrono::time_point<std::chrono::file_clock> last;
        std::chrono::time_point<std::chrono::file_clock> begin = std::chrono::file_clock::now();

        timespec time;
        int join_state = 5;
        size_t spos;

        do{
          if(join_state == 3) break;
          last = logfile.last_write_time();

          clock_gettime(CLOCK_REALTIME, &time);
          time.tv_sec += 1;

          //if( std::chrono::duration_cast<std::chrono::seconds>(std::chrono::file_clock::now() - begin) > std::chrono::seconds(5000) ){
          //  begin = std::chrono::file_clock::now();
          //  pthread_kill(pth, SIGINT);
          //}


        }while( (join_state = pthread_timedjoin_np(pth, NULL, &time)) && (std::chrono::duration_cast<std::chrono::seconds>(std::chrono::file_clock::now() - last) < std::chrono::seconds(100)));
        std::chrono::seconds s(std::chrono::duration_cast<std::chrono::seconds>(std::chrono::file_clock::now() - last));
        if(join_state != 0) {
          std::cerr << "Stalling/Timeout detected, cancelling!" << std::endl;
          assert(!pthread_cancel(pth));
        }
        pthread_join(pth, NULL);
      }while(!curr_data.values_complete());
      data_file.open(data_path, std::fstream::out);
      data_file << curr_data;
      data_file.close();
    }
  }

  std::cerr << std::endl;

  return success;
}
