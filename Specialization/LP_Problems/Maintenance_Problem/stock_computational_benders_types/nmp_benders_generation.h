#ifndef NMP_BENDER_GENERATION_H
#define NMP_BENDER_GENERATION_H
#include "../Maintenance_Problem.h"

SCIP* nmp_generate_benders(Maintenance_Problem& mp);

#endif
