#include "opt_dir.h"

std::ostream& operator<<(std::ostream& os, const opt_dir& direction){
  switch(direction){
    case max:
      os << "max";
      break;
    case fix:
      os << "fix";
      break;
    case min:
      os << "min";
      break;
    default:
      throw std::runtime_error("invalid direction");
  }
  return os;
}
std::istream& operator>>(std::istream& is, opt_dir& direction){
  std::string curr;
  is >> curr;
  if(curr == "max"){
    direction = max;
  }else if(curr == "fix"){
    direction = fix;
  }else if(curr == "min"){
    direction = min;
  }else{
    throw std::invalid_argument("invalid direction");
  }
  return is;
}
