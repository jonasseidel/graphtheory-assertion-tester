#include "../Maintenance_Problem.h"
#include "nmp_benders_generation.h"
#include <iostream>

//struct SCIP_VarData{
//  int nepochs;
//  SCIP_VAR** vars_of_repr_edge;
//};

struct SCIP_ProbData{
  int nepochs;
  SCIP** subproblems;
};

//SCIP_RETCODE del_vardata_benders(SCIP* scip, SCIP_VAR* var, SCIP_VARDATA** vardata_unpec){
//  SCIP_VarData** vardata = (SCIP_VarData**) vardata_unpec;
//
//  SCIPfreeBlockMemoryArray(scip, &(*vardata)->vars_of_repr_edge, (*vardata)->nepochs);
//
//  SCIPfreeBlockMemory(scip, vardata);
//
//  return SCIP_OKAY;
//}

static
SCIP_RETCODE del_prob_data (SCIP* scip, SCIP_PROBDATA** probdata){
  SCIP_ProbData** prob_data = (SCIP_ProbData**) probdata;

  for(int index = 0; index < (*prob_data)->nepochs; ++index){
    SCIPfree(&(*prob_data)->subproblems[index]);
  }
  
  SCIPfreeBlockMemoryArray(scip, &(*prob_data)->subproblems, (*prob_data)->nepochs);

  SCIPfreeBlockMemory(scip, probdata);

  return SCIP_OKAY;
}


SCIP* nmp_generate_benders(Maintenance_Problem& mp){
  // Master Problem:
  SCIP* master;
  SCIP_CALL_ABORT( SCIPcreate(&master));
  SCIP_CALL_ABORT( SCIPincludeDefaultPlugins(master));
  SCIP_CALL_ABORT( SCIPcreateProbBasic(master, "maintenance_problem_benders"));

  SCIP_CALL_ABORT( SCIPsetObjsense(master, SCIP_OBJSENSE_MAXIMIZE));

  SCIP_CALL_ABORT( SCIPsetIntParam(master, "presolving/maxrestarts",0) );
  SCIP_CALL_ABORT( SCIPsetIntParam(master, "heuristics/trysol/freq", 1) );


  std::cout << "generating MP variables" << std::endl;
  std::unordered_map<Variable*, SCIP_VAR*> mp_variable_lookup;
  for(auto pair : mp.polyeder().variables()){
    Variable* curr_var = pair.left;
    // only decision variables and lower bound on flow enters the mp:
    if(curr_var->description().find("Selected") != std::string::npos){
      SCIP_VAR* computational_var = curr_var->computational_var(master, 0);
      mp_variable_lookup.insert({curr_var, computational_var});
      SCIP_CALL_ABORT( SCIPaddVar(master, computational_var));
    }else if(curr_var->description().find("target_variable") != std::string::npos){
      SCIP_VAR* computational_var = curr_var->computational_var(master, 1);
      mp_variable_lookup.insert({curr_var, computational_var});
      SCIP_CALL_ABORT( SCIPaddVar(master, computational_var));

      SCIP_CONS* remove;
      SCIPcreateConsBasicLinear(master, &remove, "aritficial_bound", 0, NULL, NULL, 0, 500000);
      SCIPaddCoefLinear(master, remove, computational_var, 1);
      SCIPaddCons(master, remove);

      SCIPreleaseVar(master, &computational_var);
      SCIPreleaseCons(master, &remove);
    }
  }

  // subproblems:
  SCIP** subproblems;
  SCIP_CALL_ABORT( SCIPallocBlockMemoryArray(master, &subproblems, mp.number_of_epochs()));
  std::vector<std::unordered_map<Variable*, SCIP_VAR*>> subproblem_variable_lookups;
  subproblem_variable_lookups.reserve(mp.number_of_epochs());
  for(int index = 0; index < mp.number_of_epochs(); index++){
    subproblem_variable_lookups.push_back(std::unordered_map<Variable*, SCIP_VAR*>());

    SCIP_CALL_ABORT( SCIPcreate(&subproblems[index]));
    SCIP_CALL_ABORT( SCIPincludeDefaultPlugins(subproblems[index]));
    std::stringstream name;
    name << index;
    char* namec = new char[name.str().size()+1];
    SCIP_CALL_ABORT( SCIPcreateProbBasic(subproblems[index], strcpy(namec, name.str().c_str())));
    SCIP_CALL_ABORT( SCIPsetObjsense(subproblems[index], SCIP_OBJSENSE_MAXIMIZE));

    std::cout << "generating subproblem_" << index << "'s variables" << std::endl;
    std::stringstream curr_epoch;
    curr_epoch << "epoch_" << index;
    for(auto pair : mp.polyeder().variables()){
      Variable* curr_var = pair.left;
      // only variables of the corresponding epoch may enter the subproblem:
      if(curr_var->description().find(curr_epoch.str()) != std::string::npos || curr_var->description().find("target_variable") != std::string::npos){
        SCIP_VAR* computational_var = curr_var->computational_var(subproblems[index], 0);


        SCIP_Bool infeasible = FALSE;
        SCIP_CALL_ABORT( SCIPchgVarType(subproblems[index], computational_var, SCIP_VARTYPE_CONTINUOUS, &infeasible));
        assert(!infeasible);


        subproblem_variable_lookups[index].insert({curr_var, computational_var});
        SCIP_CALL_ABORT( SCIPaddVar(subproblems[index], computational_var));
      }
    }
  }


  // constraints of both:
  std::cout << "generating constraints" << std::endl;
  for(Constraint cons : mp.polyeder().constraints()){
    if(cons.description().find("processed") != std::string::npos){
      SCIP_CONS* computational_cons = cons.computational_con(master, mp_variable_lookup);
      SCIP_CALL_ABORT( SCIPaddCons(master, computational_cons));

      //// make the variables for other epochs discoverable via vardata of mp vars:
      //SCIP_VAR** edge_vars;
      //SCIP_CALL_ABORT( SCIPallocBlockMemoryArray(master, &edge_vars, mp.number_of_epochs()));
      //SCIP_Bool result;
      //SCIP_CALL_ABORT( SCIPgetConsVars(master, computational_cons, edge_vars, mp.number_of_epochs(), &result));
      ///*for(int i = 0; i < mp.number_of_epochs(); i++){
      //  SCIP_CALL_ABORT( SCIPcaptureVar(master, edge_vars[i]));
      //}*/ // We would ordinary capture these variables since they are saved in one anothers vardata, however doing so would create a circular dependence, meaning that non of them would be released ever. We trust that they will continue being valid for the lifetime of our scip instance
      //assert(result);
      //// sorting array by epoch
      //class epoch_relation_extractor{
      //public:
      //  bool operator()(SCIP_VAR* a, SCIP_VAR* b){
      //    size_t pos_a = std::string(SCIPvarGetName(a)).find("epoch_");
      //    assert(pos_a != std::string::npos);
      //    size_t pos_b = std::string(SCIPvarGetName(b)).find("epoch_");
      //    assert(pos_b != std::string::npos);

      //    return (std::atoi(SCIPvarGetName(a)+pos_a+6) < std::atoi(SCIPvarGetName(b)+pos_a+6));
      //  }
      //} epoch_relation_extractor_instance;

      //std::sort(edge_vars, edge_vars+mp.number_of_epochs(), epoch_relation_extractor_instance);


      //for(int epoch = 0; epoch < mp.number_of_epochs(); epoch++){
      //  SCIP_VarData* curr_var_data;
      //  SCIP_CALL_ABORT( SCIPallocBlockMemory(master, &curr_var_data));
      //  curr_var_data->nepochs = mp.number_of_epochs();
      //  curr_var_data->vars_of_repr_edge = edge_vars;
      //  SCIPvarSetData(edge_vars[epoch], curr_var_data);
      //  SCIPvarSetDelorigData(edge_vars[epoch], del_vardata_benders);
      //}

      SCIPreleaseCons(master, &computational_cons);
    /*}else if(cons.description().find("non_critical") != std::string::npos){
      SCIP_CONS* computational_cons = cons.computational_con(master, mp_variable_lookup);
      SCIP_CALL_ABORT( SCIPaddCons(master, computational_cons));*/
    }else{
      size_t epoch_parse_index = cons.description().find("epoch");
      assert(epoch_parse_index != std::string::npos);
      int epoch = std::atoi(cons.description().c_str() + (epoch_parse_index+6));
      SCIP_CONS* computational_cons = cons.computational_con(subproblems[epoch], subproblem_variable_lookups[epoch]);
      SCIP_CALL_ABORT( SCIPaddCons(subproblems[epoch], computational_cons));
      SCIPreleaseCons(subproblems[epoch], &computational_cons);
    }
  }
  for(auto& [var, comp_var] : mp_variable_lookup){
    SCIPreleaseVar(master, &comp_var);
  }
  for(int epoch = 0; epoch < mp.number_of_epochs(); epoch++){
    for(auto& [var, comp_var] : subproblem_variable_lookups[epoch]){
      SCIPreleaseVar(subproblems[epoch], &comp_var);
    }
  }

  SCIP_ProbData* prob_data;
  SCIPallocBlockMemory(master, &prob_data);
  prob_data->nepochs = mp.number_of_epochs();
  prob_data->subproblems = subproblems;
  SCIPsetProbData(master, prob_data);
  SCIPsetProbDelorig(master, del_prob_data);
  SCIP_CALL_ABORT( SCIPcreateBendersDefault(master, subproblems, mp.number_of_epochs()));

  SCIP_CALL_ABORT( SCIPsetBoolParam(master, "constraints/benders/active", TRUE) );
  SCIP_CALL_ABORT( SCIPsetBoolParam(master, "constraints/benderslp/active", TRUE) );
  SCIP_CALL_ABORT( SCIPsetIntParam(master, "constraints/benders/maxprerounds", 0) );
  SCIP_CALL_ABORT( SCIPsetIntParam(master, "presolving/maxrounds", 0) );


  //SCIP_CALL_ABORT( SCIPsetPresolving(*scip_benders_feas, SCIP_PARAMSETTING_OFF, true));

  //SCIP_CALL_ABORT( SCIPsolve(master));
  //SCIP_CALL_ABORT( SCIPprintOrigProblem(master, NULL, NULL, FALSE));
/*
  for(size_t index = 0; index < mp.number_of_epochs(); index++){
    SCIP_CALL_ABORT( SCIPprintOrigProblem(subproblems[index], NULL, NULL, FALSE));
  }*/

  //SCIPsolve(*scip_benders_feas);

  return master;
}
