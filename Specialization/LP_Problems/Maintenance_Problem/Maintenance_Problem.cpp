#include "Maintenance_Problem.h"

#include "../../../Common/io_format.h"

// computational
#include "computational_types/semiassignment_hdlr.h"
#include "computational_types/semiassignment_branch.h"
#include "computational_types/generate_nmp_bendersdecomp.h"
#include "stock_computational_benders_types/nmp_benders_generation.h"

Maintenance_Problem::Maintenance_Problem() : Linear_Program("maintenance_problem_ip", true), _number_of_epochs(0), _g({{"Flow", Attribute(max, 0)},{"Upper", Attribute(fix, 1)}, {"Selected", Attribute(fix, 0)}}), _source(nullptr), _target(nullptr) {

}

Maintenance_Problem::Maintenance_Problem(const Maintenance_Problem& mp) : Linear_Program("maintenance_problem_ip", true) {
  *this = mp;
}

Maintenance_Problem::Maintenance_Problem(Maintenance_Problem&& mp) : Linear_Program("maintenance_problem_ip", true) {
  *this = std::move(mp);
}

Maintenance_Problem::Maintenance_Problem(
  const Graph& g, const Node* source,
  const Node* target,
  size_t epochs
)
  : Linear_Program("maintenance_problem_ip", true), _number_of_epochs(epochs)
{
  // copy graph and transform source and target variables to ptrs in the copy
  assert(g.contains(source));
  assert(g.contains(target));

  auto copy_and_lookup = g.copy_ret_lookups();
  this->_g = std::move(std::get<0>(copy_and_lookup));
  auto source_lookup = std::get<1>(copy_and_lookup).find(source);
  if(source_lookup == std::get<1>(copy_and_lookup).end()) throw std::invalid_argument("source not contained in graph");
  this->_source = source_lookup->second;
  auto target_lookup = std::get<1>(copy_and_lookup).find(target);
  if(target_lookup == std::get<1>(copy_and_lookup).end()) throw std::invalid_argument("target not contained in graph");
  this->_target = target_lookup->second;

  // create lp
  std::unordered_set<const Edge*> critical = g.gather_edges({"Selected"});

  for(std::string attr : std::vector({"Selected", "Upper", "Flow"})){
    if(!this->_g.edge_template(attr).first) {
      throw std::invalid_argument("Maintenance_Problem needs Graph with predetermined attributes present");
    }
  }


  std::vector<
    std::pair<
      std::map<
        std::pair<const Node*,std::string>,
        std::pair<Variable*, size_t>
      >,
      std::map<
        std::pair<const Edge*,std::string>,
        std::pair<Variable*, size_t>
      >
    >
  > lookup;

  /*
    flow conservation constraints for all epochs
  */
  for(size_t epoch = 0; epoch < epochs; ++epoch){
    std::stringstream name_appendix;
    name_appendix << "epoch_" << epoch;
    lookup.push_back(lp_generator::grow_from_graph(
      *this,
      g,
      [&critical, epoch](const Edge* edge) -> std::vector<constraint_data> {
        std::stringstream capacity_name;
        capacity_name << "edge_" << edge->description() << "_capacity_epoch_" << epoch;
        std::stringstream non_critical_fix;
        non_critical_fix << "edge_" << edge->description() << "_non_critical_epoch_" << epoch;

        std::vector<constraint_data> constraints;
        if(critical.find(edge) == critical.end()){
          constraints.push_back({capacity_name.str(), Inequality, {{}, {{{edge, "Flow"}, 1}}}, edge->attribute_throwing("Upper").value()});
          constraints.push_back({non_critical_fix.str(), Equality, {{}, {{{edge, "Selected"}, 1}}}, 0});
        }else{
          constraints.push_back({capacity_name.str(), Inequality, {{}, {{{edge, "Flow"}, 1},{{edge,"Selected"}, edge->attribute_throwing("Upper").value()}}}, edge->attribute_throwing("Upper").value()});
        }

        return constraints;
      },
      [source, target, &critical, epoch](const Node* node) -> std::vector<constraint_data> {
        if(node == source || node == target) return {};
        std::stringstream name;
        name << "node_" << node->description() << "_flow_conservation_epoch_" << epoch;
        lhs_constraint_data lhs = {{},{}};
        for(Edge* edge : node->incident()){
          lhs.second.push_back({{edge,"Flow"}, (edge->to() == node ? 1 : -1) });
        }

        return {{name.str(), Equality, lhs, 0}};
      },
      {{"Flow", {Continuous, {true, 0}, {false, 0}}}, {"Selected", {Integral, {true, 0}, {true, 1}}}},
      {},
      name_appendix.str()
    ));
  }

  /*
    critical edges processed
  */
  for(const Edge* e : critical){
    std::unordered_map<Variable*, Coefficient> lhs;
    for(auto lookup_during_epoch : lookup){
      lhs.insert({lookup_during_epoch.second.find({e, "Selected"})->second.first, {1}});
    }
    std::stringstream name;
    name << "critical_edge_" << e->description() << "_processed";
    this->polyeder().add_constraint(
      { // constraint
        name.str(),
        Equality,
        lhs,
        1,
      }
    );
  }

  /*
    value bounded by flow
  */
  std::pair<Variable*, size_t> target_var = this->polyeder().add_variable(Variable("target_variable", Continuous));

  for(size_t epoch = 0; epoch < epochs; ++epoch){
    auto lookup_during_epoch = lookup[epoch];
    std::unordered_map<Variable*, Coefficient> lhs;
    for(Edge* e : source->incident()){
      lhs.insert({lookup_during_epoch.second.find({e, "Flow"})->second.first, {-1}});
    }
    lhs.insert({target_var.first, {1}});

    std::stringstream name;
    name << "target_variable_bounds_epoch_" << epoch;
    this->polyeder().add_constraint(
      { // constraint
        name.str(),
        Inequality,
        lhs,
        0
      }
    );
  }

  this->add_direction_coefficient({target_var.first, 1});
}

const Graph& Maintenance_Problem::network() const {
  return this->_g;
}

const Node* Maintenance_Problem::source() const {
  return this->_source;
}
const Node* Maintenance_Problem::target() const {
  return this->_target;
}

size_t Maintenance_Problem::number_of_epochs() const {
  return this->_number_of_epochs;
}

std::vector<SCIP*> Maintenance_Problem::all_computational_models(){
  // basic model:
  auto [scip_basic, lookup] = Linear_Program::computational_model();
  // release all vars
  for(auto& [variable, computational_var] : lookup){
    SCIP_CALL_ABORT( SCIPreleaseVar(scip_basic, &computational_var));
  }

  //SCIPsolve(scip_basic);
/*
  // semiassignment_branching
  SCIP* scip_semiassign = Linear_Program::computational_model().first;
  SCIP_CONS** conss = SCIPgetOrigConss(scip_semiassign);
  std::vector<SCIP_CONS*> process_cons;
  for(int cons_index = 0; cons_index < SCIPgetNOrigConss(scip_semiassign); cons_index++){
    if(std::string(conss[cons_index]->name).find("processed") != std::string::npos){
      process_cons.push_back(conss[cons_index]);
    }
  }
  SCIPincludeConshdlrSemiassign(scip_semiassign);

  SCIP_CONS** process_cons_array = new SCIP_CONS*[process_cons.size()];
  std::copy(process_cons.begin(), process_cons.end(), process_cons_array);
  SCIPincludeBranchruleSemiassign(scip_semiassign, process_cons.size(), process_cons_array);
*/
  // benders

  SCIP* benders_native = nmp_generate_benders(*this);

  //SCIP* scip_benders = generate_nmp_bendersdecomp(*this, benders_native, SCIPgetBestSol(benders_native));


  // return all models
  return {scip_basic/*, scip_semiassign/*, scip_benders/**/, benders_native};
}

void Maintenance_Problem::operator=(const Maintenance_Problem& mp){
  this->Linear_Program::operator=(const_cast<Maintenance_Problem&>(mp)); // yeah, I am not doing const correctness for Linear_Program... not gonna happen
  this->_number_of_epochs = mp._number_of_epochs;
  auto copy_and_lookup = mp._g.copy_ret_lookups();
  this->_g = std::move(std::get<0>(copy_and_lookup));
  auto source_lookup = std::get<1>(copy_and_lookup).find(mp._source);
  this->_source = source_lookup->second;
  auto target_lookup = std::get<1>(copy_and_lookup).find(mp._target);
  this->_target = target_lookup->second;
}

void Maintenance_Problem::operator=(Maintenance_Problem&& mp){
  this->Linear_Program::operator=(mp);
  this->_number_of_epochs = std::move(mp._number_of_epochs);
  this->_g = std::move(mp._g);
  mp._g = Graph();
  this->_source = mp._source;
  this->_target = mp._target;
}

Maintenance_Problem::Maintenance_Problem(std::istream& is) : Linear_Program("maintenance_problem_ip", true) {
  std::string curr;
  is >> curr;
  assert(curr == "Maintenance_Problem");
  is >> curr;
  IO_THROW(curr, "{");
  is >> this->_number_of_epochs
      >> this->_g;

  std::unordered_set<Node*> sources = this->_g.gather_nodes({"Source"});
  assert(sources.size() == 1);
  this->_source = *sources.begin();
  std::unordered_set<Node*> targets = this->_g.gather_nodes({"Target"});
  assert(targets.size() == 1);
  this->_target = *targets.begin();
  is >> curr;
  IO_THROW(curr, "}");

  if(!is) throw std::invalid_argument("stream did not contain complete Maintenance_Problem object");
  *this = Maintenance_Problem(this->_g, this->_source, this->_target, this->_number_of_epochs);
}

std::ostream& operator<<(std::ostream& os, const Maintenance_Problem& mp){
  os << "Maintenance_Problem {\n"
      << mp.number_of_epochs() << "\n"
      << mp.network() << "\n"
      << "}";
  return os;
}

std::istream& operator>>(std::istream& is, Maintenance_Problem& mp){
  mp = std::move(Maintenance_Problem(is));
  return is;
}
