#include "Linear_Program.h"

Linear_Program::Linear_Program(const Linear_Program& lp){
  *this = lp;
}

Linear_Program::Linear_Program(Linear_Program&& lp){
  *this = std::move(lp);
}

Linear_Program::Linear_Program(std::string description, bool maximum) : _description(description), _maximum(maximum) {}

std::string Linear_Program::description() const {
  return this->_description;
}

bool Linear_Program::is_maximum() const {
  return this->_maximum;
}

std::unordered_map<Variable*, Coefficient>& Linear_Program::direction(){
  return this->_direction;
}

void Linear_Program::add_direction_coefficient(std::pair<Variable*, Coefficient> summand){
  this->_direction.insert(summand);
}

Coefficient Linear_Program::direction_coefficient(Variable* var){
  auto search_result = this->_direction.find(var);
  if(search_result == this->_direction.end()) return {0};
  return search_result->second;
}

Polyeder& Linear_Program::polyeder(){
  return this->_polyeder;
}

const Polyeder& Linear_Program::polyeder() const {
  return this->_polyeder;
}

std::pair<SCIP*, std::unordered_map<Variable*, SCIP_VAR*> > Linear_Program::computational_model(){
  SCIP* scip;
  SCIP_CALL_ABORT( SCIPcreate(&scip));
  SCIPincludeDefaultPlugins(scip);


  SCIP_CALL_ABORT( SCIPcreateProb(scip, this->description().c_str(), NULL, NULL,
                             NULL, NULL, NULL, NULL, NULL));
  if(this->is_maximum()){
    SCIP_CALL_ABORT( SCIPsetObjsense(scip, SCIP_OBJSENSE_MAXIMIZE));
  }else{
    SCIP_CALL_ABORT( SCIPsetObjsense(scip, SCIP_OBJSENSE_MINIMIZE));
  }
  std::unordered_map<Variable*, SCIP_VAR*> variable_lookup;
  for(auto pair : this->_polyeder.variables()){
    auto search = this->_direction.find(pair.left);

    SCIP_VAR* computational_var = pair.left->computational_var(scip, (search != this->_direction.end() ? search->second.value() : 0));

    variable_lookup.insert({pair.left, computational_var});
    SCIP_CALL_ABORT( SCIPaddVar(scip, computational_var));
  }
  for(Constraint con : this->_polyeder.constraints()){
    SCIP_CONS* computational_con = con.computational_con(scip, variable_lookup);
    SCIP_CALL_ABORT( SCIPaddCons(scip, computational_con));
    SCIP_CALL_ABORT( SCIPreleaseCons(scip, &computational_con));
  }

  return {scip, variable_lookup};
}

void Linear_Program::operator=(const Linear_Program& lp){
  this->_description = lp._description;
  this->_maximum = lp._maximum;
  auto [copy, variable_lookup] = lp._polyeder.copy_ret_lookup();
  this->_polyeder = std::move(copy);
  this->_direction = {};
  for( const auto& [var, coeff] : lp._direction){
    auto variable_search = variable_lookup.find(var);
    if( variable_search == variable_lookup.end() ) throw std::range_error("lookup doesn't include all variables");
    this->_direction.insert({variable_search->second, Coefficient(coeff, variable_lookup)});
  }
}

void Linear_Program::operator=(Linear_Program&& lp){
  this->_description = lp._description;
  this->_maximum = lp._maximum;
  this->_direction = std::move(lp._direction);
  this->_polyeder = std::move(lp._polyeder);
  lp._polyeder = Polyeder();
  lp._direction = std::unordered_map<Variable*, Coefficient>();
}

Linear_Program Linear_Program::relaxation_dual(){
  std::stringstream dual_name;
  dual_name << this->description() << "_dual";
  Linear_Program dual(dual_name.str(), !this->is_maximum());
  Polyeder& p = dual.polyeder();

  for(size_t index = 0; index < this->polyeder().number_of_constraints(); ++index){
    std::stringstream name;
    name << "dual_" << this->polyeder().constraint(index).description();


    if(this->polyeder().constraint(index).is_equality()){
      p.add_variable(Variable(name.str(), Continuous));
    }else{
      p.add_variable(Variable(name.str(), Continuous, {true, 0}));
    }
  }

  // we introduce one constraint per primal variable
  for(auto curr_variable : this->polyeder().variables()){
    // gather lhs_coefficient
    std::unordered_map<Variable*, Coefficient> lhs;
    for(size_t index = 0; index < this->polyeder().number_of_constraints(); ++index){
      lhs.insert({p.variables().right.find(index)->second, {-this->polyeder().constraint(index).lhs_coefficient(curr_variable.left)}});
    }


    // name of new constraint
    std::stringstream name;
    name << "dual_" << curr_variable.left->description();

    // generate contraint where inequality / equality property is based in existance of 0 <= x_curr_var bound
    {
      // if upper bound ex. generate new var and add to lhs
      if(curr_variable.left->upper_bound().first == true){
        std::stringstream ub_name;
        ub_name << "upper_bound_" << curr_variable.left->description();
        Variable* upper_bound_var = p.add_variable(Variable(ub_name.str(), Continuous, {true, 0})).first;

        lhs.insert({upper_bound_var, {-1}});
        dual.add_direction_coefficient({upper_bound_var, curr_variable.left->upper_bound().second});
      }
    }

    {
      if(curr_variable.left->lower_bound().first == true){
        if(curr_variable.left->lower_bound().second == 0){
          p.add_constraint(Constraint(name.str(), Inequality, lhs, -this->direction_coefficient(curr_variable.left)));
        }else{
          std::stringstream lb_name;
          lb_name << "lower_bound_" << curr_variable.left->description();
          Variable* lower_bound_var = p.add_variable(Variable(lb_name.str(), Continuous, {true, 0})).first;

          lhs.insert({lower_bound_var, {1}});
          p.add_constraint(Constraint(name.str(), Equality, lhs, -this->direction_coefficient(curr_variable.left)));
          dual.add_direction_coefficient({lower_bound_var, {-1}});
        }
      }else{
        p.add_constraint(Constraint(name.str(), Equality, lhs, -this->direction_coefficient(curr_variable.left)));
      }
    }
  }

  for(size_t index = 0; index < this->polyeder().number_of_constraints(); ++index){
    dual.add_direction_coefficient({p.variables().right.find(index)->second, this->polyeder().constraint(index).rhs()});
  }

  return dual;
}

std::ostream& operator<<(std::ostream& os, const Linear_Program& linear_program){
  if(linear_program.is_maximum()){
    os << "Maximize\n";
  }else{
    os << "Minimize\n";
  }
  os << "\tobj: ";
  bool empty = true;
  if(linear_program._direction.size() > 0){
    bool add_plus = false;
    for(std::pair<Variable*, Coefficient> direction_summand : linear_program._direction){
      if(direction_summand.second.is_non_zero()){
        empty = false;
        if(add_plus && !Coefficient::always_add_sign){
          os << " + ";
        }
        add_plus = true;
        os << direction_summand.second << direction_summand.first->description();
      }
    }
  }
  if(empty){
    os << "0";
  }
  os << "\n";

  os << linear_program._polyeder << "\n";
  os << "End";
  return os;
}
