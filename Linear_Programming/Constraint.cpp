#include "Constraint.h"

Constraint::Constraint(const Constraint& other){
  *this = other;
}
Constraint::Constraint(Constraint&& other){
  *this = std::move(other);
}

Constraint::Constraint(std::string description, relation relation_type, std::unordered_map<Variable*, Coefficient> lhs, Coefficient rhs)
  : _description(description), _relation(relation_type), _lhs(lhs), _rhs(rhs){}

Constraint::Constraint(relation relation_type, std::unordered_map<Variable*, Coefficient> lhs, Coefficient rhs)
  : _relation(relation_type), _lhs(lhs), _rhs(rhs){}

Constraint::Constraint(const Constraint& other, const std::unordered_map<const Variable*, Variable*>& variable_lookup)
  : _description(other._description),
    _relation(other._relation),
    _rhs(other._rhs, variable_lookup)
{
  for(const auto& [var, coeff] : other._lhs){
    auto var_search = variable_lookup.find(var);
    if(var_search == variable_lookup.end()) throw std::invalid_argument("variable_lookup didn't include necessary entries");
    this->_lhs.insert({var_search->second, Coefficient(coeff, variable_lookup)});
  }
}

std::string Constraint::description() const {
  return this->_description;
}

relation Constraint::is_equality() const {
  return this->_relation;
}

std::unordered_map<Variable*, Coefficient> Constraint::lhs(){
  return this->_lhs;
}

Coefficient Constraint::lhs_coefficient(Variable* var){
  auto tmp = this->_lhs.find(var);
  if(tmp == this->_lhs.end()){
    return {0};
  }
  return tmp->second;
}

Coefficient Constraint::rhs(){
  return this->_rhs;
}

SCIP_CONS* Constraint::computational_con(SCIP* scip, std::unordered_map<Variable*, SCIP_VAR*>& variable_lookup){
  SCIP_CONS* cons;
  SCIP_Real right_inequality = this->_rhs.value();
  SCIP_Real left_inequality = (this->is_equality() ? right_inequality : -SCIPinfinity(scip));

  SCIP_CALL_ABORT( SCIPcreateConsBasicLinear(scip, &cons, this->description().c_str(),
                                   0, NULL, NULL, left_inequality, right_inequality));

  for(std::pair<Variable*, Coefficient> pair : this->_lhs){
    SCIP_CALL_ABORT( SCIPaddCoefLinear(scip, cons, variable_lookup.find(pair.first)->second, pair.second.value()));
  }
  return cons;
}

void Constraint::operator=(const Constraint& other){
  this->_description = other._description;
  this->_relation = other._relation;
  this->_lhs = other._lhs;
  this->_rhs = other._rhs;
}
void Constraint::operator=(Constraint&& other){
  this->_description = std::move(other._description);
  this->_relation = std::move(other._relation);
  this->_lhs = std::move(other._lhs);
  this->_rhs = std::move(other._rhs);
}

std::ostream& operator<<(std::ostream& os, const Constraint& constraint){
  os << constraint.description() << ":\t";
  bool empty = true;

  bool add_plus = false;
  for(auto summand : constraint._lhs){
    if(summand.second.is_non_zero()){
      empty = false;
      if(add_plus && !Coefficient::always_add_sign){
        os << " + ";
      }
      add_plus = true;
      os << summand.second << *summand.first;
    }
  }
  if(empty){
    os << "0";
  }
  if(constraint.is_equality()){
    os << " = ";
  }else{
    os << " <= ";
  }
  if(!constraint._rhs.is_non_zero()){
    os << "0";
  }else if(constraint._rhs.is_unity()){
    os << "1";
  }else{
    os << constraint._rhs;
  }
  return os;
}
