#ifndef UNIFORM_EDGE_STEP_FUZZING_H
#define UNIFORM_EDGE_STEP_FUZZING_H

#include <functional>
#include "edge_step_fuzzing_plugin.h"

class uniform_edge_step_fuzzing : public edge_step_fuzzing_plugin {
  void constr();
public:
  uniform_edge_step_fuzzing(size_t number_of_edges, size_t fuzzing_distance_from, size_t fuzzing_distance_to, random_attribute_generator edge_attribute_generator, bool at_least_strongly_connected, bool at_least_weakly_connected, bool acyclic, bool simple, bool anti_reflexive)
    : edge_step_fuzzing_plugin(number_of_edges, fuzzing_distance_from, fuzzing_distance_to, edge_attribute_generator, at_least_strongly_connected, at_least_weakly_connected, acyclic, simple, anti_reflexive){
      if( this->_fuzzing_distance_from > this->_fuzzing_distance_to ) throw std::invalid_argument("fuzzing_distance_from needs to be \"<= fuzzing_distance_to\"");
      constr();
      init();
  }
};

#endif
