#include "./Linear_Programming/Linear_Program.h"

#include <iostream>

int main(){
//  while(true){
    Linear_Program lp("test_2", true);
    {
      Linear_Program one ("test", true);
      Polyeder& p = one.polyeder();
      Variable* var1 = p.add_variable(Variable("x1", Continuous, {true, 0})).first;
      Variable* var2 = p.add_variable(Variable("x2", Continuous, {true, 0})).first;
      Variable* var3 = p.add_variable(Variable("x3", Continuous, {true, 0})).first;

      std::cout << one << std::endl;

      one.add_direction_coefficient({var1, 1});
      one.add_direction_coefficient({var2, -2});

      std::cout << one << std::endl;

      p.add_constraint(Constraint("1", Equality, {{var1, 1}, {var3, -1}}, 0));
      p.add_constraint(Constraint("2", Inequality, {{var1, 1}, {var2, 1}, {var3, 1}}, 1));
      p.add_constraint(Constraint("3", Inequality, {{var1, -1}, {var3, -1}}, 5));

      std::cout << "test" << std::endl;

      lp = one;
      std::cout << "test" << std::endl;
    }
    std::cout << lp << std::endl;
/*
    auto [model, variable_lookup] = lp.computational_model();
    for(auto [var, comp_var] : variable_lookup){
      SCIPreleaseVar(model, &comp_var);
    }
    SCIPwriteOrigProblem(model, NULL, NULL, false);
    SCIPsolve(model);
    SCIPfree(&model);

    auto dual = lp.relaxation_dual();
    std::cout << dual << std::endl;
    auto [dual_model, variable_lookup_dual] = dual.computational_model();
    for(auto [var, comp_var] : variable_lookup_dual){
      SCIPreleaseVar(dual_model, &comp_var);
    }
    SCIPwriteOrigProblem(dual_model, NULL, NULL, false);
    SCIPsolve(dual_model);
    SCIPfree(&dual_model);
  }*/
}
