#ifndef NMP_BULKSEPA_CONSHDLR_H

#include <scip/scip.h>
#include <scip/scipdefplugins.h>

extern "C"{
  SCIP_RETCODE SCIPincludeConshdlrNMPBendersLP(
     SCIP*                  scip,
     SCIP**                 benders_separators_lpsol,
     SCIP**                 benders_separators_heursol,
     int                    nepochs,
     int                    nedges,
     SCIP_SOL*              sol
  );

  SCIP_RETCODE SCIPincludeConshdlrNMPBendersBranch(
     SCIP*                  scip,
     SCIP**                 benders_separators_lpsol,
     SCIP**                 benders_separators_heursol,
     int                    nepochs,
     int                    nedges
  );
}

#endif
