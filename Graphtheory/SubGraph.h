#ifndef SUBGRAPH_H
#define SUBGRAPH_H

#include "Graph.h"

class SubGraph : private Graph {
  SubGraph* _supergraph; // may also point to Graph Obj, dynamic_cast before use
  std::unordered_set<Node*> _node_restriction;
  std::unordered_set<Edge*> _edge_restrction;
  std::unordered_map<Node*, SubGraph*> _node_ledger;
  std::unordered_map<Edge*, SubGraph*> _edge_ledger;

  // inlineable
  Graph* mastergraph(){
    SubGraph* curr_supergraph = this;
    while(dynamic_cast<SubGraph*>(curr_supergraph) != nullptr){
      curr_supergraph = curr_supergraph->supergraph();
    }
    Graph* master = dynamic_cast<Graph*>(curr_supergraph);
    if(master == nullptr) throw std::runtime_error("Ownership sequence doesn't terminate in Graph object");
    return master;
  }

  SubGraph*& supergraph() {
    if(this->obj_type() != SlaveSubGraph) throw std::runtime_error("Obj is not a SubGraph. Graph doesn't have a supergraph");
    assert(this->_supergraph != nullptr);
    return this->_supergraph;
  }

  std::unordered_map<Node*, SubGraph*>& node_ledger(){
    if(this->obj_type() != SlaveSubGraph) throw std::runtime_error("Obj is not a SubGraph. Graph doesn't have borrowed_nodes");
    return this->_node_ledger;
  }

  std::unordered_map<Edge*, SubGraph*>& edge_ledger(){
    if(this->obj_type() != SlaveSubGraph) throw std::runtime_error("Obj is not a SubGraph. Graph doesn't have borrowed_edges");
    return this->_edge_ledger;
  }

  Node* borrow(Node* node, bool unconditionally);
  Edge* borrow(Edge* edge, bool unconditionally);
  void settle(Node* node, bool unconditionally);


  std::unordered_map<std::string, Attribute>& template_node_attributes(){
    return this->supergraph()->template_node_attributes();
  }
  std::unordered_map<std::string, Attribute>& template_edge_attributes(){
    return this->supergraph()->template_edge_attributes();
  }

  std::unordered_set<Node*>& nodes(){
    return this->supergraph()->nodes();
  }
  std::unordered_set<Edge*>& edges(){
    return this->supergraph()->edges();
  }

  GraphtheoryObjType obj_type(){
    return GraphtheoryObjType::SlaveSubGraph;
  }

  std::unordered_set<Node*>& local_nodes(){
    return this->_node_restriction;
  }
  std::unordered_set<Edge*>& local_edges(){
    return this->_edge_restrction;
  }
  std::unordered_set<const Node*> local_nodes() const {
    std::unordered_set<Node*>& local_nodes = const_cast<SubGraph*>(this)->local_nodes();
    return std::unordered_set<const Node*>(local_nodes.begin(), local_nodes.end());
  }
  std::unordered_set<const Edge*> local_edges() const {
    std::unordered_set<Edge*>& local_edges = const_cast<SubGraph*>(this)->local_edges();
    return std::unordered_set<const Edge*>(local_edges.begin(), local_edges.end());
  }

  size_t& lifetime_node_counter() {
    return this->supergraph()->lifetime_node_counter();
  }
  size_t& lifetime_edge_counter() {
    return this->supergraph()->lifetime_edge_counter();
  }

protected:
  friend Graph;
public:
  // _special_members_and_operators
  SubGraph(Graph& graph);

  SubGraph(SubGraph& subgraph, const std::unordered_set<Node*>& node_restriction);
  SubGraph(SubGraph& subgraph, const std::unordered_set<Node*>& node_restriction, const std::unordered_set<Edge*>& edge_restriction);

  ~SubGraph();


  //inlineable
  // TODO: declare as const Graph*?
  SubGraph* supergraph() const {
    if(dynamic_cast<const SubGraph*>(this) == nullptr) return nullptr;
    return const_cast<SubGraph*>(this)->supergraph();
  }

  std::unordered_set<Node*> export_nodes() {
    return Graph::export_nodes();
  }
  std::unordered_set<Edge*> export_edges() {
    return Graph::export_edges();
  }

  std::unordered_set<const Node*> export_nodes() const {
    return Graph::export_nodes();
  }
  std::unordered_set<const Edge*> export_edges() const {
    return Graph::export_edges();
  }

  size_t lifetime_node_count() const {
    return Graph::lifetime_node_count();
  }
  size_t lifetime_edge_count() const {
    return Graph::lifetime_edge_count();
  }


  bool contains(Node* node){
    return Graph::contains(node);
  }
  bool contains(Edge* edge){
    return Graph::contains(edge);
  }

  Node* borrow(Node* node){
    return this->borrow(node, false);
  }
  Edge* borrow(Edge* edge){
    return this->borrow(edge, false);
  }
  void settle(Node* node){
    this->settle(node, false);
  }

  GraphSize size() const {
    return Graph::size();
  }

  // _borrowing_logic
  bool can_borrow(Node* node);
  bool can_borrow(Edge* edge);
  bool can_settle(Node* node);
  bool can_settle(Edge* edge);

  void settle(Edge* edge);
  std::unordered_set<Node*> borrow(const std::unordered_set<Node*>& nodes);
  std::unordered_set<Edge*> borrow(const std::unordered_set<Edge*>& edges);
  std::unordered_set<Node*> settle(const std::unordered_set<Node*> nodes);
  std::unordered_set<Edge*> settle(const std::unordered_set<Edge*> edges);


  //

  Edge* add_edge(Node* from, Node* to, const std::string& description, const std::unordered_map<std::string,Attribute>& edge_attributes = {});
  Edge* add_edge(const Edge* remote_edge, const std::unordered_map<const Node*, Node*>& node_lookup);
  std::pair<std::unordered_set<Edge*>, std::unordered_map<const Edge*, Edge*>> add_edges(const std::unordered_set<Edge*>& remote_nodes, const std::unordered_map<const Node*, Node*>& node_lookup);
  std::pair<std::unordered_set<Edge*>, std::unordered_map<const Edge*, Edge*>> add_edges(const std::unordered_set<const Edge*>& remote_nodes, const std::unordered_map<const Node*, Node*>& node_lookup);
  Node* add_node(std::string description, const std::unordered_map<std::string, Attribute>& node_attributes = {});
  Node* add_node(const Node* remote_node);
  std::pair<std::unordered_set<Node*>, std::unordered_map<const Node*, Node*> > add_nodes(const std::unordered_set<Node*>& remote_nodes);
  std::pair<std::unordered_set<Node*>, std::unordered_map<const Node*, Node*> > add_nodes(const std::unordered_set<const Node*>& remote_nodes);

  void remove_node(Node* node);
  void remove_edge(Edge* edge);

  // _path_operations
  Path directed_admissible_st_path(
    Node* s, Node* t,
    const std::function<bool(const Node* from, const Edge* via)>& guide = [](const Node* n, const Edge* e)->bool {return e->from() == n;},
    const std::function<bool(const Node* node)>& mask = [](const Node* node){return true;}
  );
  const Path directed_admissible_st_path(
    const Node* s, const Node* t,
    const std::function<bool(const Node* from, const Edge* via)>& guide = [](const Node* n, const Edge* e)->bool {return e->from() == n;},
    const std::function<bool(const Node* node)>& mask = [](const Node* node){return true;}
  ) const ;

  // _basic_algorithms
  std::unordered_set<Node*> conditional_bfs_all_reachable(
    const std::function<void(Node* from, Edge* via, bool used_in_traversal)>& edge_exec,
    const std::function<bool(Edge* via, Node* node)>& node_exec,
    const std::unordered_set<Node*>& starting_nodes,
    const bool& all_paths = false,
    const std::function<bool(const Node* from, const Edge* via)>& guide = [](const Node* n, const Edge* e)->bool {return e->from() == n;},
    const std::function<bool(const Node* node)>& mask = [](const Node* node)->bool {return true;}
  );
  std::unordered_set<Node*> conditional_bfs_all_reachable(
    const std::function<void(const Node* from, const Edge* via, bool used_in_traversal)>& edge_exec,
    const std::function<bool(const Edge* via, const Node* node)>& node_exec,
    const std::unordered_set<const Node*>& starting_nodes,
    const bool& all_paths = false,
    const std::function<bool(const Node* from, const Edge* via)>& guide = [](const Node* n, const Edge* e)->bool {return e->from() == n;},
    const std::function<bool(const Node* node)>& mask = [](const Node* node)->bool {return true;}
  ) const ;
  void conditional_bfs_all_components(
    const std::function<void(Node* from, Edge* via, bool used_in_traversal)>& edge_exec,
    const std::function<bool(Edge* via, Node* node)>& node_exec,
    const std::unordered_set<Node*>& starting_nodes = {},
    const bool& all_paths = false,
    const std::function<bool(const Node* from, const Edge* via)>& guide = [](const Node* n, const Edge* e)->bool {return e->from() == n;},
    const std::function<bool(const Node* node)>& mask = [](const Node* node)->bool {return true;}
  );
  void conditional_bfs_all_components(
    const std::function<void(const Node* from, const Edge* via, bool used_in_traversal)>& edge_exec,
    const std::function<bool(const Edge* via, const Node* node)>& node_exec,
    const std::unordered_set<const Node*>& starting_nodes = {},
    const bool& all_paths = false,
    const std::function<bool(const Node* from, const Edge* via)>& guide = [](const Node* n, const Edge* e)->bool {return e->from() == n;},
    const std::function<bool(const Node* node)>& mask = [](const Node* node)->bool {return true;}
  ) const ;
  bool is_strongly_connected(
    const std::function<bool(const Node* from, const Edge* via)>& guide_forward = [](const Node* n, const Edge* e)->bool {return e->from() == n;},
    const std::function<bool(const Node* from, const Edge* via)>& guide_backward = [](const Node* n, const Edge* e)->bool {return e->to() == n;},
    const std::function<bool(const Node* node)>& mask = [](const Node* node){return true;}
  ) const ;
  bool is_weakly_connected(
    const std::function<bool(const Node* from, const Edge* via)>& guide_forward = [](const Node* n, const Edge* e)->bool {return e->from() == n;},
    const std::function<bool(const Node* from, const Edge* via)>& guide_backward = [](const Node* n, const Edge* e)->bool {return e->to() == n;},
    const std::function<bool(const Node* node)>& mask = [](const Node* node){return true;}
  ) const ;

  void dump(){
    Graph::dump();
  }
  friend std::ostream& operator<<(std::ostream& os, const SubGraph& g);

};

std::ostream& operator<<(std::ostream& os, const SubGraph& g);

#endif
