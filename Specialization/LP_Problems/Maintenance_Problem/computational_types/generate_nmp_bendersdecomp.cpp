#include "generate_nmp_bendersdecomp.h"
#include "nmp_benders_conshdlr.h"
#include "../Maintenance_Problem.h"

#include <scip/prob.h>
#include <scip/struct_var.h>


struct SCIP_VarData_Master{
  SCIP* subproblem_lpsol;
  SCIP_VAR* edgepot_of_subproblem_lpsol;
  SCIP* subproblem_heursol;
  SCIP_VAR* edgepot_of_subproblem_heursol;
};

struct SCIP_Probdata_Subproblem{
  int epoch;
  int nepochs;
  int nedges;
  int number_of_added_cons;
};

struct SCIP_VarData_Subproblem{
  SCIP_Real capacity;
  SCIP* master;
  SCIP_VAR* decision_of_master;
};

SCIP_RETCODE del_vardata_master(SCIP* scip, SCIP_VAR* var, SCIP_VARDATA** vardata_unpec){
  std::cout << "deleting" << std::endl;
  SCIP_VarData_Master** vardata = (SCIP_VarData_Master**) vardata_unpec;
  SCIPreleaseVar((*vardata)->subproblem_lpsol, &(*vardata)->edgepot_of_subproblem_lpsol);
  SCIPreleaseVar((*vardata)->subproblem_heursol, &(*vardata)->edgepot_of_subproblem_heursol);

  SCIPfreeBlockMemory(scip, vardata);

  return SCIP_OKAY;
}

SCIP_RETCODE del_vardata_subproblem(SCIP* scip, SCIP_VAR* var, SCIP_VARDATA** vardata_unpec){
  std::cout << "deleting" << std::endl;
  SCIP_VarData_Subproblem** vardata = (SCIP_VarData_Subproblem**) vardata_unpec;
  SCIPreleaseVar((*vardata)->master, &(*vardata)->decision_of_master);

  SCIPfreeBlockMemory(scip, vardata);

  return SCIP_OKAY;
}

SCIP_RETCODE del_probdata_subproblem(SCIP* scip, SCIP_PROBDATA** probdata_unpec){
  std::cout << "deleting" << std::endl;
  SCIP_Probdata_Subproblem** probdata = (SCIP_Probdata_Subproblem**) probdata_unpec;
  SCIPfreeBlockMemory(scip, probdata);

  return SCIP_OKAY;
}

std::pair<
  Linear_Program,
  std::pair<
    std::map<
      std::pair<const Node*,std::string>,
      std::pair<Variable*, size_t>
    >,
    std::map<
      std::pair<const Edge*,std::string>,
      std::pair<Variable*, size_t>
    >
  >
> generate_dual_flow_problem(const Graph& graph, const Node* source, const Node* target){
  Linear_Program lp("min_cut_lp", false);

  assert(graph.contains(source));
  assert(graph.contains(target));

  for(std::string attr : std::vector({"Upper", "Flow"})){
    if(!graph.edge_template(attr).first) {
      std::cout << graph << std::endl;
      throw std::invalid_argument("generate_dual_flow_problem needs Graph with predetermined attributes present");
    }
  }


  std::pair<
    std::map<
      std::pair<const Node*,std::string>,
      std::pair<Variable*, size_t>
    >,
    std::map<
      std::pair<const Edge*,std::string>,
      std::pair<Variable*, size_t>
    >
  > lookup;

  /*
    constraints
  */
  lookup = lp_generator::grow_from_graph(
    lp,
    graph,
    [](const Edge* edge) -> std::vector<constraint_data> {
      std::stringstream capacity_name;
      capacity_name << "edge_" << edge->description();

      std::vector<constraint_data> constraints;
      constraints.push_back({capacity_name.str(), Inequality, {{{{edge->from(), "Nodepotential"}, 1}, {{edge->to(), "Nodepotential"}, -1}}, {{{edge, "Edgepotential"}, -1}}}, 0});

      return constraints;
    },
    [source, target](const Node* node) -> std::vector<constraint_data> {

      std::vector<constraint_data> constraints;
      if(node == source){
        constraints.push_back({"set_source_potential", Equality, {{{{source, "Nodepotential"}, 1}}, {} }, 1});
      }else if(node == target){
        constraints.push_back({"set_target_potential", Equality, {{{{target, "Nodepotential"}, 1}}, {} }, 0});
      }
      return constraints;
    },
    {{"Edgepotential", {Continuous, {true, 0}, {false, 1}}}},
    {{"Nodepotential", {Continuous, {false, 0}, {false, 1}}}},
    ""
  );

  return {lp, lookup};
}

std::pair<
  Linear_Program,
  std::vector<
    std::pair<
      std::map<
        std::pair<const Node*,std::string>,
        std::pair<Variable*, size_t>
      >,
      std::map<
        std::pair<const Edge*,std::string>,
        std::pair<Variable*, size_t>
      >
    >
  >
> generate_master(Maintenance_Problem& nmp, const Graph& graph, const Node* source, const Node* target){
  Linear_Program lp("maintenance_problem_master_ip", true);

  std::unordered_set<const Edge*> critical = graph.gather_edges({"Selected"});

  for(std::string attr : std::vector({"Selected", "Upper", "Flow"})){
    if(!graph.edge_template(attr).first) {
      std::cout << graph << std::endl;
      throw std::invalid_argument("Maintenance_Problem needs Graph with predetermined attributes present");
    }
  }


  std::vector<
    std::pair<
      std::map<
        std::pair<const Node*,std::string>,
        std::pair<Variable*, size_t>
      >,
      std::map<
        std::pair<const Edge*,std::string>,
        std::pair<Variable*, size_t>
      >
    >
  > lookup;

  // generate variables and set decision for unmaintained edges
  for(size_t epoch = 0; epoch < nmp.number_of_epochs(); ++epoch){
    std::stringstream name_appendix;
    name_appendix << "epoch_" << epoch;
    lookup.push_back(lp_generator::grow_from_graph(
      lp,
      graph,
      [&critical, epoch](const Edge* edge) -> std::vector<constraint_data> {
        std::stringstream non_critical_fix;
        non_critical_fix << "edge_" << edge->description() << "_non_critical_epoch_" << epoch;

        if(critical.find(edge) == critical.end()){
          return {{non_critical_fix.str(), Equality, {{}, {{{edge, "Selected"}, 1}}}, 0}};
        }
        return {};
      },
      [](const Node* node) -> std::vector<constraint_data> {
        return {};
      },
      {{"Selected", {Integral, {true, 0}, {true, 1}}}},
      {},
      name_appendix.str()
    ));
  }

  /*
    critical edges processed
  */
  for(const Edge* e : critical){
    std::unordered_map<Variable*, Coefficient> lhs;
    for(auto lookup_during_epoch : lookup){
      lhs.insert({lookup_during_epoch.second.find({e, "Selected"})->second.first, {1}});
    }
    std::stringstream name;
    name << "critical_edge_" << e->description() << "_processed";
    lp.polyeder().add_constraint(
      { // constraint
        name.str(),
        Equality,
        lhs,
        1,
      }
    ); // TODO: consider making a inequality
  }

  std::pair<Variable*, size_t> target_var = lp.polyeder().add_variable(Variable("target_variable", Continuous));
  lp.add_direction_coefficient({target_var.first, 1});

  return {lp, lookup};
}

SCIP* generate_nmp_bendersdecomp(Maintenance_Problem& nmp, SCIP* scip_basic, SCIP_SOL* opt){
  // generating programs:
  std::cout << "generating master program" << std::endl;
  auto master_and_lookup = generate_master(nmp, nmp.network(), nmp.source(), nmp.target());
  auto computational_master_and_lookup = master_and_lookup.first.computational_model();

  std::cout << "generating subproblems: ";
  std::vector<
    std::pair<
      Linear_Program,
      std::pair<
        std::map<
          std::pair<const Node*,std::string>,
          std::pair<Variable*, size_t>
        >,
        std::map<
          std::pair<const Edge*,std::string>,
          std::pair<Variable*, size_t>
        >
      >
    >
  > subproblems_with_lookups_lpsols;
  subproblems_with_lookups_lpsols.reserve(nmp.number_of_epochs());
  std::vector<
    std::pair<
      SCIP*,
      std::unordered_map<Variable*, SCIP_VAR*>
    >
  > computational_subproblems_with_lookups_lpsols;
  computational_subproblems_with_lookups_lpsols.reserve(nmp.number_of_epochs());
  std::vector<
    std::pair<
      Linear_Program,
      std::pair<
        std::map<
          std::pair<const Node*,std::string>,
          std::pair<Variable*, size_t>
        >,
        std::map<
          std::pair<const Edge*,std::string>,
          std::pair<Variable*, size_t>
        >
      >
    >
  > subproblems_with_lookups_heursols;
  subproblems_with_lookups_heursols.reserve(nmp.number_of_epochs());
  std::vector<
    std::pair<
      SCIP*,
      std::unordered_map<Variable*, SCIP_VAR*>
    >
  > computational_subproblems_with_lookups_heursols;
  computational_subproblems_with_lookups_heursols.reserve(nmp.number_of_epochs());

  for( size_t epoch = 0; epoch < nmp.number_of_epochs(); epoch++){
    std::cout << "[" << epoch << "] " << std::flush;
    subproblems_with_lookups_lpsols.push_back(generate_dual_flow_problem(nmp.network(), nmp.source(), nmp.target()));
    computational_subproblems_with_lookups_lpsols.push_back(subproblems_with_lookups_lpsols[epoch].first.computational_model());
    SCIP_CALL_ABORT( SCIPsetMessagehdlr(computational_subproblems_with_lookups_lpsols[epoch].first, NULL));

    subproblems_with_lookups_heursols.push_back(generate_dual_flow_problem(nmp.network(), nmp.source(), nmp.target()));
    computational_subproblems_with_lookups_heursols.push_back(subproblems_with_lookups_heursols[epoch].first.computational_model());
    SCIP_CALL_ABORT( SCIPsetMessagehdlr(computational_subproblems_with_lookups_heursols[epoch].first, NULL));
  }
  std::cout << "\n";

  std::cout << "creating lookup tables to translate between decision variables in master and their flow variables in the subproblems: ";
  for( size_t epoch = 0; epoch < nmp.number_of_epochs(); epoch++){
    std::cout << "[" << epoch << "] " << std::flush;
    /*
      For every edge and epoch combination we have created decision
      variables in the master and a flow Edgepotential variable in a
      subproblem.
      We link these in their respective vardata for quick access.
      (This is in contrast to benders_default.c where this translation is achieved with hashmaps)
    */
    nmp.network().for_edges([
                              &nmp,
                              &master_and_lookup,
                              &computational_master_and_lookup,
                              &subproblems_with_lookups_lpsols,
                              &computational_subproblems_with_lookups_lpsols,
                              &subproblems_with_lookups_heursols,
                              &computational_subproblems_with_lookups_heursols,
                              &epoch
                            ](const Edge* e){
      Variable* master_var = master_and_lookup.second[epoch].second.find({e, "Selected"})->second.first;
      SCIP_VAR* computational_master_var = computational_master_and_lookup.second.find(master_var)->second;

      Variable* subproblem_var_lpsol = subproblems_with_lookups_lpsols[epoch].second.second.find({e, "Edgepotential"})->second.first;
      SCIP_VAR* computational_subproblem_var_lpsol = computational_subproblems_with_lookups_lpsols[epoch].second.find(subproblem_var_lpsol)->second;

      Variable* subproblem_var_heursol = subproblems_with_lookups_heursols[epoch].second.second.find({e, "Edgepotential"})->second.first;
      SCIP_VAR* computational_subproblem_var_heursol = computational_subproblems_with_lookups_heursols[epoch].second.find(subproblem_var_heursol)->second;

      // linking computational variables by setting vardata appropriately
      SCIP_VarData_Master* master_vardata;
      SCIP_CALL_ABORT( SCIPallocBlockMemory(computational_master_and_lookup.first, &master_vardata));
      *master_vardata = SCIP_VarData_Master{
        computational_subproblems_with_lookups_lpsols[epoch].first,
        computational_subproblem_var_lpsol,
        computational_subproblems_with_lookups_heursols[epoch].first,
        computational_subproblem_var_heursol
      };
      SCIPvarSetData(computational_master_var, (SCIP_VARDATA*) master_vardata);
      SCIPvarSetDelorigData(computational_master_var, del_vardata_master);

      int nedges = nmp.network().size().edges;
      SCIP_VarData_Subproblem* subproblem_vardata_lpsol;
      SCIP_CALL_ABORT( SCIPallocBlockMemory(computational_subproblems_with_lookups_lpsols[epoch].first, &subproblem_vardata_lpsol));
      *subproblem_vardata_lpsol = SCIP_VarData_Subproblem{
        e->attribute_throwing("Upper").value(),
        computational_master_and_lookup.first,
        computational_master_var
      };
      SCIPvarSetData(computational_subproblem_var_lpsol, (SCIP_VARDATA*) subproblem_vardata_lpsol);
      SCIPvarSetDelorigData(computational_subproblem_var_lpsol, del_vardata_subproblem);


      SCIP_VarData_Subproblem* subproblem_vardata_heursol;
      SCIP_CALL_ABORT( SCIPallocBlockMemory(computational_subproblems_with_lookups_heursols[epoch].first, &subproblem_vardata_heursol));
      *subproblem_vardata_heursol = SCIP_VarData_Subproblem{
        e->attribute_throwing("Upper").value(),
        computational_master_and_lookup.first,
        computational_master_var
      };
      SCIPvarSetData(computational_subproblem_var_heursol, (SCIP_VARDATA*) subproblem_vardata_heursol);
      SCIPvarSetDelorigData(computational_subproblem_var_heursol, del_vardata_subproblem);
    });
/* (this does not seem to be necessary)
    // forcing vardata of the other variables to NULL in order to reliably differentiate between them
    for(Node* n : nmp.network().nodes()){
      Variable* subproblem_var_lpsol = subproblems_with_lookups_lpsols[epoch].second.first.find({n, "Nodepotential"})->second.first;
      SCIP_VAR* computational_subproblem_var_lpsol = computational_subproblems_with_lookups_lpsols[epoch].second.find(subproblem_var_lpsol)->second;

      Variable* subproblem_var_heursol = subproblems_with_lookups_heursols[epoch].second.first.find({n, "Nodepotential"})->second.first;
      SCIP_VAR* computational_subproblem_var_heursol = computational_subproblems_with_lookups_heursols[epoch].second.find(subproblem_var_heursol)->second;

      SCIPvarSetData(computational_subproblem_var_lpsol, NULL);
      SCIPvarSetData(computational_subproblem_var_heursol, NULL);
    }*/
  }
  std::cout << "\n";


  // copying subproblem ptrs into continuous array (also setting reopt flag and probdata)
  std::cout << "Initializing Benders Conshdlr" << std::endl;
  SCIP** benders_separators_lpsols;
  SCIP_CALL_ABORT( SCIPallocBlockMemoryArray(computational_master_and_lookup.first, &benders_separators_lpsols, nmp.number_of_epochs()));
  SCIP** benders_separators_heursols;
  SCIP_CALL_ABORT( SCIPallocBlockMemoryArray(computational_master_and_lookup.first, &benders_separators_heursols, nmp.number_of_epochs()));

  for(size_t epoch = 0; epoch < nmp.number_of_epochs(); epoch++){
    benders_separators_lpsols[epoch] = computational_subproblems_with_lookups_lpsols[epoch].first;
    SCIP_Probdata_Subproblem* probdata_lpsol;
    SCIP_CALL_ABORT( SCIPallocBlockMemory(benders_separators_lpsols[epoch], &probdata_lpsol));
    *probdata_lpsol = SCIP_Probdata_Subproblem{
      (int) epoch,
      (int) nmp.number_of_epochs(),
      (int) nmp.network().size().edges,
      0
    };
    SCIP_CALL_ABORT( SCIPsetProbData(benders_separators_lpsols[epoch], (SCIP_PROBDATA*) probdata_lpsol));
    SCIP_CALL_ABORT( SCIPsetProbDelorig(benders_separators_lpsols[epoch], del_probdata_subproblem));


    benders_separators_heursols[epoch] = computational_subproblems_with_lookups_heursols[epoch].first;
    SCIP_Probdata_Subproblem* probdata_heursol;
    SCIP_CALL_ABORT( SCIPallocBlockMemory(benders_separators_heursols[epoch], &probdata_heursol));
    *probdata_heursol = SCIP_Probdata_Subproblem{
      (int) epoch,
      (int) nmp.number_of_epochs(),
      (int) nmp.network().size().edges,
      0
    };
    SCIP_CALL_ABORT( SCIPsetProbData(benders_separators_heursols[epoch], (SCIP_PROBDATA*) probdata_heursol));
    SCIP_CALL_ABORT( SCIPsetProbDelorig(benders_separators_heursols[epoch], del_probdata_subproblem));

    SCIP_CALL_ABORT( SCIPenableReoptimization( benders_separators_lpsols[epoch], TRUE));
    SCIP_CALL_ABORT( SCIPenableReoptimization( benders_separators_heursols[epoch], TRUE));
  }


  SCIP_CALL_ABORT( SCIPsetBoolParam(computational_master_and_lookup.first, "constraints/benders/active", TRUE) );
  SCIP_CALL_ABORT( SCIPsetIntParam(computational_master_and_lookup.first, "propagating/maxrounds", 0) );
  SCIP_CALL_ABORT( SCIPsetIntParam(computational_master_and_lookup.first, "propagating/maxroundsroot", 0) );
  SCIP_CALL_ABORT( SCIPsetIntParam(computational_master_and_lookup.first, "heuristics/trysol/freq", 1) );
  SCIP_CALL_ABORT( SCIPsetIntParam(computational_master_and_lookup.first, "presolving/maxrestarts", 0) );
  SCIP_CALL_ABORT( SCIPsetIntParam(computational_master_and_lookup.first, "constraints/benders/maxprerounds", 0) );
  SCIP_CALL_ABORT( SCIPsetIntParam(computational_master_and_lookup.first, "presolving/maxrounds", 0) );

  SCIP_SOL* master_opt;
  SCIPcreateSol(computational_master_and_lookup.first, &master_opt, NULL);
  SCIP_VAR** vars = SCIPgetOrigVars(computational_master_and_lookup.first);
  int nvars = SCIPgetNOrigVars(computational_master_and_lookup.first);
  for(int index = 0; index < nvars; index++){
    SCIPsetSolVal(computational_master_and_lookup.first, master_opt, vars[index], SCIPgetSolVal(scip_basic, opt, SCIPfindVar(scip_basic, SCIPvarGetName(vars[index]))) );
  }


  SCIP_CALL_ABORT( SCIPincludeConshdlrNMPBendersLP( computational_master_and_lookup.first, benders_separators_lpsols, benders_separators_heursols, nmp.number_of_epochs(), nmp.network().size().edges, master_opt));
  SCIP_CALL_ABORT( SCIPincludeConshdlrNMPBendersBranch( computational_master_and_lookup.first, benders_separators_lpsols, benders_separators_heursols, nmp.number_of_epochs(), nmp.network().size().edges));

  // returning just the scip ptr to our master
  return computational_master_and_lookup.first;
}
