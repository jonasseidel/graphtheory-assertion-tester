#ifndef POLYEDER_H
#define POLYEDER_H

#include <cstddef>
#include <vector>
#include <sstream>
#include <cassert>
#include <stdexcept>

#include <boost/bimap.hpp>

#include "Variable.h"
#include "Constraint.h"

class Polyeder{
  boost::bimap<Variable*, size_t> _variables;
  std::vector<Constraint> _constraints;
public:
  Polyeder();
  Polyeder(const Polyeder& p);
  Polyeder(Polyeder&& p);
  std::pair<Polyeder, std::unordered_map<const Variable*, Variable*>> copy_ret_lookup() const ;

  boost::bimap<Variable*, size_t>& variables();
  size_t vs_dim() const ;
  Variable& variable(size_t index);
  const Variable& variable(size_t index) const ;
  size_t variable(Variable& var) const ;
  std::pair<Variable*, size_t> add_variable(const Variable& var);

  std::vector<Constraint>& constraints();
  const std::vector<Constraint>& constraints() const ;
  size_t number_of_constraints() const ;
  Constraint& constraint(size_t index);
  void add_constraint(const Constraint& constraint);
  void add_constraint(const Constraint& constraint, const std::unordered_map<const Variable*, Variable*>& variable_lookup);

  std::string variable_identifier(size_t index) const ;

  void operator=(const Polyeder& p);
  void operator=(Polyeder&& p);

  ~Polyeder();

  friend std::ostream& operator<<(std::ostream& os, const Polyeder& polyeder);
};

std::ostream& operator<<(std::ostream& os, const Polyeder& polyeder);

#endif
