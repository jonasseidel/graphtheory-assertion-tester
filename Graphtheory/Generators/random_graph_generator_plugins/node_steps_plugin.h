#ifndef NODE_STEP_PLUGIN_H
#define NODE_STEP_PLUGIN_H

#include <string>
#include <iostream>
#include <vector>
#include <unordered_set>
#include <exception>
#include <stdexcept>

#include "../../../Common/random_set_element_generator.h"
#include "../../Graph.h"

class node_step_plugin{
  bool _initialized;
protected:
  size_t _number_of_nodes;
  size_t _number_of_steps;
  random_attribute_generator _node_attribute_generator;

  std::function<std::string()> _get_node_step_plugin_type;
  std::function<std::vector<std::unordered_set<Node*>>(Graph& g)> _add_node_steps;
  std::function<void(node_step_plugin* here)> _init;

  node_step_plugin(size_t number_of_nodes, size_t number_of_steps, random_attribute_generator node_attribute_generator)
  : _initialized(false),
    _number_of_nodes(number_of_nodes), _number_of_steps(number_of_steps),
    _node_attribute_generator(node_attribute_generator),
    _get_node_step_plugin_type([]() -> std::string { return "temporary_non_descript_node_step_plugin"; }),
    _add_node_steps([](Graph& g) -> std::vector<std::unordered_set<Node*>> { throw std::logic_error("all instances should be derived"); }),
    _init([](node_step_plugin* here){  })
  {}
public:
  node_step_plugin()
  : _initialized(false),
    _number_of_nodes(0), _number_of_steps(0),
    _node_attribute_generator(),
    _get_node_step_plugin_type([]() -> std::string { return "temporary_non_descript_node_step_plugin"; }),
    _add_node_steps([](Graph& g) -> std::vector<std::unordered_set<Node*>> { throw std::logic_error("all instances should be derived"); }),
    _init([](node_step_plugin* here){  })
  {}

  std::string get_node_step_plugin_type() const {return this->_get_node_step_plugin_type();}
  std::vector<std::unordered_set<Node*>> add_node_steps(Graph& g) {if(!this->_initialized) throw std::runtime_error("initialize before use"); return this->_add_node_steps(g);}
  void init(){this->_init(this); this->_initialized = true;}
  void uninit(){this->_initialized = false;}

  node_step_plugin(const node_step_plugin& other){
    *this = other;
  }
  node_step_plugin(node_step_plugin&& other){
    *this = std::move(other);
  }


  static std::string csv_columns(std::string prefix, std::string separator);
  std::string csv_data(std::string separator) const ;

  void operator=(const node_step_plugin& other);
  void operator=(node_step_plugin&& other);

  bool operator==(const node_step_plugin& other) const ;
  bool operator!=(const node_step_plugin& other) const {
    return !(*this == other);
  }

  node_step_plugin(std::istream& is);

  friend std::ostream& operator<<(std::ostream& os, const node_step_plugin& node_step_gen);
  friend std::istream& operator>>(std::istream& is, node_step_plugin& node_step_gen);
};

std::ostream& operator<<(std::ostream& os, const node_step_plugin& node_step_gen);
std::istream& operator>>(std::istream& is, node_step_plugin& node_step_gen);

#endif
