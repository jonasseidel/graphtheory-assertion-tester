#include "edge_step_fuzzing_plugin.h"
#include "../../../Common/io_format.h"

// enumerate all edge_step_fuzzing_plugin derivatives
#include "uniform_edge_step_fuzzing.h"

void edge_step_fuzzing_plugin::operator=(const edge_step_fuzzing_plugin& other){
  this->_initialized = false;
  this->_number_of_edges = other._number_of_edges;
  this->_fuzzing_distance_from = other._fuzzing_distance_from;
  this->_fuzzing_distance_to = other._fuzzing_distance_to;
  this->_edge_attribute_generator = other._edge_attribute_generator;
  this->_at_least_strongly_connected = other._at_least_strongly_connected;
  this->_at_least_weakly_connected = other._at_least_weakly_connected;
  this->_acyclic = other._acyclic;
  this->_simple = other._simple;
  this->_anti_reflexive = other._anti_reflexive;
  this->_get_edge_step_fuzzing_plugin_type = other._get_edge_step_fuzzing_plugin_type;
  this->_add_edges = other._add_edges;
  this->_init = other._init;
  this->init();
}
void edge_step_fuzzing_plugin::operator=(edge_step_fuzzing_plugin&& other){
  this->_initialized = false;
  this->_number_of_edges = std::move(other._number_of_edges);
  this->_fuzzing_distance_from = std::move(other._fuzzing_distance_from);
  this->_fuzzing_distance_to = std::move(other._fuzzing_distance_to);
  this->_edge_attribute_generator = std::move(other._edge_attribute_generator);
  this->_at_least_strongly_connected = std::move(other._at_least_strongly_connected);
  this->_at_least_weakly_connected = std::move(other._at_least_weakly_connected);
  this->_acyclic = std::move(other._acyclic);
  this->_simple = std::move(other._simple);
  this->_anti_reflexive = std::move(other._anti_reflexive);
  this->_get_edge_step_fuzzing_plugin_type = std::move(other._get_edge_step_fuzzing_plugin_type);
  this->_add_edges = std::move(other._add_edges);
  this->_init = std::move(other._init);
  this->init();
}


std::string edge_step_fuzzing_plugin::csv_columns(std::string prefix, std::string separator){
  std::stringstream csv_columns_stream;
  csv_columns_stream << prefix << "number_of_edges" << separator
                      << prefix << "fuzzing_distance_from" << separator
                      << prefix << "fuzzing_distance_to" << separator
                      << random_attribute_generator::csv_columns(prefix, separator) << separator
                      << prefix << "at_least_strongly_connected" << separator
                      << prefix << "at_least_weakly_connected" << separator
                      << prefix << "acyclic" << separator
                      << prefix << "simple" << separator
                      << prefix << "anti_reflexive";
  return csv_columns_stream.str();
}
std::string edge_step_fuzzing_plugin::csv_data(std::string separator) const {
  std::stringstream csv_data;
  csv_data << this->_number_of_edges << separator
            << this->_fuzzing_distance_from << separator
            << this->_fuzzing_distance_to << separator
            << this->_edge_attribute_generator.csv_data(separator) << separator
            << this->_at_least_strongly_connected << separator
            << this->_at_least_weakly_connected << separator
            << this->_acyclic << separator
            << this->_simple << separator
            << this->_anti_reflexive;
  return csv_data.str();
}

bool edge_step_fuzzing_plugin::operator==(const edge_step_fuzzing_plugin& other) const {
  if(this->get_edge_step_fuzzing_plugin_type() != other.get_edge_step_fuzzing_plugin_type()) return false;
  if(this->_number_of_edges != other._number_of_edges ) return false;
  if(this->_fuzzing_distance_from != other._fuzzing_distance_from ) return false;
  if(this->_fuzzing_distance_to != other._fuzzing_distance_to ) return false;
  if(this->_edge_attribute_generator != other._edge_attribute_generator ) return false;
  if(this->_at_least_strongly_connected != other._at_least_strongly_connected ) return false;
  if(this->_at_least_weakly_connected != other._at_least_weakly_connected ) return false;
  if(this->_acyclic != other._acyclic ) return false;
  if(this->_simple != other._simple ) return false;
  if(this->_anti_reflexive != other._anti_reflexive ) return false;
  return true;
}

edge_step_fuzzing_plugin::edge_step_fuzzing_plugin(std::istream& is){
  std::string curr;
  is >> curr;
  IO_THROW(curr, "edge_step_fuzzing_plugin");
  is >> curr;
  IO_THROW(curr, "{");
  std::string get_edge_step_fuzzing_plugin_type;
  is >> get_edge_step_fuzzing_plugin_type
      >> this->_number_of_edges
      >> this->_fuzzing_distance_from
      >> this->_fuzzing_distance_to
      >> this->_edge_attribute_generator
      >> this->_at_least_strongly_connected
      >> this->_at_least_weakly_connected
      >> this->_acyclic
      >> this->_simple
      >> this->_anti_reflexive;
  is >> curr;
  IO_THROW(curr, "}");
  if(!is) throw std::invalid_argument("stream did not contain complete edge_step_fuzzing_plugin object");

  if(get_edge_step_fuzzing_plugin_type == "uniform_edge_step_fuzzing"){
    *this = uniform_edge_step_fuzzing(this->_number_of_edges, this->_fuzzing_distance_from, this->_fuzzing_distance_to, this->_edge_attribute_generator, this->_at_least_strongly_connected, this->_at_least_weakly_connected, this->_acyclic, this->_simple, this->_anti_reflexive);
  }else{
    throw std::range_error("you need to manually extend the node_steps_plugin read operator to support your plugins");
  }
}

std::ostream& operator<<(std::ostream& os, const edge_step_fuzzing_plugin& edge_step_gen){
  os << "edge_step_fuzzing_plugin" << " {" << "\n"
      << edge_step_gen.get_edge_step_fuzzing_plugin_type() << "\n"
      << edge_step_gen._number_of_edges << "\n"
      << edge_step_gen._fuzzing_distance_from << " "
      << edge_step_gen._fuzzing_distance_to << " "
      << edge_step_gen._edge_attribute_generator << "\n"
      << edge_step_gen._at_least_strongly_connected << " "
      << edge_step_gen._at_least_weakly_connected << " "
      << edge_step_gen._acyclic << " "
      << edge_step_gen._simple << " "
      << edge_step_gen._anti_reflexive << "\n"
      << "}";
  return os;
}

std::istream& operator>>(std::istream& is, edge_step_fuzzing_plugin& edge_step_gen){
  edge_step_gen = edge_step_fuzzing_plugin(is);
  return is;
}
