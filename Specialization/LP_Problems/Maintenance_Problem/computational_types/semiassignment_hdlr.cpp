#include "semiassignment_hdlr.h"


struct SCIP_ConsData
{
  int ndisabled_vars;
  SCIP_VAR** disabled_vars;
};

static
SCIP_RETCODE enforce_semiassign(SCIP* scip, SCIP_CONSHDLR* conshdlr, SCIP_CONS** conss, int nconss, int nusefulconss, SCIP_Bool solinfeasible, SCIP_RESULT* result)
{
  // Q: is this necessary, if we fixed the variable?


  return SCIP_OKAY;
}

static
SCIP_RETCODE enforce_relax_semiassign (SCIP* scip, SCIP_SOL* sol, SCIP_CONSHDLR* conshdlr, SCIP_CONS** conss, int nconss, int nusefulconss, SCIP_Bool solinfeasible, SCIP_RESULT* result)
{
   SCIPABORT();

   return SCIP_OKAY;
}

static
SCIP_RETCODE enforce_pseudo_semiassign(SCIP* scip, SCIP_CONSHDLR* conshdlr, SCIP_CONS** conss, int nconss, int nusefulconss, SCIP_Bool solinfeasible, SCIP_Bool objinfeasible, SCIP_RESULT* result)
{
   //SCIPerrorMessage("method of xyz constraint handler not implemented yet\n");
   //SCIPABORT();

   *result = SCIP_SOLVELP;
   return SCIP_OKAY;
}

static
SCIP_RETCODE check_semiassign(SCIP* scip, SCIP_CONSHDLR* conshdlr, SCIP_CONS** conss, int nconss, SCIP_SOL* sol, SCIP_Bool checkintegrality, SCIP_Bool checklprows, SCIP_Bool printreason, SCIP_Bool completely, SCIP_RESULT* result)
{
  // Q: is this necessary, if we fixed the variable?
  *result = SCIP_FEASIBLE;
  for(int cons_index = 0; cons_index < nconss; cons_index++){
    auto cons_data = SCIPconsGetData(conss[cons_index]);
    for(int var_index = 0; var_index < cons_data->ndisabled_vars; var_index++){
      if(SCIPgetSolVal(scip, sol, cons_data->disabled_vars[var_index]) != 0){
        *result = SCIP_INFEASIBLE;
      }
    }
  }

  return SCIP_OKAY;
}

static
SCIP_RETCODE lock_semiassign(SCIP* scip, SCIP_CONSHDLR* conshdlr, SCIP_CONS* cons, SCIP_LOCKTYPE locktype, int nlockspos, int nlocksneg)
{
  // Q: is this necessary, if we fixed the variable?
  auto cons_data = SCIPconsGetData(cons);
  for(int var_index = 0; var_index < cons_data->ndisabled_vars; var_index++){
    SCIP_CALL( SCIPaddVarLocks(scip, cons_data->disabled_vars[var_index], nlockspos+nlocksneg, nlockspos+nlocksneg));
  }

  return SCIP_OKAY;
}

SCIP_RETCODE activate_semiassign(SCIP* scip, SCIP_CONSHDLR* conshdlr, SCIP_CONS* cons){
  for(int var_index = 0; var_index < cons->consdata->ndisabled_vars; var_index++){
    SCIP_Bool infeasible = FALSE;
    SCIP_Bool fixed = FALSE;

    if(!SCIPisFeasZero(scip, SCIPvarGetUbLocal(cons->consdata->disabled_vars[var_index]))){
      SCIP_CALL( SCIPfixVar(scip, cons->consdata->disabled_vars[var_index], 0.0, &infeasible, &fixed));
    }
  }

  return SCIP_OKAY;
}


SCIP_RETCODE propagate_semiassign(SCIP* scip, SCIP_CONSHDLR* conshdlr, SCIP_CONS** conss, int nconss, int nusefulconss, int nmarkedconss, SCIP_PROPTIMING proptiming, SCIP_RESULT* result){
  std::cout << "propagating" << std::endl;
  *result = SCIP_DIDNOTFIND;
  // Q: which cons to check and when?
  for(size_t cons_index = 0; cons_index < nconss /*std::max(nusefulconss, nmarkedconss)*/; cons_index++){
    if(*result == SCIP_CUTOFF){
      break;
    }

    SCIP_CONS* cons = *(conss+cons_index);

    assert(SCIPconsIsActive(cons));

    for(int var_index = 0; var_index < cons->consdata->ndisabled_vars; var_index++){
      SCIP_Bool infeasible = FALSE;
      SCIP_Bool fixed = FALSE;

      SCIP_CALL( SCIPfixVar(scip, cons->consdata->disabled_vars[var_index], 0.0, &infeasible, &fixed));
      if(infeasible){
        *result = SCIP_CUTOFF;
      }else{
        //assert(fixed);
        *result = SCIP_REDUCEDDOM;
      }
    }
  }
  return SCIP_OKAY;
}

SCIP_RETCODE SCIPincludeConshdlrSemiassign(
   SCIP*                 scip
   )
{
  std::cout << "conshdlr added" << std::endl;

  SCIP_CONSHDLRDATA* conshdlrdata;
  SCIP_CONSHDLR* conshdlr;

  conshdlrdata = NULL;

  SCIP_CALL( SCIPincludeConshdlrBasic(scip, &conshdlr, "semiassign", "manages branching on logical or constraints. Tracks deactivation status of variables.",
         1, 1, -1, TRUE,
         enforce_semiassign, enforce_pseudo_semiassign, check_semiassign, lock_semiassign,
         conshdlrdata) );
  assert(conshdlr != NULL);

   // Q: Timing and priority reasoning?
   /*SCIP_CALL( SCIPsetConshdlrProp(scip, conshdlr, propagate_semiassign, 5, FALSE,
      SCIP_PROPTIMING_BEFORELP) );*/
  SCIP_CALL( SCIPsetConshdlrActive(scip, conshdlr, activate_semiassign) );

  return SCIP_OKAY;
}

SCIP_RETCODE SCIPcreateConsSemiassign(
   SCIP*                 scip,
   SCIP_CONS**           cons,
   const char*           name,
   int                   nvars,
   SCIP_VAR**            vars
   )
{
  SCIP_CONSHDLR* conshdlr;
  SCIP_CONSDATA* consdata;


  conshdlr = SCIPfindConshdlr(scip, "semiassign");
  if( conshdlr == NULL )
  {
    SCIPerrorMessage("semiassign constraint handler not found\n");
    return SCIP_PLUGINNOTFOUND;
  }

  SCIP_CALL( SCIPallocBlockMemory(scip, &consdata) );
  consdata->ndisabled_vars = nvars;
  consdata->disabled_vars = vars;

  /* create constraint */
  // Q: enforce?
  SCIP_CALL( SCIPcreateCons(scip, cons, name, conshdlr, consdata, FALSE, FALSE, TRUE, TRUE, TRUE,
         TRUE, FALSE, FALSE, FALSE, TRUE) );

  return SCIP_OKAY;
}
