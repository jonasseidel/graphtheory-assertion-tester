#include "critical_selection_policy.h"

std::unordered_set<Edge*> critical_selection_policy::critical_edge_candidates(Graph& g, const std::unordered_set<Edge*>& core_network_param, const std::unordered_set<Edge*>& network_cores_param, const std::unordered_set<Edge*>& aggregated_networks_param, const std::unordered_set<Edge*>& network_connectors_param) const {
  switch(this->selection_rule){
    case everywhere:
      return g.export_edges();
      break;
    case core_network:
      return core_network_param;
      break;
    case network_cores:
      return network_cores_param;
      break;
    case aggregated_networks:
      return aggregated_networks_param;
      break;
    case network_connectors:
      return network_connectors_param;
      break;
    default:
      throw std::invalid_argument("invalid value of enum critical_edge_candidates::policy");
  }
  return {};
}

size_t critical_selection_policy::number_of_critical_candidates(Graph& g, const std::unordered_set<Edge*>& core_network_param, const std::unordered_set<Edge*>& network_cores_param, const std::unordered_set<Edge*>& aggregated_networks_param, const std::unordered_set<Edge*>& network_connectors_param) const {
  switch(this->selection_rule){
    case everywhere:
      return g.size().edges;
      break;
    case core_network:
      return core_network_param.size();
      break;
    case network_cores:
      return network_cores_param.size();
      break;
    case aggregated_networks:
      return aggregated_networks_param.size();
      break;
    case network_connectors:
      return network_connectors_param.size();
      break;
    default:
      throw std::invalid_argument("invalid value of enum critical_edge_candidates::policy");
  }
  return {};
}

std::string critical_selection_policy::csv_columns(std::string prefix, std::string separator){
  std::stringstream csv_columns_stream;
  csv_columns_stream << prefix << "critical_selection_policy";

  return csv_columns_stream.str();
}

std::string critical_selection_policy::csv_data(std::string separator) const {
  switch(this->selection_rule){
    case everywhere:
      return "everywhere";
      break;
    case core_network:
      return "core_network";
      break;
    case network_cores:
      return "network_cores";
      break;
    case aggregated_networks:
      return "aggregated_networks";
      break;
    case network_connectors:
      return "network_connectors";
      break;
    default:
      throw std::invalid_argument("invalid value of enum critical_edge_candidates::policy");
  }
  return {};
}

bool critical_selection_policy::operator==(const critical_selection_policy& other) const {
  return (this->selection_rule == other.selection_rule);
}

critical_selection_policy::critical_selection_policy(std::istream& is){
  std::string curr;
  is >> curr;

  if(curr == "everywhere"){
    this->selection_rule = everywhere;
  }else if( curr == "core_network" ){
    this->selection_rule = core_network;
  }else if( curr == "network_cores" ){
    this->selection_rule = network_cores;
  }else if( curr == "aggregated_networks" ){
    this->selection_rule = aggregated_networks;
  }else if( curr == "network_connectors" ){
    this->selection_rule = network_connectors;
  }
}

std::ostream& operator<<(std::ostream& os, const critical_selection_policy& tipping_parameters){
  switch(tipping_parameters.selection_rule){
    case everywhere:
      os << "everywhere";
      break;
    case core_network:
      os << "core_network";
      break;
    case network_cores:
      os << "network_cores";
      break;
    case aggregated_networks:
      os << "aggregated_networks";
      break;
    case network_connectors:
      os << "network_connectors";
      break;
    default:
      throw std::invalid_argument("invalid value of enum critical_edge_candidates::policy");
  }
  return os;
}

std::istream& operator>>(std::istream& is, critical_selection_policy& critical_selection_rules){
  critical_selection_rules = std::move(critical_selection_policy(is));
  return is;
}
