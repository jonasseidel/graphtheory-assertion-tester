// opertations only supported on master obj copy subgraphs if you need to tip

/*
  targets
*/
bool has_outgoing(const Node* n){
  for(const Edge* e : n->incident()){
    if(e->from() == n) return true;
  }
  return false;
}

std::unordered_set<Node*> Graph::select_targets(const std::unordered_set<Node*>& candidates_target) { // TODO: rename to terminal / extreme or similar
  return this->select_nodes(candidates_target, [](const Node* n){ return !has_outgoing(n); });
}

std::unordered_set<const Node*> Graph::select_targets(const std::unordered_set<const Node*>& candidates_target) const {
  return this->select_nodes(candidates_target, [](const Node* n){ return !has_outgoing(n); });
}

Node* Graph::tip_targets(std::unordered_set<Node*>& tip_candidates_target, random_attribute_generator edge_attribute_generator, random_attribute_generator node_attribute_generator, bool shelter_orphans){
  // nodes must be part of graph
  if(this->obj_type() != GraphtheoryObjType::MasterGraph) throw std::logic_error("calling tip_targets on non-master obj");
  for(Node* n : tip_candidates_target) {
    if(this->local_nodes().find(n) == this->local_nodes().end()) throw std::invalid_argument("Some nodes to be tipped are not part of the graph");
  }

  std::unordered_set<Node*> targets = Graph::select_targets(tip_candidates_target);
  for(auto iter = targets.begin(); iter != targets.end();){
    if(!shelter_orphans && ((*iter)->incident().size() == 0)){
      iter = targets.erase(iter);
    }else{
      ++iter;
    }
  }

  Node* t;

  if(targets.size() > 1){
    std::stringstream node_name;
    node_name << this->lifetime_node_count();
    t = this->add_node(node_name.str(), node_attribute_generator.next());

    for(Node* n : targets){
      std::stringstream edge_name;
      edge_name << n->description() << "_" << t->description();
      this->add_edge(n, t, edge_name.str(), edge_attribute_generator.next());
    }
  }else if(targets.size() == 1){
    t = *targets.begin();
  }else{
    throw std::invalid_argument("no targets to tip");
  }

  return t;
}

Node* Graph::tip_targets(random_attribute_generator edge_attribute_generator, random_attribute_generator node_attribute_generator){
  return this->tip_targets(this->nodes(), edge_attribute_generator, node_attribute_generator, true);
}


/*
  sources
*/
bool has_incoming(const Node* n){
  for(const Edge* e : n->incident()){
    if(e->to() == n) return true;
  }
  return false;
}


std::unordered_set<Node*> Graph::select_sources(const std::unordered_set<Node*>& candidates_source) {
  return this->select_nodes(candidates_source, [](const Node* n){ return !has_incoming(n); });
}

std::unordered_set<const Node*> Graph::select_sources(const std::unordered_set<const Node*>& candidates_source) const {
  return this->select_nodes(candidates_source, [](const Node* n){ return !has_incoming(n); });
}



Node* Graph::tip_sources(random_attribute_generator edge_attribute_generator, random_attribute_generator node_attribute_generator){
  return this->tip_sources(this->nodes(), edge_attribute_generator, node_attribute_generator, true);
}


Node* Graph::tip_sources(std::unordered_set<Node*>& tip_candidates_source, random_attribute_generator edge_attribute_generator, random_attribute_generator node_attribute_generator, bool shelter_orphans){
  // nodes must be part of graph
  if(this->obj_type() != GraphtheoryObjType::MasterGraph) throw std::logic_error("calling select_nodes on non-master obj");
  for(Node* n : tip_candidates_source) {
    if(!this->locally_contains(n)) throw std::invalid_argument("Some tip_candidates_source are not contained in graph");
  }

  std::unordered_set<Node*> sources = Graph::select_sources(tip_candidates_source);
  for(auto iter = sources.begin(); iter != sources.end(); ){
    if(!shelter_orphans && ((*iter)->incident().size() == 0)){
      iter = sources.erase(iter);
    }else{
      ++iter;
    }
  }

  Node* s;

  if(sources.size() > 1){
    std::stringstream node_name;
    node_name << this->lifetime_node_count();
    s = this->add_node(node_name.str(), node_attribute_generator.next());

    for(Node* n : sources){
      std::stringstream edge_name;
      edge_name << s->description() << "_" << n->description();
      this->add_edge(s, n, edge_name.str(), edge_attribute_generator.next());
    }
  }else if(sources.size() == 1){
    s = *sources.begin();
  }else{
    throw std::invalid_argument("no sources to tip");
  }

  return s;
}

/*
  tandem
*/

std::pair<Node*, Node*> Graph::tip_fringes(random_attribute_generator edge_attribute_generator, random_attribute_generator node_attribute_generator, bool shelter_orphans){
  return this->tip_fringes(this->nodes(), this->nodes(), edge_attribute_generator, node_attribute_generator, shelter_orphans);
}

std::pair<Node*, Node*> Graph::tip_fringes(std::unordered_set<Node*>& tip_candidates_source, std::unordered_set<Node*>& tip_candidates_target, random_attribute_generator edge_attribute_generator, random_attribute_generator node_attribute_generator, bool shelter_orphans){
  Node* source = this->tip_sources(tip_candidates_source, edge_attribute_generator, node_attribute_generator, shelter_orphans);
  Node* target = this->tip_targets(tip_candidates_target, edge_attribute_generator, node_attribute_generator, shelter_orphans);
  return {source, target};
}
