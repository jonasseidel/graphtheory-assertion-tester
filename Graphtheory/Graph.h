#ifndef GRAPH_H
#define GRAPH_H

#include <list>
#include <queue>
#include <vector>
#include <unordered_set>
#include <unordered_map>
#include <initializer_list>
#include <functional>
#include <algorithm>
#include <iostream>
#include <string>

#include <exception>
#include <stdexcept>


#include "Edge.h"
#include "Node.h"

#include "Generators/random_attribute_generator.h"
#include "Path.h"

class random_graph_generator;
class SubGraph;

enum GraphtheoryObjType{
  MasterGraph,
  SlaveSubGraph,
  SlavePath
};

class Graph{
public:
  struct GraphSize{
    size_t nodes;
    size_t edges;
  };
private:
  std::unordered_set<Edge*> _edges;
  size_t _lifetime_edge_counter;
  std::unordered_set<Node*> _nodes;
  size_t _lifetime_node_counter;
  std::unordered_map<std::string, Attribute> _template_node_attributes;
  std::unordered_map<std::string, Attribute> _template_edge_attributes;
  SubGraph* _borrower;


  bool has_compatible_default_attributes(const Graph& other) const ;
  // inlineable
  virtual GraphtheoryObjType obj_type(){
    return GraphtheoryObjType::MasterGraph;
  }

  virtual std::unordered_map<std::string, Attribute>& template_node_attributes(){
    return this->_template_node_attributes;
  }
  virtual std::unordered_map<std::string, Attribute>& template_edge_attributes(){
    return this->_template_edge_attributes;
  }

  virtual const std::unordered_map<std::string, Attribute>& template_node_attributes() const {
    return const_cast<Graph*>(this)->_template_node_attributes;
  }
  virtual const std::unordered_map<std::string, Attribute>& template_edge_attributes() const {
    return const_cast<Graph*>(this)->_template_edge_attributes;
  }

  virtual std::unordered_set<Node*>& nodes(){
    return this->_nodes;
  }
  virtual std::unordered_set<Edge*>& edges(){
    return this->_edges;
  }
  virtual std::unordered_set<const Node*> nodes() const {// TODO: there has to be a better way of doing this
    std::unordered_set<Node*>& nodes = const_cast<Graph*>(this)->nodes();
    return std::unordered_set<const Node*>(nodes.begin(), nodes.end());
  }
  virtual std::unordered_set<const Edge*> edges() const {
    std::unordered_set<Edge*>& edges = const_cast<Graph*>(this)->edges();
    return std::unordered_set<const Edge*>(edges.begin(), edges.end());
  }


  virtual std::unordered_set<Node*>& local_nodes() {
    return this->nodes();
  }
  virtual std::unordered_set<Edge*>& local_edges() {
    return this->edges();
  }
  virtual std::unordered_set<const Node*> local_nodes() const {
    std::unordered_set<Node*>& local_nodes = const_cast<Graph*>(this)->local_nodes();
    return std::unordered_set<const Node*>(local_nodes.begin(), local_nodes.end());
  }
  virtual std::unordered_set<const Edge*> local_edges() const {
    std::unordered_set<Edge*>& local_edges = const_cast<Graph*>(this)->local_edges();
    return std::unordered_set<const Edge*>(local_edges.begin(), local_edges.end());
  }

  bool locally_contains(const Node* node) const {
    std::unordered_set<Node*>& local_nodes = const_cast<Graph*>(this)->local_nodes();
    return local_nodes.find(const_cast<Node*>(node)) != local_nodes.end();
  }
  bool locally_contains(const Edge* edge) const {
    std::unordered_set<Edge*>& local_edges = const_cast<Graph*>(this)->local_edges();
    return local_edges.find(const_cast<Edge*>(edge)) != local_edges.end();
  }

  bool globally_contains(const Node* node) const {
    std::unordered_set<Node*>& local_nodes = const_cast<Graph*>(this)->local_nodes();
    return local_nodes.find(const_cast<Node*>(node)) != local_nodes.end();
  }
  bool globally_contains(const Edge* edge) const {
    std::unordered_set<Edge*>& edges = const_cast<Graph*>(this)->edges();
    return edges.find(const_cast<Edge*>(edge)) != edges.end();
  }

  GraphSize local_size() const {
    return {const_cast<Graph*>(this)->local_nodes().size(), const_cast<Graph*>(this)->local_edges().size()};
  }

  GraphSize global_size() const {
    return {const_cast<Graph*>(this)->nodes().size(), const_cast<Graph*>(this)->edges().size()};
  }

  virtual size_t& lifetime_node_counter() {
    return this->_lifetime_node_counter;
  }
  virtual size_t& lifetime_edge_counter() {
    return this->_lifetime_edge_counter;
  }

  SubGraph*& borrower() {
    return this->_borrower;
  }
  void set_unborrowed(){
    this->_borrower = nullptr;
  }

  // _borrowing_logic
  void set_borrowed_by(SubGraph& borrower);
protected:
  /*
    subgraph instances have master graphs pointers and but are not created via casting, however they do inherit functionality.
    In order to let the parent functions simulate inheritance from master we need to allow access to master elements.
    Friendship is the answer.
  */
  friend SubGraph;
public:
  Graph(std::unordered_map<std::string, Attribute> default_values_edge_attributes = {}, std::unordered_map<std::string, Attribute> default_values_node_attributes = {});
  Graph(const Graph& graph);
  Graph(Graph&& graph);
  Graph(const std::unordered_set<Graph>& graphs);
  Graph(std::unordered_set<Graph>&& graphs);

  Edge* add_edge(Node* from, Node* to, const std::string& description, const std::unordered_map<std::string,Attribute>& edge_attributes = {});
  Edge* add_edge(const Edge* remote_edge, const std::unordered_map<const Node*, Node*>& node_lookup);
  std::pair<std::unordered_set<Edge*>, std::unordered_map<const Edge*, Edge*>> add_edges(const std::unordered_set<Edge*>& remote_edge, const std::unordered_map<const Node*, Node*>& node_lookup);
  std::pair<std::unordered_set<Edge*>, std::unordered_map<const Edge*, Edge*>> add_edges(const std::unordered_set<const Edge*>& remote_edge, const std::unordered_map<const Node*, Node*>& node_lookup);
  Node* add_node(std::string description, const std::unordered_map<std::string, Attribute>& node_attributes = {});
  Node* add_node(const Node* remote_node);
  std::pair<std::unordered_set<Node*>, std::unordered_map<const Node*, Node*> > add_nodes(const std::unordered_set<Node*>& remote_nodes);
  std::pair<std::unordered_set<Node*>, std::unordered_map<const Node*, Node*> > add_nodes(const std::unordered_set<const Node*>& remote_nodes);


  void remove_node(Node* node);
  void remove_edge(Edge* edge);
  void remove_nodes(const std::unordered_set<Node*> nodes);
  void remove_edges(const std::unordered_set<Edge*> edges);

  // define aggregate removal functions (also in subgraph)

  std::tuple<std::unordered_set<Node*>, std::unordered_map<const Node*, Node*>, std::unordered_set<Edge*>, std::unordered_map<const Edge*, Edge*>> join_with(const Graph& graph);
  void join_with(Graph&& graph);
  void shrink_graph(std::unordered_set<Node*> spanning_nodes);

  // inlineable

  size_t lifetime_node_count() const {
    return const_cast<Graph*>(this)->lifetime_node_counter();
  }
  size_t lifetime_edge_count() const {
    return const_cast<Graph*>(this)->lifetime_edge_counter();
  }

  bool contains(const Node* node) const {
    return this->locally_contains(node);
  }
  bool contains(const Edge* edge) const {
    return this->locally_contains(edge);
  }

  std::unordered_set<Edge*> export_edges() {
    return this->local_edges();
  }
  std::unordered_set<Node*> export_nodes() {
    return this->local_nodes();
  }

  std::unordered_set<const Edge*> export_edges() const {
    return this->local_edges();
  }
  std::unordered_set<const Node*> export_nodes() const {
    return this->local_nodes();
  }

  bool is_borrowed() const {
    return (this->_borrower != nullptr);
  }
  const SubGraph* borrower() const {
    return this->_borrower;
  }

  std::pair<bool, Attribute> node_template(const std::string& attr) const { // TODO: delete non-throwing functions
    auto search = this->template_node_attributes().find(attr);
    if(search == this->template_node_attributes().end()){
      return {false, {fix, 0}};
    }
    return {true, search->second};
  }

  std::pair<bool, Attribute> edge_template(const std::string& attr) const {
    auto search = this->template_edge_attributes().find(attr);
    if(search == this->template_edge_attributes().end()){
      return {false, {fix, 0}};
    }
    return {true, search->second};
  }

  Attribute& edge_template_throwing(const std::string& attr){
    auto search = this->template_edge_attributes().find(attr);
    if(search == this->template_edge_attributes().end()){
      std::stringstream text;
      text << "\"" << attr << "\" is not defined by default for Edges in used Graph";
      throw std::range_error(text.str());
    }
    return search->second;
  }

  Attribute& node_template_throwing(const std::string& attr){
    auto search = this->template_node_attributes().find(attr);
    if(search == this->template_node_attributes().end()){
      std::stringstream text;
      text << "\"" << attr << "\" is not defined by default for Nodes in used Graph";
      throw std::range_error(text.str());
    }
    return search->second;
  }

  const Attribute& edge_template_throwing(const std::string& attr) const {
    auto search = this->template_edge_attributes().find(attr);
    if(search == this->template_edge_attributes().end()){
      std::stringstream text;
      text << "\"" << attr << "\" is not defined by default for Edges in used Graph";
      throw std::range_error(text.str());
    }
    return search->second;
  }

  const Attribute& node_template_throwing(const std::string& attr) const {
    auto search = this->template_node_attributes().find(attr);
    if(search == this->template_node_attributes().end()){
      std::stringstream text;
      text << "\"" << attr << "\" is not defined by default for Nodes in used Graph";
      throw std::range_error(text.str());
    }
    return search->second;
  }

  void for_nodes(const std::function<void(Node* n)>& action){
    for(Node* n : this->local_nodes()){
      action(n);
    }
  }

  void for_nodes(const std::function<void(const Node* n)>& action) const {
    for(const Node* n : this->local_nodes()){
      action(n);
    }
  }

  void for_edges(const std::function<void(Edge* e)>& action){
    for(Edge* e : this->local_edges()){
      action(e);
    }
  }

  void for_edges(const std::function<void(const Edge* e)>& action) const {
    for(const Edge* e : this->local_edges()){
      action(e);
    }
  }

  void reset_attribute_values(const std::unordered_set<std::string>& edge_attr, const std::unordered_set<std::string>& node_attr = {});

  GraphSize size() const {
    return  this->local_size();
  }

  ~Graph();

  // _path_operations
  Path directed_admissible_st_path(
    Node* s, Node* t,
    const std::function<bool(const Node* from, const Edge* via)>& guide = [](const Node* n, const Edge* e)->bool {return e->from() == n;},
    const std::function<bool(const Node* node)>& mask = [](const Node* node){return true;}
  ) ;
  const Path directed_admissible_st_path(
    const Node* s, const Node* t,
    const std::function<bool(const Node* from, const Edge* via)>& guide = [](const Node* n, const Edge* e)->bool {return e->from() == n;},
    const std::function<bool(const Node* node)>& mask = [](const Node* node){return true;}
  ) const ;
  Path directed_admissible_st_path(
    Node* s, Node* t,
    std::string opt_attr,
    std::string lower_limit_attr,
    std::string upper_limit_attr
  ) ;

  // _select_opertations
  // TODO: declare virtual and support in SubGraph
  std::unordered_set<Edge*> gather_edges(std::unordered_set<std::string> attributes);
  std::unordered_set<Node*> gather_nodes(std::unordered_set<std::string> attributes);
  std::unordered_set<const Edge*> gather_edges(std::unordered_set<std::string> attributes) const ;
  std::unordered_set<const Node*> gather_nodes(std::unordered_set<std::string> attributes) const ;

  std::unordered_set<Node*> select_nodes(const std::unordered_set<Node*>& node_candidates, const std::function<bool(const Node* node)>& criterion);
  std::unordered_set<const Node*> select_nodes(const std::unordered_set<const Node*>& node_candidates, const std::function<bool(const Node* node)>& criterion) const ;

  std::unordered_set<Edge*> select_edges(const std::unordered_set<Edge*>& edge_candidates, const std::function<bool(const Edge* node)>& criterion);
  std::unordered_set<const Edge*> select_edges(const std::unordered_set<const Edge*>& edge_candidates, const std::function<bool(const Edge* edge)>& criterion) const ;


  // _fringe_operations
  // TODO: declare virtual and support in SubGraph?
  std::unordered_set<Node*> select_targets(const std::unordered_set<Node*>& candidates_target);
  std::unordered_set<const Node*> select_targets(const std::unordered_set<const Node*>& candidates_target) const ;

  std::unordered_set<Node*> select_sources(const std::unordered_set<Node*>& candidates_source);
  std::unordered_set<const Node*> select_sources(const std::unordered_set<const Node*>& candidates_source) const ;

  std::unordered_set<Node*> select_targets(){
    return this->select_targets(this->nodes());
  }
  std::unordered_set<const Node*> select_targets() const {
    return this->select_targets(this->nodes());
  }

  std::unordered_set<Node*> select_sources(){
    return this->select_sources(this->nodes());
  }
  std::unordered_set<const Node*> select_sources() const {
    return this->select_sources(this->nodes());
  }

  Node* tip_targets(
    std::unordered_set<Node*>& candidates_target,
    random_attribute_generator edge_attribute_generator = {{}},
    random_attribute_generator node_attribute_generator = {{}},
    bool shelter_orphans = true
  );
  Node* tip_targets(
    random_attribute_generator edge_attribute_generator = {{}},
    random_attribute_generator node_attribute_generator = {{}}
  );
  Node* tip_sources(
    std::unordered_set<Node*>& candidates_source,
    random_attribute_generator edge_attribute_generator = {{}},
    random_attribute_generator node_attribute_generator = {{}},
    bool shelter_orphans = true
  );
  Node* tip_sources(
    random_attribute_generator edge_attribute_generator = {{}},
    random_attribute_generator node_attribute_generator = {{}}
  );
  std::pair<Node*, Node*> tip_fringes(random_attribute_generator edge_attribute_generator = {{}}, random_attribute_generator node_attribute_generator = {{}}, bool shelter_orphans = true);
  std::pair<Node*, Node*> tip_fringes(std::unordered_set<Node*>& candidates_source, std::unordered_set<Node*>& candidates_target, random_attribute_generator edge_attribute_generator = {{}}, random_attribute_generator node_attribute_generator = {{}}, bool shelter_orphans = true);


  // _basic_algorithms
  std::unordered_set<Node*> conditional_bfs_all_reachable(
    const std::function<void(Node* from, Edge* via, bool used_in_traversal)>& edge_exec,
    const std::function<bool(Edge* via, Node* node)>& node_exec,
    const std::unordered_set<Node*>& starting_nodes,
    const bool& all_paths = false,
    const std::function<bool(const Node* from, const Edge* via)>& guide = [](const Node* n, const Edge* e)->bool {return e->from() == n;},
    const std::function<bool(const Node* node)>& mask = [](const Node* node)->bool {return true;}
  );
  std::unordered_set<const Node*> conditional_bfs_all_reachable(
    const std::function<void(const Node* from, const Edge* via, bool used_in_traversal)>& edge_exec,
    const std::function<bool(const Edge* via, const Node* node)>& node_exec,
    const std::unordered_set<const Node*>& starting_nodes,
    const bool& all_paths = false,
    const std::function<bool(const Node* from, const Edge* via)>& guide = [](const Node* n, const Edge* e)->bool {return e->from() == n;},
    const std::function<bool(const Node* node)>& mask = [](const Node* node)->bool {return true;}
  ) const ;
  void conditional_bfs_all_components(
    const std::function<void(Node* from, Edge* via, bool used_in_traversal)>& edge_exec,
    const std::function<bool(Edge* via, Node* node)>& node_exec,
    const std::unordered_set<Node*>& starting_nodes = {},
    const bool& all_paths = false,
    const std::function<bool(const Node* from, const Edge* via)>& guide = [](const Node* n, const Edge* e)->bool {return e->from() == n;},
    const std::function<bool(const Node* node)>& mask = [](const Node* node)->bool {return true;}
  );
  void conditional_bfs_all_components(
    const std::function<void(const Node* from, const Edge* via, bool used_in_traversal)>& edge_exec,
    const std::function<bool(const Edge* via, const Node* node)>& node_exec,
    const std::unordered_set<const Node*>& starting_nodes = {},
    const bool& all_paths = false,
    const std::function<bool(const Node* from, const Edge* via)>& guide = [](const Node* n, const Edge* e)->bool {return e->from() == n;},
    const std::function<bool(const Node* node)>& mask = [](const Node* node)->bool {return true;}
  ) const ;
  bool is_strongly_connected(
    const std::function<bool(const Node* from, const Edge* via)>& guide_forward = [](const Node* n, const Edge* e)->bool {return e->from() == n;},
    const std::function<bool(const Node* from, const Edge* via)>& guide_backward = [](const Node* n, const Edge* e)->bool {return e->to() == n;},
    const std::function<bool(const Node* node)>& mask = [](const Node* node){return true;}
  ) const ;
  bool is_weakly_connected(
    const std::function<bool(const Node* from, const Edge* via)>& guide_forward = [](const Node* n, const Edge* e)->bool {return e->from() == n;},
    const std::function<bool(const Node* from, const Edge* via)>& guide_backward = [](const Node* n, const Edge* e)->bool {return e->to() == n;},
    const std::function<bool(const Node* node)>& mask = [](const Node* node){return true;}
  ) const ;
  // _flow_algorithms.cpp:
  double ford_fulkerson(Node* s, Node* t, std::string opt_attr, std::string lower_limit_attr, std::string upper_limit_attr);


  // _special_members_and_operators:
  void operator=(const Graph& graph);
  std::tuple< Graph, std::unordered_map<const Node*, Node*>, std::unordered_map<const Edge*, Edge*> > copy_ret_lookups() const;
  void operator=(Graph&& graph);

  // _debug_helpers (Graph.ipp)
  void dump();

  // _io:
  Graph(std::istream& is);
  friend std::ostream& operator<<(std::ostream& os, const Graph& g);
  friend std::istream& operator>>(std::istream& is, Graph& g);

  // Friendships
  friend random_graph_generator;
};

std::ostream& operator<<(std::ostream& os, const Graph& g);

std::istream& operator>>(std::istream& is, Graph& g);

#endif
