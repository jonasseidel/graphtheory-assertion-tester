#ifndef VARIABLE_H
#define VARIABLE_H

#include <string>
#include <cstring>
#include <iostream>
#include <cassert>

#include <scip/scip.h>

#include "../Common/integrality.h"


class Variable{
  std::string _description;
  integrality _integrality;
  std::pair<bool, double> _lower_bound;
  std::pair<bool, double> _upper_bound;

  bool _fixed;
  double _value;
public:
  Variable(const Variable& var);
  Variable(Variable&& var);
//  Variable(integrality integrality, std::pair<bool, double> lower_bound = {false, 0}, std::pair<bool, double> upper_bound = {false, 0}, double value = 0); // string needs to be intialized
  Variable(std::string description, integrality integrality, std::pair<bool, double> lower_bound = {false, 0}, std::pair<bool, double> upper_bound = {false, 0}, double value = 0);

  std::string description() const ;
  integrality is_integral() const ;
  std::pair<bool, double> lower_bound() const ;
  std::pair<bool, double> upper_bound() const ;

  SCIP_Real computational_lower_bound(SCIP* scip) const ;
  SCIP_Real computational_upper_bound(SCIP* scip) const ;

  bool is_fixed() const ;
  double& value();
  double value() const ;

  SCIP_VAR* computational_var(SCIP* scip, double direction_coefficient = 0) const ;

  static bool VERBOSE_IDENT;
};

std::ostream& operator<<(std::ostream& os, const Variable& var);

#endif
