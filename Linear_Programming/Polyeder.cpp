#include "Polyeder.h"

Polyeder::Polyeder(){}

Polyeder::Polyeder(const Polyeder& p){
  *this = p;
}

Polyeder::Polyeder(Polyeder&& p){
  *this = std::move(p);
}

std::pair<Polyeder, std::unordered_map<const Variable*, Variable*>> Polyeder::copy_ret_lookup() const {
  Polyeder copy;
  std::unordered_map<const Variable*, Variable*> variable_lookup;

  for(auto& [var, id] : this->_variables){
    auto [new_var, new_id] = copy.add_variable(*var);
    variable_lookup.insert({var, new_var});
  }
  for(const Constraint& cons : this->_constraints){
    copy.add_constraint(cons, variable_lookup);
  }

  return {std::move(copy), std::move(variable_lookup)};
}

boost::bimap<Variable*, size_t>& Polyeder::variables(){
  return this->_variables;
}

size_t Polyeder::vs_dim() const {
  return this->_variables.size();
}

Variable& Polyeder::variable(size_t index){
  auto search_result = this->_variables.right.find(index);
  if(search_result == this->_variables.right.end()) throw std::range_error("no such variable!");
  return *search_result->second;
}

const Variable& Polyeder::variable(size_t index) const {
  auto search_result = this->_variables.right.find(index);
  if(search_result == this->_variables.right.end()) throw std::range_error("no such variable!");
  return *search_result->second;
}

size_t Polyeder::variable(Variable& var) const {
  auto search_result = this->_variables.left.find(&var);
  if(search_result == this->_variables.left.end()) throw std::range_error("no such variable!");
  return search_result->second;
}

std::pair<Variable*, size_t> Polyeder::add_variable(const Variable& var){
  Variable* new_var = new Variable(var);
  size_t position = this->_variables.size();
  this->_variables.insert({new_var, position});
  return {new_var, position};
}

std::string Polyeder::variable_identifier(size_t index) const {
  std::stringstream name;
  if(Variable::VERBOSE_IDENT){
    name << this->variable(index).description();
  }else{
    name << "x" << index;
  }
  return name.str();
}

std::vector<Constraint>& Polyeder::constraints(){
  return this->_constraints;
}

const std::vector<Constraint>& Polyeder::constraints() const {
  return this->_constraints;
}

size_t Polyeder::number_of_constraints() const {
  return this->_constraints.size();
}

Constraint& Polyeder::constraint(size_t index){
  return this->_constraints[index];
}

void Polyeder::add_constraint(const Constraint& constraint){
  // TODO: check that variables used in constraint are registered in polyeder?
  this->_constraints.push_back(constraint);
}

void Polyeder::add_constraint(const Constraint& constraint, const std::unordered_map<const Variable*, Variable*>& variable_lookup){
  // TODO: check that variables used in constraint are registered in polyeder?
  this->_constraints.push_back(Constraint(constraint, variable_lookup));
}

Polyeder::~Polyeder(){
  for(auto& [var, is] : this->_variables){
    delete var;
  }
}

void Polyeder::operator=(const Polyeder& p){
  *this = std::move(this->copy_ret_lookup().first);
}

void Polyeder::operator=(Polyeder&& p){
  if(this != &p){
    for(auto& [vars, index] : this->_variables){
      delete vars;
    }
  }

  this->_variables = p._variables;
  this->_constraints = p._constraints;
  p._variables = {};
  p._constraints = {};
}

std::ostream& operator<<(std::ostream& os, const Polyeder& polyeder){
  os << "Subject to\n";
  for(auto constraint : polyeder.constraints()) {
    os << constraint << "\n";
  }
  os << "Bounds\n";
  for(auto var : polyeder._variables){
    if(var.left->lower_bound().first || var.left->upper_bound().first){
      if(var.left->lower_bound().first){
        os << var.left->lower_bound().second << " <= ";
      }
      os << var.left->description();
      if(var.left->upper_bound().first){
        os << " <= " << var.left->upper_bound().second;
      }
      os << "\n";
    }
  }
  os << "General\n";
  for(auto var : polyeder._variables){
    if(var.left->is_integral() == Integral){
      os << var.left->description() << " ";
    }
  }
  return os;
}
