#include <sstream>
#include <vector>
#include <iterator>

void gather_data(std::filesystem::path path, std::vector<Data>& data_vector, int depth){
  std::stringstream buffer;
  for(int i = 0; i < depth; i++){
    buffer << "   ";
  }

  //typedef std::vector<Data>::iterator Iter;
  std::cerr << buffer.str() << "--> going into " << path.string() << std::endl;

  // iterate over instances in folder
  for (const auto & entry : std::filesystem::directory_iterator(path)){
    if(entry.is_directory()){
      gather_data(entry.path(), data_vector, depth+1);
      //std::vector<Data> deep_data_vector = std::move(gather_data(entry.path(), depth+1));
      //data_vector.insert(data_vector.begin(), std::move_iterator<Iter>(deep_data_vector.begin()), std::move_iterator<Iter>(deep_data_vector.end()));
    }else{
      // check if data describes instance and if it has previously been executed (all data has been calculated)
      try{
        data_vector.push_back(std::move(Data{}));

        std::fstream data_file;
        data_file.open(entry.path(), std::ios::in);
        data_file >> data_vector.back();
        data_file.close();

        if(!data_vector.back().values_complete()) {
          throw std::range_error("not all data has been calculated; execute the models first");
        }
        std::cerr << buffer.str() << "data appears correct; collecting..." << std::endl;
      }catch(std::invalid_argument& e){
        data_vector.pop_back();
        std::cerr << buffer.str() << "file doesn't appear to contain test data" << std::endl;
      }catch(std::range_error& e){
        data_vector.pop_back();
        std::cerr << buffer.str() << "scipping instance, due to it not having been executed" << std::endl;
      }catch(std::exception& e){
        data_vector.pop_back();
        throw e;
      }
    }
  }

  //return std::move(data_vector);
}

std::map<std::string, std::vector<Data>> sort_data_by_parent_directory_name(const std::vector<Data>& data_vector){
  std::map<std::string, std::vector<Data>> parent_folder_map;
  for(const Data& data : data_vector){
    auto iter = parent_folder_map.emplace(std::filesystem::path(data.path).parent_path().filename().string(), std::vector<Data>());
    iter.first->second.push_back(data);
  }

  return parent_folder_map;
}

std::map<std::pair<double,double>, std::vector<Data>> sort_data_by_raster_position(const std::vector<Data>& data_vector, const axis_data& x_axis_data, const axis_data& y_axis_data){
  std::map<std::pair<double,double>, std::vector<Data>> parent_folder_map;

  for(const Data& data : data_vector){
    double xval = -1;
    double yval = -1;
    {
      std::stringstream raster_entry_folder_name;
      raster_entry_folder_name << std::filesystem::path(data.path).parent_path().filename().string();

      // parsing folder name
      std::string curr;
      std::getline(raster_entry_folder_name, curr, ':');
      if(curr == "raster_pos"){
        // parse for x_axis_val
        std::getline(raster_entry_folder_name, curr, '=');
        if(curr != x_axis_data.name){
          throw std::invalid_argument("data doesn't plot on the provided axes");
        }
        std::getline(raster_entry_folder_name, curr, '_');
        xval = std::stod(curr);

        // parse for y_axis_val
        std::getline(raster_entry_folder_name, curr, '=');
        if(curr != y_axis_data.name){
          throw std::invalid_argument("data doesn't plot on the provided axes");
        }
        std::getline(raster_entry_folder_name, curr);
        yval = std::stod(curr);
      }else{
        throw std::invalid_argument("data doesn't belong to raster plot");
      }
    }

    auto iter = parent_folder_map.emplace(std::pair{xval,yval}, std::vector<Data>());
    iter.first->second.push_back(data);
  }

  return parent_folder_map;
}

void sorted_data_to_csv(std::fstream& file, const std::map<std::string, std::vector<Data>>& sorted_data, const axis_data& x_axis_data, const axis_data& y_axis_data, const std::vector<std::string>& models){
  file << Data::csv_columns_incl_pos(x_axis_data, y_axis_data, models, "", ",") << "\n";
  for(const auto& category_pair : sorted_data){
    auto csv_string = Data::csv_data_avg_unknown_but_coherent_pos(category_pair.second, x_axis_data, y_axis_data, models, ",");
    file << csv_string << "\n";
  }
}

void sorted_data_to_csv(std::fstream& file, const std::map<std::pair<double,double>, std::vector<Data>>& sorted_data, const axis_data& x_axis_data, const axis_data& y_axis_data, const std::vector<std::string>& models, bool avg_or_med){
  file << Data::csv_columns_incl_pos(x_axis_data, y_axis_data, models, "", ",") << "\n";
  for(const auto& category_pair : sorted_data){
    auto csv_string = Data::csv_data_avg_or_median_at_given_pos(category_pair.first, category_pair.second, x_axis_data, y_axis_data, models, avg_or_med, ",");
    file << csv_string << "\n";
  }
}
