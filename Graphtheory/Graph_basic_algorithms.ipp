std::unordered_set<Node*> Graph::conditional_bfs_all_reachable(
  const std::function<void(Node* from, Edge* via, bool used_in_traversal)>& edge_exec,
  const std::function<bool(Edge* via, Node* node)>& node_exec,
  const std::unordered_set<Node*>& starting_nodes,
  const bool& all_paths,
  const std::function<bool(const Node* from, const Edge* via)>& guide,
  const std::function<bool(const Node* node)>& mask
){
  /**
    Executes edge_exec and node_exec for every edge or node of the weak components containing
    starting_nodes in visiting order. Guide provides information about edge directedness.


    !! possibly executes twice if guide allows !!
    node_exec will be called with nullptr for via for start node.
    node_exec might be called with nullptr for via if the graph is not strongly connected

    if node_exec returns true traversal over its incident edges will be forced no matter their visitation status

    flow controlling functions are recommended to execute in O(1)
  */

  for(Node* n : starting_nodes){
    if( !locally_contains(n) ) throw std::invalid_argument("starting bfs with nodes that are not contained in graph/subgraph");
    if( !mask(n) ) throw std::invalid_argument("starting bfs with nodes that where supposed to remain unexplored");
  }
  if(starting_nodes.size() == 0) throw std::invalid_argument("bfs' starting_nodes can not be empty");

  std::deque<std::pair<Edge*, Node*>> active;
  std::unordered_set<Node*> charted;
  for(Node* n : starting_nodes){
    active.push_back({nullptr, n});
    charted.insert(n);
  }

  while(!active.empty()){
    std::pair<Edge*, Node*> next = active.front(); active.pop_front();
    bool local_all_paths = node_exec(next.first, next.second);

    Node* n = next.second;

    for(Edge* e : n->incident()){
      if( !this->locally_contains(e->to(n)) || !this->locally_contains(e) || !(guide(n, e) || all_paths || local_all_paths) ) continue;
      bool used_in_traversal = false;

      if( mask(e->to(n)) && charted.find(e->to(n)) == charted.end() ) {
        used_in_traversal = true;

        active.push_back({e, e->to(n)});
        charted.insert(e->to(n));
      }

      edge_exec(n, e, used_in_traversal);
    }
  }

  return charted;
}

std::unordered_set<const Node*> Graph::conditional_bfs_all_reachable(
  const std::function<void(const Node* from, const Edge* via, bool used_in_traversal)>& edge_exec,
  const std::function<bool(const Edge* via, const Node* node)>& node_exec,
  const std::unordered_set<const Node*>& starting_nodes,
  const bool& all_paths,
  const std::function<bool(const Node* from, const Edge* via)>& guide,
  const std::function<bool(const Node* node)>& mask
) const {
  std::unordered_set<Node*> mutable_starting_nodes;
  for(const Node* n : starting_nodes){
    mutable_starting_nodes.insert(const_cast<Node*>(n));
  }
  const auto& result = const_cast<Graph*>(this)->conditional_bfs_all_reachable(edge_exec, node_exec, mutable_starting_nodes, all_paths, guide, mask);
  std::unordered_set<const Node*> const_result;
  for(const Node* n : result){
    const_result.insert(n);
  }
  return const_result;
  // TODO: there has to be a better way of doing this
}

void Graph::conditional_bfs_all_components(
  const std::function<void(Node* from, Edge* via, bool used_in_traversal)>& edge_exec,
  const std::function<bool(Edge* via, Node* node)>& node_exec,
  const std::unordered_set<Node*>& starting_nodes,
  const bool& all_paths,
  const std::function<bool(const Node* from, const Edge* via)>& guide,
  const std::function<bool(const Node* node)>& mask
){
  /**
    Executes edge_exec and node_exec for every edge or node of the weak components containing
    starting_nodes in visiting order. Guide provides information about edge directedness.


    !! possibly executes twice if guide allows !!
    node_exec will be called with nullptr for via for start node.
    node_exec might be called with nullptr for via if the graph is not strongly connected

    if node_exec returns true traversal over its incident edges will be forced no matter their visitation status

    flow controlling functions are recommended to execute in O(1)
  */

  // TODO: expose component number to exec functions

  std::unordered_set<Node*> uncharted;
  for(Node* n : this->local_nodes()){
    if(mask(n)) uncharted.insert(n);
  }

  std::unordered_set<Node*> bfs_starting_nodes(starting_nodes.begin(), starting_nodes.end());

  while(!uncharted.empty()){
    if( bfs_starting_nodes.empty() ) bfs_starting_nodes = {*uncharted.begin()};

    auto last_charted = conditional_bfs_all_reachable(
      edge_exec,
      node_exec,
      bfs_starting_nodes,
      all_paths,
      guide,
      [&mask, &uncharted](const Node* node){return (uncharted.find(const_cast<Node*>(node)) != uncharted.end()) && mask(node);}
    );

    for(Node* charted : last_charted){
      if(uncharted.find(charted) == uncharted.end()) std::cout << *charted << std::endl;
      uncharted.erase(charted);
    }
    bfs_starting_nodes = {};
  }
}

void Graph::conditional_bfs_all_components(
  const std::function<void(const Node* from, const Edge* via, bool used_in_traversal)>& edge_exec,
  const std::function<bool(const Edge* via, const Node* node)>& node_exec,
  const std::unordered_set<const Node*>& starting_nodes,
  const bool& all_paths,
  const std::function<bool(const Node* from, const Edge* via)>& guide,
  const std::function<bool(const Node* node)>& mask
) const {
  std::unordered_set<Node*> mutable_starting_nodes;
  for(const Node* n : starting_nodes){
    mutable_starting_nodes.insert(const_cast<Node*>(n));
  }
  const_cast<Graph*>(this)->conditional_bfs_all_components(edge_exec, node_exec, mutable_starting_nodes, all_paths, guide, mask);
}

bool Graph::is_strongly_connected(
  const std::function<bool(const Node* from, const Edge* via)>& guide_forward,
  const std::function<bool(const Node* from, const Edge* via)>& guide_backward,
  const std::function<bool(const Node* node)>& mask
) const {
  return (this->local_nodes() == this->conditional_bfs_all_reachable(
      [](const Node* from, const Edge* via, bool used_in_traversal){},
      [](const Edge* via, const Node* node){return false;},
      {*this->nodes().begin()},
      false,
      guide_forward,
      mask
    )
    &&
    this->local_nodes() == this->conditional_bfs_all_reachable(
      [](const Node* from, const Edge* via, bool used_in_traversal){},
      [](const Edge* via, const Node* node){return false;},
      {*this->nodes().begin()},
      false,
      guide_backward,
      mask
    )
  );
}

bool Graph::is_weakly_connected(
  const std::function<bool(const Node* from, const Edge* via)>& guide_forward,
  const std::function<bool(const Node* from, const Edge* via)>& guide_backward,
  const std::function<bool(const Node* node)>& mask
) const {
    return (this->nodes() == this->conditional_bfs_all_reachable(
      [](const Node* from, const Edge* via, bool used_in_traversal){},
      [](const Edge* via, const Node* node){return false;},
      {*this->nodes().begin()},
      false,
      [&guide_forward, &guide_backward](const Node* from, const Edge* via){return guide_forward(from, via) || guide_backward(from, via);},
      [](const Node* node){return true;}
  ));
}
