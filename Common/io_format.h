#ifndef IO_FORMAT_H
#define IO_FORMAT_H

#include <stdexcept>
#include <sstream>

#define IO_THROW(actual, expected) \
if(actual != expected) { \
  std::stringstream error; \
  error << "format expected \"" << expected << "\" got \"" << actual << "\""; \
  throw std::invalid_argument(error.str()); \
};

#endif
