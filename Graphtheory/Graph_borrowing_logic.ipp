#include "SubGraph.h"

void Graph::set_borrowed_by(SubGraph& borrower){
  if(borrower.obj_type() != SlaveSubGraph) throw std::runtime_error("SubGraph isn't actually of SubGraph type. Only SubGraphs can borrow");
  this->_borrower = &borrower;
}
