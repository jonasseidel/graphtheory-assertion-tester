#ifndef SEMIASSIGNMENT_HDLR_H

#include <cassert>
#include <vector>
#include <iostream>

#include <scip/scip.h>
#include <scip/scipdefplugins.h>
#include <scip/struct_cons.h>
#include <scip/struct_var.h>


extern "C" {

SCIP_EXPORT
SCIP_RETCODE SCIPincludeConshdlrSemiassign(
  SCIP*                 scip
);

SCIP_EXPORT
SCIP_RETCODE SCIPcreateConsSemiassign(
  SCIP*                 scip,
  SCIP_CONS**           cons,
  const char*           name,
  int                   nvars,
  SCIP_VAR**            vars
);
}

#endif
