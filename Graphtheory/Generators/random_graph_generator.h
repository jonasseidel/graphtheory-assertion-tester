#ifndef RANDOM_GRAPH_GENERATOR_H
#define RANDOM_GRAPH_GENERATOR_H

#include <cstddef>
#include <tuple>
#include <iostream>
#include <string>
#include <sstream>

#include "../../Common/random_set_element_generator.h"
#include "random_attribute_generator.h"
#include "../Graph.h"
#include "../Path.h"
#include "../SubGraph.h"
#include "../graphtheory_exceptions.h"

#include "random_graph_generator_plugins/edge_step_fuzzing_plugin.h"
#include "tipping_policy.h"
#include "random_graph_generator_plugins/node_steps_plugin.h"


class random_graph_generator{
  tipping_policy _structure_graph_tipping_parameters;
  node_step_plugin _structure_graph_node_generator;
  edge_step_fuzzing_plugin _structure_graph_edge_generator;

  tipping_policy _cluster_tipping_parameters;
  node_step_plugin _cluster_node_generator;
  edge_step_fuzzing_plugin _cluster_edge_generator;

  std::unordered_map<std::string, Attribute> _default_edge_attributes;
  std::unordered_map<std::string, Attribute> _default_node_attributes;


  random_attribute_generator _structure_graph_edge_tipping_attribute_generator;
  random_attribute_generator _structure_graph_node_tipping_attribute_generator;

  random_attribute_generator _cluster_edge_tipping_attribute_generator;
  random_attribute_generator _cluster_node_tipping_attribute_generator;

  // _generation
  std::tuple<std::pair<Node*, Node*>, std::unordered_set<Edge*>, Graph> generate_flow_network(tipping_policy& tipping_parameters, edge_step_fuzzing_plugin& edge_generator, node_step_plugin& node_generator, random_attribute_generator& edge_attribute_generator, random_attribute_generator& node_attribute_generator, size_t node_index_start = 0);
  std::pair<std::vector<std::unordered_set<Node*>>, std::unordered_set<Edge*>> grow_random_in_steps(Graph& g, edge_step_fuzzing_plugin& edge_generator, node_step_plugin& node_generator);

public:
  random_graph_generator(){}

  //
  random_graph_generator(
    tipping_policy structure_graph_tipping_parameters,
    node_step_plugin structure_graph_node_generator,
    edge_step_fuzzing_plugin structure_graph_edge_generator,
    tipping_policy cluster_tipping_parameters,
    node_step_plugin cluster_node_generator,
    edge_step_fuzzing_plugin cluster_edge_generator,
    std::unordered_map<std::string, Attribute> default_edge_attributes = {},
    std::unordered_map<std::string, Attribute> default_node_attributes = {},
    random_attribute_generator structure_graph_edge_tipping_attribute_generator = random_attribute_generator(),
    random_attribute_generator structure_graph_node_tipping_attribute_generator = random_attribute_generator(),
    random_attribute_generator cluster_edge_tipping_attribute_generator = random_attribute_generator(),
    random_attribute_generator cluster_node_tipping_attribute_generator = random_attribute_generator()
  );
  random_graph_generator(const random_graph_generator& other){
    *this = other;
  }
  random_graph_generator(random_graph_generator&& other){
    *this = std::move(other);
  }


  // _generation
  std::tuple<std::pair<Node*, Node*>, std::unordered_set<Edge*>, std::unordered_set<Edge*>, std::unordered_set<Edge*>, std::unordered_set<Edge*>, Graph> next();
  random_graph_generator& operator>>(Graph& var);
  static std::unordered_set<Edge*> connect_random(
    SubGraph& graph_superunion,
    SubGraph& subgraph_from,
    SubGraph& subgraph_to,
    random_attribute_generator& edge_attribute_generator,
    bool at_least_strongly_connected,
    bool at_least_weakly_connected,
    bool acyclic,
    bool simple,
    bool anti_reflexive,
    size_t at_least_number_of_edges
  );

  //
  void operator=(const random_graph_generator& other);
  void operator=(random_graph_generator&& other);
  std::pair<bool, Attribute> node_template(const std::string& attr) const;
  std::pair<bool, Attribute> edge_template(const std::string& attr) const;

  Attribute edge_template_throwing(const std::string& attr);
  Attribute node_template_throwing(const std::string& attr);

  const tipping_policy& structure_graph_tipping_parameters() const {
    return this->_structure_graph_tipping_parameters;
  }
  const tipping_policy& cluster_tipping_parameters() const {
    return this->_cluster_tipping_parameters;
  }

  bool operator==(const random_graph_generator& other) const ;
  bool operator!=(const random_graph_generator& other) const {
    return !(*this == other);
  }

  static std::string csv_columns(std::string prefix, std::string separator);
  std::string csv_data(std::string separator) const ;

  random_graph_generator(std::istream& is);

  friend std::ostream& operator<<(std::ostream& os, const random_graph_generator& graph_generator);
  friend std::istream& operator>>(std::istream& is, random_graph_generator& graph_generator);
};

std::ostream& operator<<(std::ostream& os, const random_graph_generator& graph_generator);
std::istream& operator>>(std::istream& is, random_graph_generator& graph_generator);


#endif
