#ifndef RANDOM_SET_ELEMENT_GENERATOR_H
#define RANDOM_SET_ELEMENT_GENERATOR_H

#include <cstddef>
#include <random>
#include <unordered_set>

enum generation_type{
  random_unused,
  random_all_static,
  random_all_dynamic
};

template <typename T>
class random_set_element_generator{
  static std::default_random_engine _engine;
  std::unordered_set<T>* _dynamic_set;
  std::unordered_set<T> _static_set;
  std::unordered_set<T> _current_run_set;
  generation_type _gen_type;

public:
  random_set_element_generator(std::unordered_set<T>* s, generation_type gen_type);
  random_set_element_generator(std::unordered_set<T> s, generation_type gen_type);

  void reset();
  bool has_next();
  T next();
  void operator>>(T& var);
};

template <typename T>
std::default_random_engine random_set_element_generator<T>::_engine((std::random_device())());




#include "random_set_element_generator.ipp"

#endif
