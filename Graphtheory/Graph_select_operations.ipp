
std::unordered_set<Edge*> Graph::gather_edges(std::unordered_set<std::string> attributes){
  std::unordered_set<Edge*> edges;

  for(Edge* e : this->edges()){
    for(std::string attribute_name :  attributes){
      auto attribute_search = e->attribute(attribute_name);
      if(attribute_search.first && attribute_search.second.value()){
        edges.insert(e);
      }
    }
  }

  return edges;
}


std::unordered_set<Node*> Graph::gather_nodes(std::unordered_set<std::string> attributes){
  std::unordered_set<Node*> nodes;

  for(Node* n : this->nodes()){
    for(std::string attribute_name :  attributes){
      auto attribute_search = n->attribute(attribute_name);
      if(attribute_search.first && attribute_search.second.value()){
        nodes.insert(n);
      }
    }
  }

  return nodes;
}


std::unordered_set<const Edge*> Graph::gather_edges(std::unordered_set<std::string> attributes) const {
  std::unordered_set<const Edge*> edges;

  for(const Edge* e : this->edges()){
    for(std::string attribute_name :  attributes){
      auto attribute_search = e->attribute(attribute_name);
      if(attribute_search.first && attribute_search.second.value()){
        edges.insert(e);
      }
    }
  }

  return edges;
}


std::unordered_set<const Node*> Graph::gather_nodes(std::unordered_set<std::string> attributes) const {
  std::unordered_set<const Node*> nodes;
  for(const Node* n : this->nodes()){
    for(std::string attribute_name :  attributes){
      auto attribute_search = n->attribute(attribute_name);
      if(attribute_search.first && attribute_search.second.value()){
        nodes.insert(n);
      }
    }
  }

  return nodes;
}


std::unordered_set<Node*> Graph::select_nodes(const std::unordered_set<Node*>& node_candidates, const std::function<bool(const Node* node)>& criterion) {
  // only allow on master obj since the function will have to respect local structure
  if(this->obj_type() != GraphtheoryObjType::MasterGraph) throw std::logic_error("calling select_nodes on non-master obj");
  for(Node* n : node_candidates){
    if(!this->globally_contains(n)) throw std::invalid_argument("Some of the node_candidates are not contained in graph");
  }

  std::unordered_set<Node*> selection;
  for(Node* n : node_candidates){
    if(criterion(n)) selection.insert(n);
  }
  return selection;
}

std::unordered_set<const Node*> Graph::select_nodes(const std::unordered_set<const Node*>& node_candidates, const std::function<bool(const Node* node)>& criterion) const {
  std::unordered_set<Node*> mutable_node_candidates;
  for(const Node* n : node_candidates){
    mutable_node_candidates.insert(const_cast<Node*>(n));
  }
  const auto& selection = const_cast<Graph*>(this)->select_nodes(mutable_node_candidates, criterion);
  return std::unordered_set<const Node*>(selection.begin(), selection.end());
}



std::unordered_set<Edge*> Graph::select_edges(const std::unordered_set<Edge*>& edge_candidates, const std::function<bool(const Edge* node)>& criterion) {
  // only allow on master obj since the function will have to respect local structure
  if(this->obj_type() != GraphtheoryObjType::MasterGraph) throw std::logic_error("calling select_nodes on non-master obj");
  for(Edge* e : edge_candidates){
    if(!this->globally_contains(e)) throw std::invalid_argument("Some of the edge_candidates are not contained in graph");
  }

  std::unordered_set<Edge*> selection;
  for(Edge* e : edge_candidates){
    if(criterion(e)) selection.insert(e);
  }
  return selection;
}

std::unordered_set<const Edge*> Graph::select_edges(const std::unordered_set<const Edge*>& edge_candidates, const std::function<bool(const Edge* edge)>& criterion) const {
  std::unordered_set<Edge*> mutable_edge_candidates;
  for(const Edge* e : edge_candidates){
    mutable_edge_candidates.insert(const_cast<Edge*>(e));
  }
  const auto& selection = const_cast<Graph*>(this)->select_edges(mutable_edge_candidates, criterion);
  return std::unordered_set<const Edge*>(selection.begin(), selection.end());
}
