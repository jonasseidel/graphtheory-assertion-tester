#include "uniform_edge_step_fuzzing.h"
#include "../random_graph_generator.h"

void uniform_edge_step_fuzzing::constr(){
  this->_get_edge_step_fuzzing_plugin_type = [](){
    return "uniform_edge_step_fuzzing";
  };
  this->_init = [](edge_step_fuzzing_plugin* uncast_here){
    uniform_edge_step_fuzzing* here = static_cast<uniform_edge_step_fuzzing*>(uncast_here);
    here->_add_edges = [here](Graph& g, const std::vector<std::unordered_set<Node*>>& node_steps){
      std::unordered_set<Edge*> added_edges;

      size_t number_of_possible_connections = 0;
      for(size_t step_from = 0; step_from < node_steps.size(); ++step_from){
        for(size_t step_to = step_from+here->_fuzzing_distance_from; (step_to <= step_from+here->_fuzzing_distance_to) && (step_to < node_steps.size()); ++step_to){
          number_of_possible_connections += node_steps[step_from].size() * node_steps[step_to].size();
        }
      }
      double share_of_generated_edges = ((double)here->_number_of_edges)/number_of_possible_connections;
      SubGraph subgraph_superunion(g);
      for(size_t step_from = 0; step_from < node_steps.size(); ++step_from){
        for(size_t step_to = step_from+here->_fuzzing_distance_from; (step_to <= step_from+here->_fuzzing_distance_to) && (step_to < node_steps.size()); ++step_to){
          SubGraph* subgraph_from;
          SubGraph* subgraph_to;

          if(here->_acyclic){
            subgraph_from = new SubGraph(subgraph_superunion, node_steps[step_from]);

            if(step_from == step_to){
              subgraph_to = new SubGraph(*subgraph_from, node_steps[step_to]);
            }else{
              subgraph_to = new SubGraph(subgraph_superunion, node_steps[step_to]);
            }
          }else{
            std::unordered_set<Node*> step_union = node_steps[step_from];
            step_union.insert(node_steps[step_to].begin(), node_steps[step_from].end());
            subgraph_from = new SubGraph(subgraph_superunion, step_union);
            subgraph_to = new SubGraph(*subgraph_from, step_union);
          }

          try{
            size_t number_of_edges_to_be_generated = share_of_generated_edges*subgraph_from->size().nodes*subgraph_to->size().nodes;
            if((ceil(number_of_edges_to_be_generated) - number_of_edges_to_be_generated)/number_of_edges_to_be_generated > .5 ){
              std::cerr << "significant relative difference between requested amount of edges those actually generated" << "\n"
                          << "\tnumber_of_edges = " << here->_number_of_edges << "\n"
                          << "\tnumber_of_steps = " << node_steps.size() << "\n"
                          << "\tnumber_of_edges_to_be_generated(per step) = " << number_of_edges_to_be_generated << std::endl;
            }

            auto last_added_edges = random_graph_generator::connect_random(
              subgraph_superunion,
              *subgraph_from,
              *subgraph_to,
              here->_edge_attribute_generator,
              here->_at_least_strongly_connected,
              here->_at_least_weakly_connected,
              here->_acyclic,
              here->_simple,
              here->_anti_reflexive,
              number_of_edges_to_be_generated
            );
            added_edges.insert(last_added_edges.begin(), last_added_edges.end());
          }catch(excessive_graph_generation_runtime& except){
            for(Edge* e : added_edges){
              g.remove_edge(e);
            }

            delete subgraph_to;
            delete subgraph_from;

            throw except;
          }

          delete subgraph_to;
          delete subgraph_from;
        }
      }

      return added_edges;
    };
  };
}
